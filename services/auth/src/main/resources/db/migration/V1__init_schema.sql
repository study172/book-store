CREATE TABLE authorities
(
    id   TEXT PRIMARY KEY,
    name TEXT NOT NULL UNIQUE
);

CREATE INDEX authorities_name_idx ON authorities (name);

CREATE TABLE clients
(
    id                             TEXT PRIMARY KEY,
    password                       TEXT    NOT NULL,
    scope                          TEXT[],
    auto_approve_scope             TEXT[],
    grant_types                    TEXT[]  NOT NULL,
    access_token_validity_seconds  INTEGER NOT NULL,
    refresh_token_validity_seconds INTEGER NOT NULL,
    additional_info                JSONB,
    enabled                        BOOLEAN NOT NULL
);

CREATE TABLE clients_authorities
(
    client_id    TEXT NOT NULL REFERENCES clients (id),
    authority_id TEXT NOT NULL REFERENCES authorities (id),

    PRIMARY KEY (client_id, authority_id)
);

CREATE INDEX clients_authorities_client_id_idx ON clients_authorities (client_id);

CREATE TABLE users
(
    id       TEXT PRIMARY KEY,
    username TEXT    NOT NULL UNIQUE,
    password TEXT    NOT NULL,
    expired  BOOLEAN NOT NULL,
    locked   BOOLEAN NOT NULL,
    enabled  BOOLEAN NOT NULL
);

CREATE TABLE users_authorities
(
    user_id      TEXT NOT NULL REFERENCES users (id),
    authority_id TEXT NOT NULL REFERENCES authorities (id),

    PRIMARY KEY (user_id, authority_id)
);

CREATE INDEX users_authorities_client_id_idx ON users_authorities (user_id);

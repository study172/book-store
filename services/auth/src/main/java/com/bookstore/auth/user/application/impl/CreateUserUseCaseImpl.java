package com.bookstore.auth.user.application.impl;

import com.bookstore.auth.common.exception.Exceptions;
import com.bookstore.auth.user.application.port.in.CreateUserUseCase;
import com.bookstore.auth.user.application.port.out.CreateUserPort;
import com.bookstore.auth.user.application.port.out.ExistsUserByUsernamePort;
import com.bookstore.auth.user.application.port.out.UserEventPort;
import com.bookstore.auth.user.domain.User;
import com.bookstore.auth.user.domain.command.CreateUserCommand;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class CreateUserUseCaseImpl implements CreateUserUseCase {
    private final ExistsUserByUsernamePort existsUserByUsernamePort;
    private final CreateUserPort createUserPort;
    private final UserEventPort eventPort;

    @Override
    public User create(CreateUserCommand command) {
        if (existsUserByUsernamePort.exists(command.getUsername())) {
            Exceptions.ELEMENT_ALREADY_EXISTS.create()
                    .message("User with username '{}' already exists.", command.getUsername())
                    .doThrow();
        }

        User user = createUserPort.create(new User(command));
        eventPort.created(user);

        return user;
    }
}

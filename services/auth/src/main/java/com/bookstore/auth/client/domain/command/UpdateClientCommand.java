package com.bookstore.auth.client.domain.command;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Data
@Validated
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class UpdateClientCommand {
    @Valid
    @NotNull
    private ClientData clientData;
}

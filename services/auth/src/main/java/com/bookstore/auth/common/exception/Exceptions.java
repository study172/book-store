package com.bookstore.auth.common.exception;

import com.bookstore.common.helper.exception.BaseException;
import com.bookstore.common.helper.exception.ExceptionCreator;
import com.bookstore.common.helper.exception.ExceptionCreatorFactory;
import com.bookstore.common.helper.exception.SimpleExceptionCreator;
import lombok.RequiredArgsConstructor;

import java.util.function.BiFunction;

@RequiredArgsConstructor
public enum Exceptions implements ExceptionCreatorFactory<ExceptionCreator<? extends BaseException>> {
    CONFIRM_PASSWORD_NOT_MATCHED(ConfirmPasswordNotMatchedException::new),
    EMPTY_PASSWORD(EmptyPasswordException::new),
    ILLEGAL_PASSWORD(IllegalPasswordException::new),
    ELEMENT_NOT_FOUND(ElementNotFoundException::new),
    ELEMENT_ALREADY_EXISTS(ElementAlreadyExistsException::new),
    ;

    private final BiFunction<String, Throwable, ? extends BaseException> function;

    @Override
    public SimpleExceptionCreator<? extends BaseException> create() {
        return SimpleExceptionCreator.create(function);
    }
}

package com.bookstore.auth.user.application.port.out;

import com.bookstore.auth.user.domain.User;

public interface CreateUserPort {
    User create(User user);
}

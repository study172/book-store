package com.bookstore.auth.authority.application.port.out;

import com.bookstore.auth.authority.domain.Authority;

public interface AuthorityEventPort {
    void created(Authority authority);

    void deleted(String id);
}

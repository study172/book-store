package com.bookstore.auth.user.adapter.out.stream;

import com.bookstore.auth.user.adapter.mapper.UserMapper;
import com.bookstore.auth.user.adapter.out.stream.binging.UserEventBinding;
import com.bookstore.auth.user.application.port.out.UserEventPort;
import com.bookstore.auth.user.domain.User;
import com.bookstore.common.api.authapi.event.UserEvent;
import com.bookstore.common.api.event.Events;
import lombok.RequiredArgsConstructor;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class UserEventPortImpl implements UserEventPort {
    private final UserEventBinding binding;
    private final UserMapper mapper;

    @Override
    public void created(User user) {
        UserEvent event = UserEvent.created(Events.Metadata.createDefault(), mapper.toUserDto(user));

        binding.events().send(new GenericMessage<>(event, Events.headersWithKey(user.getId())));
    }

    @Override
    public void updated(User user) {
        UserEvent event = UserEvent.updated(Events.Metadata.createDefault(), mapper.toUserDto(user));

        binding.events().send(new GenericMessage<>(event, Events.headersWithKey(user.getId())));
    }

    @Override
    public void deleted(String id) {
        UserEvent event = UserEvent.deleted(Events.Metadata.createDefault(), id);

        binding.events().send(new GenericMessage<>(event, Events.headersWithKey(id)));
    }
}

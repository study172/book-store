package com.bookstore.auth.authority.application.port.in;

import com.bookstore.auth.authority.domain.Authority;

import java.util.Optional;

public interface GetAuthorityByIdUseCase {
    Optional<Authority> get(String id);
}

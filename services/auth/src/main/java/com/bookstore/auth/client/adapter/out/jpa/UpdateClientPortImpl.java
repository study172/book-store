package com.bookstore.auth.client.adapter.out.jpa;

import com.bookstore.auth.client.adapter.mapper.ClientMapper;
import com.bookstore.auth.client.adapter.out.jpa.model.ClientEntity;
import com.bookstore.auth.client.adapter.out.jpa.repository.JpaClientRepository;
import com.bookstore.auth.client.application.port.out.UpdateClientPort;
import com.bookstore.auth.client.domain.Client;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

@Component
@RequiredArgsConstructor
public class UpdateClientPortImpl implements UpdateClientPort {
    protected final JpaClientRepository repository;
    protected final PasswordEncoder encoder;
    protected final ClientMapper mapper;

    @Override
    public Client update(Client client) {
        Assert.notNull(client, () -> "Client must be provided.");
        Assert.hasText(client.getId(), () -> "Client id must be provided.");

        String encodedPassword = encoder.encode(client.getPassword());
        client.setPassword(encodedPassword);

        ClientEntity entity = mapper.mapClient(client);
        return mapper.mapClient(repository.save(entity));
    }
}

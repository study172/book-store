package com.bookstore.auth.client.domain;

import com.bookstore.auth.authority.domain.Authority;
import com.bookstore.auth.client.domain.command.ClientData;
import com.bookstore.auth.client.domain.command.CreateClientCommand;
import com.bookstore.auth.client.domain.command.UpdateClientCommand;
import com.bookstore.auth.common.security.details.ExtendedClientDetails;
import com.bookstore.auth.common.security.details.ExtendedClientDetailsImpl;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
public class Client {
    private static final long serialVersionUID = 2377131682648593168L;

    private String id;
    private String password;
    private Set<String> scope;
    private Set<String> autoApproveScopes;
    private Set<GrantType> grantTypes;
    private Collection<Authority> authorities;
    private Integer accessTokenValiditySeconds;
    private Integer refreshTokenValiditySeconds;
    private Map<String, Object> additionalInfo;
    private boolean enabled;

    public Client(CreateClientCommand command) {
        ClientData clientData = command.getClientData();

        this.id = Optional.ofNullable(clientData.getClientId()).orElseGet(() -> UUID.randomUUID().toString());
        this.password = clientData.getPassword();
        this.scope = clientData.getScope();
        this.autoApproveScopes = clientData.getAutoApproveScopes();
        this.grantTypes = clientData.getGrantTypes();
        this.authorities = clientData.getAuthorities();
        this.accessTokenValiditySeconds = clientData.getAccessTokenValiditySeconds();
        this.refreshTokenValiditySeconds = clientData.getRefreshTokenValiditySeconds();
        this.additionalInfo = clientData.getAdditionalInfo();
        this.enabled = true;
    }

    public ExtendedClientDetails toClientDetails() {
        ExtendedClientDetailsImpl clientDetails = new ExtendedClientDetailsImpl();
        clientDetails.setClientId(id);
        clientDetails.setClientSecret(password);
        clientDetails.setScope(scope);
        if (autoApproveScopes != null) {
            clientDetails.setAutoApproveScopes(autoApproveScopes);
        }
        clientDetails.setAuthorizedGrantTypes(stringGrantTypes());
        if (authorities != null) {
            clientDetails.setAuthorities(authorities);
        }
        clientDetails.setAccessTokenValiditySeconds(accessTokenValiditySeconds);
        clientDetails.setRefreshTokenValiditySeconds(refreshTokenValiditySeconds);
        if (additionalInfo != null) {
            clientDetails.setAdditionalInformation(additionalInfo);
        }
        clientDetails.setEnabled(enabled);
        return clientDetails;
    }

    public Client update(UpdateClientCommand command) {
        ClientData clientData = command.getClientData();

        this.password = clientData.getPassword();
        this.scope = clientData.getScope();
        this.autoApproveScopes = clientData.getAutoApproveScopes();
        this.grantTypes = clientData.getGrantTypes();
        this.authorities = clientData.getAuthorities();
        this.accessTokenValiditySeconds = clientData.getAccessTokenValiditySeconds();
        this.refreshTokenValiditySeconds = clientData.getRefreshTokenValiditySeconds();
        this.additionalInfo = clientData.getAdditionalInfo();

        return this;
    }

    private Collection<String> stringGrantTypes() {
        return Optional.ofNullable(grantTypes).stream()
                .flatMap(Collection::stream)
                .map(GrantType::getGrantTypeName)
                .collect(Collectors.toSet());
    }
}

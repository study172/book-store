package com.bookstore.auth.client.application.port.out;

import com.bookstore.auth.client.domain.Client;

public interface ClientEventPort {
    void created(Client client);

    void updated(Client client);

    void deleted(String id);
}

package com.bookstore.auth.user.adapter.out.jpa.model;

import com.bookstore.auth.common.entity.BaseEntity;
import com.bookstore.auth.authority.adapter.out.jpa.model.AuthorityEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Collection;

@Data
@Entity
@Table(name = "users")
@EqualsAndHashCode(callSuper = true)
public class UserEntity extends BaseEntity {
    private String username;

    private String password;

    private boolean expired;

    private boolean locked;

    private boolean enabled;

    @OneToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "users_authorities",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "authority_id")
    )
    private Collection<AuthorityEntity> authorities;
}

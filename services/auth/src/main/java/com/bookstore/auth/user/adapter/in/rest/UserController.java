package com.bookstore.auth.user.adapter.in.rest;

import com.bookstore.auth.common.exception.Exceptions;
import com.bookstore.auth.common.mapper.PageMapper;
import com.bookstore.auth.user.adapter.mapper.UserMapper;
import com.bookstore.auth.user.application.port.in.CreateUserUseCase;
import com.bookstore.auth.user.application.port.in.DeleteUserUseCase;
import com.bookstore.auth.user.application.port.in.GetUserByIdUseCase;
import com.bookstore.auth.user.application.port.in.PageUsersUseCase;
import com.bookstore.auth.user.application.port.in.UpdateUserUseCase;
import com.bookstore.auth.user.domain.command.CreateUserCommand;
import com.bookstore.auth.user.domain.command.UpdateUserCommand;
import com.bookstore.common.api.Page;
import com.bookstore.common.api.authapi.AuthApi;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/users")
public class UserController {
    private final GetUserByIdUseCase getUserByIdUseCase;
    private final PageUsersUseCase pageUsersUseCase;
    private final CreateUserUseCase createUserUseCase;
    private final UpdateUserUseCase updateUserUseCase;
    private final DeleteUserUseCase deleteUserUseCase;
    private final UserMapper mapper;
    private final PageMapper pageMapper;

    @GetMapping("/{id}")
    public AuthApi.User get(@PathVariable("id") String id) {
        return getUserByIdUseCase.get(id)
                .map(mapper::toUserDto)
                .orElseThrow(() -> Exceptions.ELEMENT_NOT_FOUND.create()
                        .message("User with id ''{}'' not found.", id)
                        .get());
    }

    @GetMapping
    public Page<AuthApi.User> get(@PageableDefault(sort = "username") Pageable pageable) {
        return pageMapper.toPage(pageUsersUseCase.page(pageable).map(mapper::toUserDto));
    }

    @PostMapping
    public AuthApi.User create(@RequestBody @Validated CreateUserCommand command) {
        return mapper.toUserDto(createUserUseCase.create(command));
    }

    @PutMapping("/{id}")
    public AuthApi.User update(@PathVariable("id") String id,
                               @RequestBody @Validated UpdateUserCommand command) {
        return mapper.toUserDto(updateUserUseCase.update(id, command));
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable("id") String id) {
        deleteUserUseCase.delete(id);
    }

}

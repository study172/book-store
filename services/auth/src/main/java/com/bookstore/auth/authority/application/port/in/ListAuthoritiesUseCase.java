package com.bookstore.auth.authority.application.port.in;

import com.bookstore.auth.authority.domain.Authority;

import java.util.Collection;

public interface ListAuthoritiesUseCase {
    Collection<Authority> get();
}

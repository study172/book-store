package com.bookstore.auth.client.application;

import com.bookstore.auth.client.application.port.in.UpdateClientUseCase;
import com.bookstore.auth.client.application.port.out.ClientEventPort;
import com.bookstore.auth.client.application.port.out.CreateClientPort;
import com.bookstore.auth.client.application.port.out.ExistsClientByIdPort;
import com.bookstore.auth.client.application.port.out.GetClientByIdPort;
import com.bookstore.auth.client.application.port.out.UpdateClientPort;
import com.bookstore.auth.client.domain.Client;
import com.bookstore.auth.client.domain.command.UpdateClientCommand;
import com.bookstore.auth.common.exception.Exceptions;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

@Component
@RequiredArgsConstructor
public class UpdateClientUseCaseImpl implements UpdateClientUseCase {
    private final GetClientByIdPort getClientByIdPort;
    private final UpdateClientPort updateClientPort;
    private final ClientEventPort eventPort;

    @Override
    @Transactional(isolation = Isolation.REPEATABLE_READ)
    public Client update(String id, UpdateClientCommand command) {
        Client client = getClientByIdPort.get(id).orElseThrow(() -> Exceptions.ELEMENT_NOT_FOUND.create()
                .message("Client with id ''{}'' not found.", id)
                .get());

        Client updated = updateClientPort.update(client.update(command));
        eventPort.updated(updated);

        return updated;
    }
}

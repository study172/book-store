package com.bookstore.auth.user.application.impl;

import com.bookstore.auth.common.exception.Exceptions;
import com.bookstore.auth.user.application.port.in.DeleteUserUseCase;
import com.bookstore.auth.user.application.port.out.DeleteUserPort;
import com.bookstore.auth.user.application.port.out.ExistsUserByUsernamePort;
import com.bookstore.auth.user.application.port.out.UserEventPort;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

@Component
@RequiredArgsConstructor
public class DeleteUserUseCaseImpl implements DeleteUserUseCase {
    private final ExistsUserByUsernamePort existsUserByUsernamePort;
    private final DeleteUserPort deleteUserPort;
    private final UserEventPort eventPort;

    @Override
    @Transactional(isolation = Isolation.REPEATABLE_READ)
    public void delete(String id) {
        if (!existsUserByUsernamePort.exists(id)) {
            Exceptions.ELEMENT_NOT_FOUND.create()
                    .message("User with id ''{}'' not found.", id)
                    .doThrow();
        }
        deleteUserPort.delete(id);
        eventPort.deleted(id);
    }
}

package com.bookstore.auth.authority.adapter.in.yaml;

import com.bookstore.auth.authority.adapter.in.yaml.properties.AuthorityInitializeProperties;
import com.bookstore.auth.authority.adapter.mapper.AuthorityMapper;
import com.bookstore.auth.authority.application.port.in.CreateAuthorityUseCase;
import com.bookstore.auth.authority.application.port.in.ListAuthoritiesByNamesUseCase;
import com.bookstore.auth.authority.domain.Authority;
import com.bookstore.auth.authority.domain.command.CreateAuthorityCommand;
import com.bookstore.auth.common.initializer.Initializer;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@Component
@RequiredArgsConstructor
public class AuthorityYamlInitializer implements Initializer {
    public static final int ORDER = 0;

    private final AuthorityMapper mapper;
    private final AuthorityInitializeProperties authorityInitializeProperties;
    private final ListAuthoritiesByNamesUseCase listAuthoritiesUseCase;
    private final CreateAuthorityUseCase createAuthorityUseCase;

    @Override
    public void init() {
        log.info("YAML INITIALIZATION: Authorities initialization started.");

        Collection<AuthorityInitializeProperties.AuthorityInfo> authorityInfos =
                authorityInitializeProperties.getAuthorityInfos();

        if (!CollectionUtils.isEmpty(authorityInfos)) {
            Collection<String> authorityNames = authorityInfos.stream()
                    .map(AuthorityInitializeProperties.AuthorityInfo::getName)
                    .collect(Collectors.toList());

            Set<String> existingAuthorities = listAuthoritiesUseCase.get(authorityNames).stream()
                    .map(Authority::getAuthority)
                    .collect(Collectors.toSet());

            for (AuthorityInitializeProperties.AuthorityInfo authorityInfo : authorityInfos) {
                if (!existingAuthorities.contains(authorityInfo.getName())) {
                    CreateAuthorityCommand command = mapper.toCommand(authorityInfo);
                    createAuthorityUseCase.create(command);
                }
            }
        }

        log.info("YAML INITIALIZATION: Authorities initialization finished.");
    }

    @Override
    public int getOrder() {
        return ORDER;
    }
}

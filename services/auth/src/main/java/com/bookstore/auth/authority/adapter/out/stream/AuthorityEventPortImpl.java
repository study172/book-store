package com.bookstore.auth.authority.adapter.out.stream;

import com.bookstore.auth.authority.adapter.mapper.AuthorityMapper;
import com.bookstore.auth.authority.adapter.out.stream.binding.AuthorityEventBinding;
import com.bookstore.auth.authority.application.port.out.AuthorityEventPort;
import com.bookstore.auth.authority.domain.Authority;
import com.bookstore.common.api.authapi.event.AuthorityEvent;
import com.bookstore.common.api.event.Events;
import com.bookstore.common.api.event.Events.Metadata;
import lombok.RequiredArgsConstructor;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class AuthorityEventPortImpl implements AuthorityEventPort {
    protected final AuthorityMapper mapper;
    protected final AuthorityEventBinding binding;

    @Override
    public void created(Authority authority) {
        AuthorityEvent event = AuthorityEvent.created(Metadata.createDefault(), mapper.toAuthorityDto(authority));

        binding.events().send(new GenericMessage<>(event, Events.headersWithKey(authority.getId())));
    }

    @Override
    public void deleted(String id) {
        AuthorityEvent event = AuthorityEvent.deleted(Metadata.createDefault(), id);

        binding.events().send(new GenericMessage<>(event, Events.headersWithKey(id)));
    }
}

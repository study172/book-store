package com.bookstore.auth.authority.application.port.out;

import com.bookstore.auth.authority.domain.Authority;

public interface CreateAuthorityPort {
    Authority create(Authority authority);
}

package com.bookstore.auth.user.application.port.in;

import com.bookstore.auth.user.domain.User;

import java.util.Optional;

public interface GetUserByIdUseCase {
    Optional<User> get(String id);
}

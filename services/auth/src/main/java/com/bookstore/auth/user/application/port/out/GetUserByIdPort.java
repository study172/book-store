package com.bookstore.auth.user.application.port.out;

import com.bookstore.auth.user.domain.User;

import java.util.Optional;

public interface GetUserByIdPort {
    Optional<User> get(String id);
}

package com.bookstore.auth.authority.adapter.out.jpa;

import com.bookstore.auth.authority.adapter.out.jpa.repository.JpaAuthorityRepository;
import com.bookstore.auth.authority.adapter.mapper.AuthorityMapper;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public abstract class AbstractJpaAuthorityPort {
    protected final AuthorityMapper mapper;
    protected final JpaAuthorityRepository repository;
}

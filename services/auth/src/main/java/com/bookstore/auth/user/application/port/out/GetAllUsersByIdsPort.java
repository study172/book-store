package com.bookstore.auth.user.application.port.out;

import com.bookstore.auth.user.domain.User;

import java.util.Collection;

public interface GetAllUsersByIdsPort {
    Collection<User> get(Collection<String> ids);
}

package com.bookstore.auth.authority.application.port.out;

public interface DeleteAuthorityPort {
    void delete(String id);
}

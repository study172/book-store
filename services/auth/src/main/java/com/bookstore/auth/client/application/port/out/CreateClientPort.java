package com.bookstore.auth.client.application.port.out;

import com.bookstore.auth.client.domain.Client;

public interface CreateClientPort {
    Client create(Client client);
}

package com.bookstore.auth.user.adapter.out.jpa;

import com.bookstore.auth.user.adapter.mapper.UserMapper;
import com.bookstore.auth.user.adapter.out.jpa.model.UserEntity;
import com.bookstore.auth.user.adapter.out.jpa.repository.JpaUserRepository;
import com.bookstore.auth.user.application.port.out.CreateUserPort;
import com.bookstore.auth.user.domain.User;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class CreateUserPortImpl extends AbstractJpaUserPort implements CreateUserPort {
    public CreateUserPortImpl(JpaUserRepository repository, PasswordEncoder encoder, UserMapper mapper) {
        super(repository, encoder, mapper);
    }

    @Override
    public User create(User user) {
        String encoded = encoder.encode(user.getPassword());
        user.setPassword(encoded);

        UserEntity entity = mapper.toUserEntity(user);
        return mapper.toUser(repository.save(entity));
    }
}

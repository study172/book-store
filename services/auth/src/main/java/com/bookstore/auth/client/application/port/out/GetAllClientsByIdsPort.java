package com.bookstore.auth.client.application.port.out;

import com.bookstore.auth.client.domain.Client;

import java.util.Collection;

public interface GetAllClientsByIdsPort {
    Collection<Client> getByIds(Collection<String> ids);
}

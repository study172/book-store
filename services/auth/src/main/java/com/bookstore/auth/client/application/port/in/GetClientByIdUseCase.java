package com.bookstore.auth.client.application.port.in;

import com.bookstore.auth.client.domain.Client;

import java.util.Optional;

public interface GetClientByIdUseCase {
    Optional<Client> get(String id);
}

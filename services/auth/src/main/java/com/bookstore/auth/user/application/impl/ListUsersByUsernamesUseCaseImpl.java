package com.bookstore.auth.user.application.impl;

import com.bookstore.auth.user.application.port.in.ListUsersByUsernamesUseCase;
import com.bookstore.auth.user.application.port.out.ListUsersByUsernamesPort;
import com.bookstore.auth.user.domain.User;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Collection;

@Component
@RequiredArgsConstructor
public class ListUsersByUsernamesUseCaseImpl implements ListUsersByUsernamesUseCase {
    private final ListUsersByUsernamesPort listUsersByUsernamesPort;

    @Override
    public Collection<User> list(Collection<String> usernames) {
        return listUsersByUsernamesPort.get(usernames);
    }
}

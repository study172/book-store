package com.bookstore.auth.authority.application;

import com.bookstore.auth.authority.application.port.in.ListAuthoritiesUseCase;
import com.bookstore.auth.authority.application.port.out.ListAuthoritiesPort;
import com.bookstore.auth.authority.domain.Authority;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Collection;

@Component
@RequiredArgsConstructor
public class ListAuthoritiesUseCaseImpl implements ListAuthoritiesUseCase {
    private final ListAuthoritiesPort listAuthoritiesPort;

    @Override
    public Collection<Authority> get() {
        return listAuthoritiesPort.get();
    }
}

package com.bookstore.auth.authority.application.port.out;

public interface ExistsAuthorityByIdPort {
    boolean exists(String id);
}

package com.bookstore.auth.client.adapter.in.rest;

import com.bookstore.auth.client.adapter.mapper.ClientMapper;
import com.bookstore.auth.client.application.port.in.CreateClientUseCase;
import com.bookstore.auth.client.application.port.in.DeleteClientUseCase;
import com.bookstore.auth.client.application.port.in.GetClientByIdUseCase;
import com.bookstore.auth.client.application.port.in.PageClientsUseCase;
import com.bookstore.auth.client.application.port.in.UpdateClientUseCase;
import com.bookstore.auth.client.domain.command.CreateClientCommand;
import com.bookstore.auth.client.domain.command.UpdateClientCommand;
import com.bookstore.auth.common.exception.Exceptions;
import com.bookstore.auth.common.mapper.PageMapper;
import com.bookstore.common.api.Page;
import com.bookstore.common.api.authapi.AuthApi;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/clients")
public class ClientController {
    private final CreateClientUseCase createClientUseCase;
    private final UpdateClientUseCase updateClientUseCase;
    private final PageClientsUseCase pageClientsUseCase;
    private final GetClientByIdUseCase getClientByIdUseCase;
    private final DeleteClientUseCase deleteClientUseCase;
    private final ClientMapper mapper;
    private final PageMapper pageMapper;

    @GetMapping("/{id}")
    public AuthApi.Client get(@PathVariable("id") String id) {
        return getClientByIdUseCase.get(id)
                .map(mapper::toClientDto)
                .orElseThrow(() -> Exceptions.ELEMENT_NOT_FOUND.create()
                        .message("Client with id ''{}'' not found.", id)
                        .get());
    }

    @GetMapping
    public Page<AuthApi.Client> get(@PageableDefault(sort = "id") Pageable pageable) {
        return pageMapper.toPage(pageClientsUseCase.page(pageable).map(mapper::toClientDto));
    }

    @PostMapping
    public AuthApi.Client create(@RequestBody @Validated CreateClientCommand command) {
        return mapper.toClientDto(createClientUseCase.create(command));
    }

    @PutMapping("/{id}")
    public AuthApi.Client update(@PathVariable("id") String id,
                                 @RequestBody @Validated UpdateClientCommand command) {
        return mapper.toClientDto(updateClientUseCase.update(id, command));
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable("id") String id) {
        deleteClientUseCase.delete(id);
    }
}

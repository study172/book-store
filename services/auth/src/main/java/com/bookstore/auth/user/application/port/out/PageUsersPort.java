package com.bookstore.auth.user.application.port.out;

import com.bookstore.auth.user.domain.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface PageUsersPort {
    Page<User> get(Pageable pageable);
}

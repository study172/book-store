package com.bookstore.auth.client.adapter.out.jpa;

import com.bookstore.auth.client.adapter.out.jpa.model.ClientEntity;
import com.bookstore.auth.client.adapter.out.jpa.repository.JpaClientRepository;
import com.bookstore.auth.client.application.port.out.CreateClientPort;
import com.bookstore.auth.client.adapter.mapper.ClientMapper;
import com.bookstore.auth.client.domain.Client;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class CreateClientPortImpl extends AbstractJpaClientPort implements CreateClientPort {
    public CreateClientPortImpl(JpaClientRepository repository, PasswordEncoder encoder, ClientMapper mapper) {
        super(repository, encoder, mapper);
    }

    @Override
    public Client create(Client client) {
        String encodedPassword = encoder.encode(client.getPassword());
        client.setPassword(encodedPassword);

        ClientEntity entity = mapper.mapClient(client);
        return mapper.mapClient(repository.save(entity));
    }
}

package com.bookstore.auth.authority.application.port.out;

import com.bookstore.auth.authority.domain.Authority;

import java.util.Optional;

public interface GetAuthorityByIdPort {
    Optional<Authority> get(String id);
}

package com.bookstore.auth.user.application.port.in;

import com.bookstore.auth.user.domain.User;

import java.util.Collection;

public interface ListUsersByUsernamesUseCase {
    Collection<User> list(Collection<String> usernames);
}

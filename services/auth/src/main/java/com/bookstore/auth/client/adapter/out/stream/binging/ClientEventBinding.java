package com.bookstore.auth.client.adapter.out.stream.binging;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

public interface ClientEventBinding {
    String CLIENT_EVENTS = "client-events";

    @Output(CLIENT_EVENTS)
    MessageChannel events();
}

package com.bookstore.auth.client.adapter.out.stream.configuration;

import com.bookstore.auth.client.adapter.out.stream.binging.ClientEventBinding;
import org.springframework.cloud.stream.annotation.EnableBinding;

@EnableBinding(ClientEventBinding.class)
public class ClientEventConfiguration {
}

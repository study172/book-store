package com.bookstore.auth.authority.application.port.in;

import com.bookstore.auth.authority.domain.Authority;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface PageAuthoritiesUseCase {
    Page<Authority> page(Pageable pageable);
}

package com.bookstore.auth.common.mapper;

import com.bookstore.common.api.Page;
import org.mapstruct.Mapper;

@Mapper(config = CommonMapperConfig.class)
public abstract class PageMapper {

    public <T> Page<T> toPage(org.springframework.data.domain.Page<T> page) {
        if (page == null) {
            return null;
        }

        return new Page<>(
                page.getNumber(),
                page.getSize(),
                page.getTotalPages(),
                page.getTotalElements(),
                page.getContent()
        );
    }
}

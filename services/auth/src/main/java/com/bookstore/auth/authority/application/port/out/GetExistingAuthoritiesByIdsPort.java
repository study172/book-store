package com.bookstore.auth.authority.application.port.out;

import com.bookstore.auth.authority.domain.Authority;

import java.util.Collection;

public interface GetExistingAuthoritiesByIdsPort {
    Collection<Authority> get(Collection<String> authorityIds);
}

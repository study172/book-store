package com.bookstore.auth.authority.application.port.in;

import com.bookstore.auth.authority.domain.Authority;

import java.util.Collection;

public interface ListAuthoritiesByNamesUseCase {
    Collection<Authority> get(Collection<String> ids);
}

package com.bookstore.auth.client.application.port.in;

public interface DeleteClientUseCase {
    void delete(String id);
}

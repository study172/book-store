package com.bookstore.auth.user.domain;

import com.bookstore.auth.authority.domain.Authority;
import com.bookstore.auth.user.domain.command.CreateUserCommand;
import com.bookstore.auth.user.domain.command.UpdateUserCommand;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.Collection;
import java.util.UUID;

@Data
@NoArgsConstructor
@Accessors(chain = true)
public class User {
    private String id;
    private String username;
    private String password;
    private boolean expired;
    private boolean locked;
    private boolean enabled;
    private Collection<Authority> authorities;

    public User(CreateUserCommand command) {
        this.id = UUID.randomUUID().toString();
        this.username = command.getUsername();
        this.password = command.getPassword();
        this.enabled = true;
        this.authorities = command.getAuthorities();
    }

    public User update(UpdateUserCommand command) {
        this.username = command.getUsername();
        this.password = command.getPassword();
        this.authorities = command.getAuthorities();

        return this;
    }
}

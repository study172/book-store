package com.bookstore.auth.user.application.port.in;

import com.bookstore.auth.user.domain.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface PageUsersUseCase {
    Page<User> page(Pageable pageable);
}

package com.bookstore.auth.common.security.enhancer;

import com.bookstore.auth.common.security.details.UserDetailsAdapter;
import com.bookstore.auth.user.domain.User;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;

import java.util.LinkedHashMap;

@SuppressWarnings("deprecation")
public class UserIdTokenEnhancer implements TokenEnhancer {
    @Override
    public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
        DefaultOAuth2AccessToken result = new DefaultOAuth2AccessToken(accessToken);

        Object principal = authentication.getPrincipal();

        if (principal instanceof UserDetailsAdapter) {
            var info = new LinkedHashMap<>(accessToken.getAdditionalInformation());
            info.put("user_id", ((UserDetailsAdapter) principal).getId());
            result.setAdditionalInformation(info);
        }

        return result;
    }
}

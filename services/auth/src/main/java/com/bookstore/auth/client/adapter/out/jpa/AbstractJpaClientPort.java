package com.bookstore.auth.client.adapter.out.jpa;

import com.bookstore.auth.client.adapter.out.jpa.repository.JpaClientRepository;
import com.bookstore.auth.client.adapter.mapper.ClientMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;

@RequiredArgsConstructor
public abstract class AbstractJpaClientPort {
    protected final JpaClientRepository repository;
    protected final PasswordEncoder encoder;
    protected final ClientMapper mapper;
}

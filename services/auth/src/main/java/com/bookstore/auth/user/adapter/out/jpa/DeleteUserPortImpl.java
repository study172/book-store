package com.bookstore.auth.user.adapter.out.jpa;

import com.bookstore.auth.user.adapter.mapper.UserMapper;
import com.bookstore.auth.user.adapter.out.jpa.repository.JpaUserRepository;
import com.bookstore.auth.user.application.port.out.DeleteUserPort;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class DeleteUserPortImpl extends AbstractJpaUserPort implements DeleteUserPort {
    public DeleteUserPortImpl(JpaUserRepository repository, PasswordEncoder encoder, UserMapper mapper) {
        super(repository, encoder, mapper);
    }

    @Override
    public void delete(String id) {
        repository.deleteById(id);
    }
}

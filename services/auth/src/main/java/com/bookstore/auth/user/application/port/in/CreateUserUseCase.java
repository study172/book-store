package com.bookstore.auth.user.application.port.in;

import com.bookstore.auth.user.domain.User;
import com.bookstore.auth.user.domain.command.CreateUserCommand;

public interface CreateUserUseCase {
    User create(CreateUserCommand command);
}

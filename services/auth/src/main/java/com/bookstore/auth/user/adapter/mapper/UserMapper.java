package com.bookstore.auth.user.adapter.mapper;

import com.bookstore.auth.common.mapper.CommonMapperConfig;
import com.bookstore.auth.common.mapper.MapperHelper;
import com.bookstore.auth.authority.adapter.out.jpa.model.AuthorityEntity;
import com.bookstore.auth.authority.domain.Authority;
import com.bookstore.auth.user.adapter.in.yaml.properties.UserInitializeProperties;
import com.bookstore.auth.user.adapter.out.jpa.model.UserEntity;
import com.bookstore.auth.user.domain.User;
import com.bookstore.auth.user.domain.command.CreateUserCommand;
import com.bookstore.common.api.authapi.AuthApi;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.Collection;
import java.util.List;

import static com.bookstore.auth.common.mapper.MapperHelper.mapSplit;

@Mapper(config = CommonMapperConfig.class)
public abstract class UserMapper {
    public abstract UserEntity toUserEntity(User user);

    @Mapping(target = "update", ignore = true)
    public abstract User toUser(UserEntity user);

    public Authority toAuthority(AuthorityEntity authorityEntity) {
        return new Authority(authorityEntity.getId(), authorityEntity.getName());
    }

    public abstract Collection<User> toUsers(List<UserEntity> userEntities);

    public CreateUserCommand mapToCommand(UserInitializeProperties.UserInfo userInfo) {
        return new CreateUserCommand()
                .setUsername(userInfo.getUsername())
                .setPassword(userInfo.getPassword())
                .setConfirmPassword(userInfo.getPassword())
                .setAuthorities(MapperHelper.mapSplit(userInfo.getAuthorities(), name -> new Authority(null, name)));
    }

    public abstract AuthApi.User toUserDto(User user);

    public abstract AuthApi.Authority toAuthorityDto(Authority authority);
}

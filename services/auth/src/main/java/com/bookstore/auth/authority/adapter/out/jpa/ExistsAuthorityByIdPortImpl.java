package com.bookstore.auth.authority.adapter.out.jpa;

import com.bookstore.auth.authority.adapter.out.jpa.repository.JpaAuthorityRepository;
import com.bookstore.auth.authority.application.port.out.ExistsAuthorityByIdPort;
import com.bookstore.auth.authority.adapter.mapper.AuthorityMapper;
import org.springframework.stereotype.Component;

@Component
public class ExistsAuthorityByIdPortImpl extends AbstractJpaAuthorityPort implements ExistsAuthorityByIdPort {
    public ExistsAuthorityByIdPortImpl(AuthorityMapper mapper, JpaAuthorityRepository repository) {
        super(mapper, repository);
    }

    @Override
    public boolean exists(String id) {
        return repository.existsById(id);
    }
}

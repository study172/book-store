package com.bookstore.auth.user.application.port.out;

import com.bookstore.auth.user.domain.User;

public interface UserEventPort {
    void created(User user);

    void updated(User user);

    void deleted(String id);
}

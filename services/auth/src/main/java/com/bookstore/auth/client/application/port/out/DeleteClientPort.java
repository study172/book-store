package com.bookstore.auth.client.application.port.out;

public interface DeleteClientPort {
    void delete(String id);
}

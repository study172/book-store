package com.bookstore.auth.authority.adapter.out.jpa;

import com.bookstore.auth.authority.adapter.mapper.AuthorityMapper;
import com.bookstore.auth.authority.adapter.out.jpa.repository.JpaAuthorityRepository;
import com.bookstore.auth.authority.application.port.out.GetExistingAuthoritiesByIdsPort;
import com.bookstore.auth.authority.domain.Authority;
import org.springframework.stereotype.Component;

import java.util.Collection;

@Component
public class GetExistingAuthoritiesByIdsPortImpl extends AbstractJpaAuthorityPort implements GetExistingAuthoritiesByIdsPort {
    public GetExistingAuthoritiesByIdsPortImpl(AuthorityMapper mapper, JpaAuthorityRepository repository) {
        super(mapper, repository);
    }

    @Override
    public Collection<Authority> get(Collection<String> authorityIds) {
        return mapper.toAuthorities(repository.findAllById(authorityIds));
    }
}

package com.bookstore.auth.user.adapter.out.jpa;

import com.bookstore.auth.user.adapter.mapper.UserMapper;
import com.bookstore.auth.user.adapter.out.jpa.repository.JpaUserRepository;
import com.bookstore.auth.user.application.port.out.PageUsersPort;
import com.bookstore.auth.user.domain.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class PageUsersPortImpl extends AbstractJpaUserPort implements PageUsersPort {
    public PageUsersPortImpl(JpaUserRepository repository, PasswordEncoder encoder, UserMapper mapper) {
        super(repository, encoder, mapper);
    }

    @Override
    public Page<User> get(Pageable pageable) {
        return repository.findAll(pageable).map(mapper::toUser);
    }
}

package com.bookstore.auth.authority.adapter.out.jpa;

import com.bookstore.auth.authority.adapter.mapper.AuthorityMapper;
import com.bookstore.auth.authority.adapter.out.jpa.repository.JpaAuthorityRepository;
import com.bookstore.auth.authority.application.port.out.ExistsAuthorityByNamePort;
import org.springframework.stereotype.Component;

@Component
public class ExistsAuthorityByNamePortImpl extends AbstractJpaAuthorityPort implements ExistsAuthorityByNamePort {
    public ExistsAuthorityByNamePortImpl(AuthorityMapper mapper, JpaAuthorityRepository repository) {
        super(mapper, repository);
    }

    @Override
    public boolean exists(String name) {
        return repository.existsByName(name);
    }
}

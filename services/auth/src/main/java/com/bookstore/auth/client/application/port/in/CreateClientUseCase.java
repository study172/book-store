package com.bookstore.auth.client.application.port.in;

import com.bookstore.auth.client.domain.Client;
import com.bookstore.auth.client.domain.command.CreateClientCommand;

public interface CreateClientUseCase {
    Client create(CreateClientCommand command);
}

package com.bookstore.auth.client.application.port.in;

import com.bookstore.auth.client.domain.Client;

import java.util.Collection;

public interface ListClientsUseCase {
    Collection<Client> list(Collection<String> ids);
}

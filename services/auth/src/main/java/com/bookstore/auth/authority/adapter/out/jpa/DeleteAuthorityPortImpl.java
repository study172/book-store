package com.bookstore.auth.authority.adapter.out.jpa;

import com.bookstore.auth.authority.adapter.out.jpa.repository.JpaAuthorityRepository;
import com.bookstore.auth.authority.application.port.out.DeleteAuthorityPort;
import com.bookstore.auth.authority.adapter.mapper.AuthorityMapper;
import org.springframework.stereotype.Component;

@Component
public class DeleteAuthorityPortImpl extends AbstractJpaAuthorityPort implements DeleteAuthorityPort {
    public DeleteAuthorityPortImpl(AuthorityMapper mapper, JpaAuthorityRepository repository) {
        super(mapper, repository);
    }

    @Override
    public void delete(String id) {
        repository.deleteById(id);
    }
}

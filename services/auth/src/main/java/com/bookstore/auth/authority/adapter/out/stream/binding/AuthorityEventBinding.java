package com.bookstore.auth.authority.adapter.out.stream.binding;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

public interface AuthorityEventBinding {
    String AUTHORITY_EVENTS = "authority-events";

    @Output(AUTHORITY_EVENTS)
    MessageChannel events();
}

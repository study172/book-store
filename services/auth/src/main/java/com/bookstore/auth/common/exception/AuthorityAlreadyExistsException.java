package com.bookstore.auth.common.exception;

import com.bookstore.common.helper.exception.BaseException;

public class AuthorityAlreadyExistsException extends BaseException {
    private static final long serialVersionUID = 2181768544287129654L;

    public AuthorityAlreadyExistsException(String message, Throwable cause) {
        super(message, cause);
    }
}

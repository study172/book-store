package com.bookstore.auth.authority.application.port.in;

public interface DeleteAuthorityUseCase {
    void delete(String id);
}

package com.bookstore.auth.user.adapter.out.jpa;

import com.bookstore.auth.user.adapter.mapper.UserMapper;
import com.bookstore.auth.user.adapter.out.jpa.repository.JpaUserRepository;
import com.bookstore.auth.user.application.port.out.GetUserByUsernamePort;
import com.bookstore.auth.user.domain.User;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class GetUserByUsernamePortImpl extends AbstractJpaUserPort implements GetUserByUsernamePort {
    public GetUserByUsernamePortImpl(JpaUserRepository repository, PasswordEncoder encoder, UserMapper mapper) {
        super(repository, encoder, mapper);
    }

    @Override
    public Optional<User> get(String username) {
        return repository.findByUsername(username).map(mapper::toUser);
    }
}

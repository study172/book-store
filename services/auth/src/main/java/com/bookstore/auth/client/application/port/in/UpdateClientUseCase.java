package com.bookstore.auth.client.application.port.in;

import com.bookstore.auth.client.domain.Client;
import com.bookstore.auth.client.domain.command.UpdateClientCommand;

public interface UpdateClientUseCase {
    Client update(String id, UpdateClientCommand command);
}

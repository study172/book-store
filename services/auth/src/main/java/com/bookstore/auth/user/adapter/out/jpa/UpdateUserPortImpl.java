package com.bookstore.auth.user.adapter.out.jpa;

import com.bookstore.auth.user.adapter.mapper.UserMapper;
import com.bookstore.auth.user.adapter.out.jpa.model.UserEntity;
import com.bookstore.auth.user.adapter.out.jpa.repository.JpaUserRepository;
import com.bookstore.auth.user.application.port.out.UpdateUserPort;
import com.bookstore.auth.user.domain.User;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

@Component
@RequiredArgsConstructor
public class UpdateUserPortImpl implements UpdateUserPort {
    protected final JpaUserRepository repository;
    protected final PasswordEncoder encoder;
    protected final UserMapper mapper;

    @Override
    public User update(User user) {
        Assert.notNull(user, () -> "User must be provided.");
        Assert.hasText(user.getId(), () -> "User id must be provided.");

        String encoded = encoder.encode(user.getPassword());
        user.setPassword(encoded);

        UserEntity entity = mapper.toUserEntity(user);
        return mapper.toUser(repository.save(entity));
    }
}

package com.bookstore.auth.user.adapter.in.yaml;

import com.bookstore.auth.authority.adapter.in.yaml.AuthorityYamlInitializer;
import com.bookstore.auth.authority.application.port.in.ListAuthoritiesByNamesUseCase;
import com.bookstore.auth.authority.domain.Authority;
import com.bookstore.auth.common.initializer.Initializer;
import com.bookstore.auth.user.adapter.in.yaml.properties.UserInitializeProperties;
import com.bookstore.auth.user.adapter.mapper.UserMapper;
import com.bookstore.auth.user.application.port.in.CreateUserUseCase;
import com.bookstore.auth.user.application.port.in.ListUsersByUsernamesUseCase;
import com.bookstore.auth.user.domain.User;
import com.bookstore.auth.user.domain.command.CreateUserCommand;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@Component
@RequiredArgsConstructor
public class UserYamlInitializer implements Initializer {
    public static final int ORDER = AuthorityYamlInitializer.ORDER + 100;

    private final UserMapper userMapper;
    private final UserInitializeProperties userInitializeProperties;
    private final ListUsersByUsernamesUseCase listUsersByUsernamesUseCase;
    private final CreateUserUseCase createUserUseCase;
    private final ListAuthoritiesByNamesUseCase listAuthoritiesByNamesUseCase;

    @Override
    public void init() {
        log.info("YAML INITIALIZATION: Users initialization started.");

        Collection<UserInitializeProperties.UserInfo> userInfos = userInitializeProperties.getUserInfos();

        if (!CollectionUtils.isEmpty(userInfos)) {
            Collection<String> userNames = userInfos.stream()
                    .map(UserInitializeProperties.UserInfo::getUsername)
                    .collect(Collectors.toList());

            Set<String> existingUsernames = listUsersByUsernamesUseCase.list(userNames).stream()
                    .map(User::getUsername)
                    .collect(Collectors.toSet());

            for (UserInitializeProperties.UserInfo userInfo : userInfos) {
                if (!existingUsernames.contains(userInfo.getUsername())) {
                    CreateUserCommand command = userMapper.mapToCommand(userInfo);
                    fillAuthorities(command);
                    createUserUseCase.create(command);
                }
            }
        }

        log.info("YAML INITIALIZATION: Users initialization finished.");
    }

    private void fillAuthorities(CreateUserCommand command) {
        List<String> authorityNames = command.getAuthorities().stream()
                .map(Authority::getAuthority)
                .collect(Collectors.toList());
        command.setAuthorities(listAuthoritiesByNamesUseCase.get(authorityNames));
    }

    @Override
    public int getOrder() {
        return ORDER;
    }
}

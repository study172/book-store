package com.bookstore.auth.user.adapter.out.stream.binging;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

public interface UserEventBinding {
    String USER_EVENTS = "user-events";

    @Output(USER_EVENTS)
    MessageChannel events();
}

package com.bookstore.auth.client.application.port.out;

public interface ExistsClientByIdPort {
    boolean exists(String clientId);
}

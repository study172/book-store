package com.bookstore.auth.client.adapter.out.jpa;

import com.bookstore.auth.client.adapter.out.jpa.repository.JpaClientRepository;
import com.bookstore.auth.client.application.port.out.DeleteClientPort;
import com.bookstore.auth.client.adapter.mapper.ClientMapper;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class DeleteClientPortImpl extends AbstractJpaClientPort implements DeleteClientPort {
    public DeleteClientPortImpl(JpaClientRepository repository, PasswordEncoder encoder, ClientMapper mapper) {
        super(repository, encoder, mapper);
    }

    @Override
    public void delete(String id) {
        repository.findById(id)
                .map(u -> u.setEnabled(false))
                .map(repository::save)
                .orElseThrow();
    }
}

package com.bookstore.auth.authority.application;

import com.bookstore.auth.authority.application.port.in.DeleteAuthorityUseCase;
import com.bookstore.auth.authority.application.port.out.AuthorityEventPort;
import com.bookstore.auth.authority.application.port.out.DeleteAuthorityPort;
import com.bookstore.auth.authority.application.port.out.ExistsAuthorityByIdPort;
import com.bookstore.auth.common.exception.Exceptions;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

@Component
@RequiredArgsConstructor
public class DeleteAuthorityUseCaseImpl implements DeleteAuthorityUseCase {
    private final ExistsAuthorityByIdPort existsAuthorityByIdPort;
    private final DeleteAuthorityPort deleteAuthorityPort;
    private final AuthorityEventPort eventPort;

    @Override
    @Transactional(isolation = Isolation.REPEATABLE_READ)
    public void delete(String id) {
        if (!existsAuthorityByIdPort.exists(id)) {
            Exceptions.ELEMENT_NOT_FOUND.create()
                    .message("Authority with id ''{}'' not found.", id)
                    .doThrow();
        }

        deleteAuthorityPort.delete(id);
        eventPort.deleted(id);
    }
}

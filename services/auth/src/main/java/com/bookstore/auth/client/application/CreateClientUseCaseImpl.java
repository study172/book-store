package com.bookstore.auth.client.application;

import com.bookstore.auth.client.application.port.in.CreateClientUseCase;
import com.bookstore.auth.client.application.port.out.ClientEventPort;
import com.bookstore.auth.client.application.port.out.CreateClientPort;
import com.bookstore.auth.client.application.port.out.ExistsClientByIdPort;
import com.bookstore.auth.client.domain.Client;
import com.bookstore.auth.client.domain.command.CreateClientCommand;
import com.bookstore.auth.common.exception.Exceptions;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class CreateClientUseCaseImpl implements CreateClientUseCase {
    private final ExistsClientByIdPort existsClientByIdPort;
    private final CreateClientPort createClientPort;
    private final ClientEventPort eventPort;

    @Override
    public Client create(CreateClientCommand command) {
        if (command.getClientData().getClientId() != null
                && existsClientByIdPort.exists(command.getClientData().getClientId())) {
            Exceptions.ELEMENT_ALREADY_EXISTS.create()
                    .message("Client with id ''{}'' already exists.", command.getClientData().getClientId())
                    .doThrow();
        }

        Client client = createClientPort.create(new Client(command));

        eventPort.created(client);

        return client;
    }
}

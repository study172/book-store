package com.bookstore.auth.user.application.port.out;

public interface DeleteUserPort {
    void delete(String id);
}

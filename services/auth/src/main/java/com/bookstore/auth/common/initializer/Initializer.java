package com.bookstore.auth.common.initializer;

import org.springframework.core.Ordered;

public interface Initializer extends Ordered {
    void init();
}

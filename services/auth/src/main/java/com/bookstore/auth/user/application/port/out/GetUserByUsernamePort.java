package com.bookstore.auth.user.application.port.out;

import com.bookstore.auth.user.domain.User;

import java.util.Optional;

public interface GetUserByUsernamePort {
    Optional<User> get(String username);
}

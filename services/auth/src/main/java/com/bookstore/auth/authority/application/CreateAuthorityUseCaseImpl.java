package com.bookstore.auth.authority.application;

import com.bookstore.auth.authority.application.port.in.CreateAuthorityUseCase;
import com.bookstore.auth.authority.application.port.out.AuthorityEventPort;
import com.bookstore.auth.authority.application.port.out.CreateAuthorityPort;
import com.bookstore.auth.authority.application.port.out.ExistsAuthorityByNamePort;
import com.bookstore.auth.authority.domain.Authority;
import com.bookstore.auth.authority.domain.command.CreateAuthorityCommand;
import com.bookstore.auth.common.exception.Exceptions;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

@Component
@RequiredArgsConstructor
public class CreateAuthorityUseCaseImpl implements CreateAuthorityUseCase {
    private final ExistsAuthorityByNamePort existsAuthorityByNamePort;
    private final CreateAuthorityPort createAuthorityPort;
    private final AuthorityEventPort eventPort;

    @Override
    @Transactional(isolation = Isolation.REPEATABLE_READ)
    public Authority create(CreateAuthorityCommand command) {
        if (existsAuthorityByNamePort.exists(command.getName())) {
            Exceptions.ELEMENT_ALREADY_EXISTS.create()
                    .message("Authority with name ''{}'' already exists.", command.getName())
                    .doThrow();
        }

        Authority authority = createAuthorityPort.create(new Authority(command));

        eventPort.created(authority);

        return authority;
    }
}

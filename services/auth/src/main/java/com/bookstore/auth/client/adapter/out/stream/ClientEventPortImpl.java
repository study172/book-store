package com.bookstore.auth.client.adapter.out.stream;

import com.bookstore.auth.client.adapter.mapper.ClientMapper;
import com.bookstore.auth.client.adapter.out.stream.binging.ClientEventBinding;
import com.bookstore.auth.client.application.port.out.ClientEventPort;
import com.bookstore.auth.client.domain.Client;
import com.bookstore.common.api.authapi.event.ClientEvent;
import com.bookstore.common.api.event.Events;
import lombok.RequiredArgsConstructor;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class ClientEventPortImpl implements ClientEventPort {
    private final ClientEventBinding binding;
    private final ClientMapper mapper;

    @Override
    public void created(Client client) {
        ClientEvent event = ClientEvent.created(Events.Metadata.createDefault(), mapper.toClientDto(client));

        binding.events().send(new GenericMessage<>(event, Events.headersWithKey(client.getId())));
    }

    @Override
    public void updated(Client client) {
        ClientEvent event = ClientEvent.updated(Events.Metadata.createDefault(), mapper.toClientDto(client));

        binding.events().send(new GenericMessage<>(event, Events.headersWithKey(client.getId())));
    }

    @Override
    public void deleted(String id) {
        ClientEvent event = ClientEvent.deleted(Events.Metadata.createDefault(), id);

        binding.events().send(new GenericMessage<>(event, Events.headersWithKey(id)));
    }
}

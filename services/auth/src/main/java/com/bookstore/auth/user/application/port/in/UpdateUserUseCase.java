package com.bookstore.auth.user.application.port.in;

import com.bookstore.auth.user.domain.User;
import com.bookstore.auth.user.domain.command.UpdateUserCommand;

public interface UpdateUserUseCase {
    User update(String id, UpdateUserCommand command);
}

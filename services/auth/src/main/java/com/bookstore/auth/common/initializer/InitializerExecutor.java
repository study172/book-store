package com.bookstore.auth.common.initializer;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.SmartLifecycle;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Slf4j
@Component
@RequiredArgsConstructor
public class InitializerExecutor implements SmartLifecycle {
    private boolean running = false;
    private final List<Initializer> initializers;

    @Override
    @Transactional
    public void start() {
        log.info("YAML INITIALIZATION: Start initialization.");

        running = true;
        for (Initializer initializer : initializers) {
            initializer.init();
        }
        running = false;

        log.info("YAML INITIALIZATION: Initialization finished.");
    }

    @Override
    public void stop() { }

    @Override
    public boolean isRunning() {
        return running;
    }
}

package com.bookstore.auth.user.application.port.out;

import com.bookstore.auth.user.domain.User;

import java.util.Collection;

public interface ListUsersByUsernamesPort {
    Collection<User> get(Collection<String> usernames);
}

package com.bookstore.auth.client.adapter.in.yaml;

import com.bookstore.auth.authority.adapter.in.yaml.AuthorityYamlInitializer;
import com.bookstore.auth.authority.application.port.in.ListAuthoritiesByNamesUseCase;
import com.bookstore.auth.authority.domain.Authority;
import com.bookstore.auth.client.adapter.in.yaml.properties.ClientInitializeProperties;
import com.bookstore.auth.client.adapter.mapper.ClientMapper;
import com.bookstore.auth.client.application.port.in.CreateClientUseCase;
import com.bookstore.auth.client.application.port.in.ListClientsUseCase;
import com.bookstore.auth.client.domain.Client;
import com.bookstore.auth.client.domain.command.CreateClientCommand;
import com.bookstore.auth.common.initializer.Initializer;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@Component
@RequiredArgsConstructor
public class ClientYamlInitializer implements Initializer {
    public static final int ORDER = AuthorityYamlInitializer.ORDER + 100;

    private final ClientMapper clientMapper;
    private final ClientInitializeProperties clientInitializeProperties;
    private final CreateClientUseCase createClientUseCase;
    private final ListClientsUseCase listClientsUseCase;
    private final ListAuthoritiesByNamesUseCase listAuthoritiesByNamesUseCase;

    @Override
    public void init() {
        log.info("YAML INITIALIZATION: Clients initialization started.");

        Collection<ClientInitializeProperties.ClientInfo> clientInfos = clientInitializeProperties.getClientInfos();

        if (!CollectionUtils.isEmpty(clientInfos)) {
            Collection<String> clientIds = clientInfos.stream()
                    .map(ClientInitializeProperties.ClientInfo::getId)
                    .collect(Collectors.toList());

            Set<String> existClientIds = listClientsUseCase.list(clientIds).stream()
                    .map(Client::getId)
                    .collect(Collectors.toSet());

            for (ClientInitializeProperties.ClientInfo clientInfo : clientInfos) {
                if (!existClientIds.contains(clientInfo.getId())) {
                    CreateClientCommand command = clientMapper.mapToCommand(clientInfo);
                    fillAuthorities(command);
                    createClientUseCase.create(command);
                }
            }
        }

        log.info("YAML INITIALIZATION: Clients initialization finished.");
    }

    private void fillAuthorities(CreateClientCommand command) {
        List<String> authorityNames = command.getClientData().getAuthorities().stream()
                .map(Authority::getAuthority)
                .collect(Collectors.toList());
        command.getClientData().setAuthorities(listAuthoritiesByNamesUseCase.get(authorityNames));
    }

    @Override
    public int getOrder() {
        return ORDER;
    }
}

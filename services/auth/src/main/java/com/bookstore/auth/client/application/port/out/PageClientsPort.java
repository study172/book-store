package com.bookstore.auth.client.application.port.out;

import com.bookstore.auth.client.domain.Client;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface PageClientsPort {
    Page<Client> get(Pageable pageable);
}

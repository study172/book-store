package com.bookstore.auth.authority.adapter.out.jpa.model;

import com.bookstore.auth.common.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@Entity
@Accessors(chain = true)
@Table(name = "authorities")
@EqualsAndHashCode(callSuper = true)
public class AuthorityEntity extends BaseEntity {
    @Column(name = "name", unique = true)
    private String name;
}

package com.bookstore.auth.user.application.impl;

import com.bookstore.auth.common.exception.Exceptions;
import com.bookstore.auth.user.application.port.in.UpdateUserUseCase;
import com.bookstore.auth.user.application.port.out.GetUserByIdPort;
import com.bookstore.auth.user.application.port.out.UpdateUserPort;
import com.bookstore.auth.user.application.port.out.UserEventPort;
import com.bookstore.auth.user.domain.User;
import com.bookstore.auth.user.domain.command.UpdateUserCommand;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

@Component
@RequiredArgsConstructor
public class UpdateUserUseCaseImpl implements UpdateUserUseCase {
    private final GetUserByIdPort getUserByIdPort;
    private final UpdateUserPort updateUserPort;
    private final UserEventPort eventPort;

    @Override
    @Transactional(isolation = Isolation.REPEATABLE_READ)
    public User update(String id, UpdateUserCommand command) {
        User user = getUserByIdPort.get(id)
                .orElseThrow(() -> Exceptions.ELEMENT_NOT_FOUND.create()
                        .message("User with id ''{}'' not found.", id)
                        .get());

        User updated = updateUserPort.update(user.update(command));
        eventPort.updated(updated);

        return updated;
    }
}

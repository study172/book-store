package com.bookstore.auth.client.adapter.out.jpa;

import com.bookstore.auth.client.adapter.out.jpa.repository.JpaClientRepository;
import com.bookstore.auth.client.application.port.out.ExistsClientByIdPort;
import com.bookstore.auth.client.adapter.mapper.ClientMapper;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class ExistsClientByIdPortImpl extends AbstractJpaClientPort implements ExistsClientByIdPort {
    public ExistsClientByIdPortImpl(JpaClientRepository repository, PasswordEncoder encoder, ClientMapper mapper) {
        super(repository, encoder, mapper);
    }

    @Override
    public boolean exists(String clientId) {
        return repository.existsById(clientId);
    }
}

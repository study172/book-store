package com.bookstore.auth.user.application.port.in;

public interface DeleteUserUseCase {
    void delete(String id);
}

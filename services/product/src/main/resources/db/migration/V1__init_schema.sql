CREATE TABLE shedlock
(
    name       TEXT PRIMARY KEY,
    lock_until TIMESTAMP NOT NULL,
    locked_at  TIMESTAMP NOT NULL,
    locked_by  TEXT      NOT NULL
);

CREATE TABLE _product
(
    id           TEXT PRIMARY KEY,
    description  JSONB,
    product_info JSONB,
    picture      JSONB,

    created_at   TIMESTAMP WITHOUT TIME ZONE,
    modified_at  TIMESTAMP WITHOUT TIME ZONE,
    created_by   TEXT,
    modified_by  TEXT
);

CREATE TABLE _category
(
    name        TEXT PRIMARY KEY,
    description JSONB,

    created_at   TIMESTAMP WITHOUT TIME ZONE,
    modified_at  TIMESTAMP WITHOUT TIME ZONE,
    created_by   TEXT,
    modified_by  TEXT
);

CREATE TABLE _product_category
(
    category TEXT REFERENCES _category (name),
    product  TEXT REFERENCES _product (id),
    index    BIGINT,

    PRIMARY KEY (category, product)
);

CREATE TABLE _reservation
(
    id          TEXT PRIMARY KEY,
    reserved_to TIMESTAMP WITHOUT TIME ZONE,
    created_at  TIMESTAMP WITHOUT TIME ZONE,
    modified_at TIMESTAMP WITHOUT TIME ZONE,
    created_by  TEXT,
    modified_by TEXT
);

CREATE TABLE _reservation_details
(
    id             TEXT PRIMARY KEY,
    product_id     TEXT REFERENCES _product (id),
    reservation_id TEXT REFERENCES _reservation (id),
    quantity       BIGINT NOT NULL,
    index          BIGINT
);

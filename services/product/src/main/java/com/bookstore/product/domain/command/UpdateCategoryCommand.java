package com.bookstore.product.domain.command;

import com.bookstore.product.common.validation.annotation.RequiredCategoryDescriptionFields;
import com.bookstore.product.domain.CategoryDescription;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class UpdateCategoryCommand {

    @NotNull(message = "{category.description.not-null}")
    @RequiredCategoryDescriptionFields(message = "{category.description.required-fields}")
    private CategoryDescription description;
}

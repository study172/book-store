package com.bookstore.product.application.port.out;

import com.bookstore.product.domain.Product;

public interface CreateProductPort {
    Product create(Product product);
}

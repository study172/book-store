package com.bookstore.product.application.impl;

import com.bookstore.product.application.port.in.CancelExpiredReservationsUseCase;
import com.bookstore.product.application.port.out.DeleteReservationByIdPort;
import com.bookstore.product.application.port.out.ReservationEventPort;
import com.bookstore.product.application.port.out.StreamExpiredReservationsPort;
import com.bookstore.product.application.port.out.UpdateProductPort;
import com.bookstore.product.domain.Reservation;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.stream.Stream;

@Component
@RequiredArgsConstructor
public class CancelExpiredReservationsUseCaseImpl implements CancelExpiredReservationsUseCase {
    private final StreamExpiredReservationsPort streamExpiredReservationsPort;
    private final DeleteReservationByIdPort deleteReservationByIdPort;
    private final UpdateProductPort updateProductPort;
    private final ReservationEventPort reservationEventPort;

    @Override
    @Transactional(isolation = Isolation.REPEATABLE_READ)
    public void cancel() {
        try (Stream<Reservation> stream = streamExpiredReservationsPort.stream()) {
            stream.forEach(reservation -> {
                reservation.cancel();

                updateProductPort.updateAll(reservation.getAllProducts());
                deleteReservationByIdPort.delete(reservation.getId());

                reservationEventPort.expired(reservation.getId());
            });
        }
    }
}

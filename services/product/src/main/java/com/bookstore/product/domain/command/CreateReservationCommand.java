package com.bookstore.product.domain.command;

import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.util.Collection;

@Data
public class CreateReservationCommand {
    @NotEmpty
    private Collection<@Valid ProductReservation> reservations;
}

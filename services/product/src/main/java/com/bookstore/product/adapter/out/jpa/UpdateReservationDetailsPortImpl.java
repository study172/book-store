package com.bookstore.product.adapter.out.jpa;

import com.bookstore.product.adapter.mapper.ReservationEntityMapper;
import com.bookstore.product.adapter.out.jpa.entity.ReservationDetailsEntity;
import com.bookstore.product.adapter.out.jpa.repository.ReservationDetailsRepository;
import com.bookstore.product.application.port.out.UpdateReservationDetailsPort;
import com.bookstore.product.domain.ReservationDetails;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.util.List;
import java.util.Objects;

@Component
@RequiredArgsConstructor
public class UpdateReservationDetailsPortImpl implements UpdateReservationDetailsPort {
    private final ReservationDetailsRepository repository;
    private final ReservationEntityMapper mapper;

    @Override
    public List<ReservationDetails> update(List<ReservationDetails> details) {
        Assert.notEmpty(details, () -> "Reservation details must be provided.");
        Assert.noNullElements(details, () -> "Reservation details can not contain null elements.");
        Assert.isTrue(haveIds(details), () -> "Reservation details must have ids.");

        List<ReservationDetailsEntity> entities = mapper.toDetailsEntity(details);
        return mapper.toDetails(repository.saveAll(entities));
    }

    private boolean haveIds(List<ReservationDetails> details) {
        return details.stream()
                .map(ReservationDetails::getId)
                .allMatch(Objects::nonNull);
    }
}

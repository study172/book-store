package com.bookstore.product.application.port.in;

public interface ResetReservationDateUseCase {
    void reset(String id);
}

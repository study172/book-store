package com.bookstore.product.adapter.out.jpa;

import com.bookstore.product.adapter.mapper.CategoryEntityMapper;
import com.bookstore.product.adapter.out.jpa.entity.CategoryEntity;
import com.bookstore.product.adapter.out.jpa.repository.CategoryRepository;
import com.bookstore.product.application.port.out.CreateCategoryPort;
import com.bookstore.product.domain.Category;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class CreateCategoryPortImpl implements CreateCategoryPort {
    private final CategoryRepository repository;
    private final CategoryEntityMapper mapper;

    @Override
    public Category create(Category category) {
        CategoryEntity saved = repository.save(mapper.toCategoryEntity(category));
        return mapper.toCategory(saved);
    }
}

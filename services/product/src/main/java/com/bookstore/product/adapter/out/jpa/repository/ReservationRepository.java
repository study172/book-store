package com.bookstore.product.adapter.out.jpa.repository;

import com.bookstore.product.adapter.out.jpa.entity.ReservationEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.Instant;
import java.util.stream.Stream;

public interface ReservationRepository extends JpaRepository<ReservationEntity, String> {
    Stream<ReservationEntity> streamTop50ByReservedToLessThanEqualOrderByReservedTo(Instant reservedTo);
}

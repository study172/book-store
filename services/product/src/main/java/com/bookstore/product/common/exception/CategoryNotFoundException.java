package com.bookstore.product.common.exception;

import com.bookstore.common.helper.exception.BaseException;

public class CategoryNotFoundException extends BaseException {
    private static final long serialVersionUID = 5507809356342113922L;

    public CategoryNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}

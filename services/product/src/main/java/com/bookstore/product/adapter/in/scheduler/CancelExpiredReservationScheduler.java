package com.bookstore.product.adapter.in.scheduler;

import com.bookstore.product.application.port.in.CancelExpiredReservationsUseCase;
import lombok.RequiredArgsConstructor;
import net.javacrumbs.shedlock.spring.annotation.SchedulerLock;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class CancelExpiredReservationScheduler {
    private final CancelExpiredReservationsUseCase cancelExpiredReservationsUseCase;

    @Scheduled(fixedDelayString = "PT5M")
    @SchedulerLock(name = "cancel-reservation-scheduler", lockAtLeastFor = "PT10S")
    public void cancel() {
        cancelExpiredReservationsUseCase.cancel();
    }
}

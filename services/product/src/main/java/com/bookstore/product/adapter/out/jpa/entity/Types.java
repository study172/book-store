package com.bookstore.product.adapter.out.jpa.entity;

public final class Types {
    private Types() {
        throw new UnsupportedOperationException();
    }

    public static final String JSONB = "jsonb";
}

package com.bookstore.product.adapter.out.jpa;

import com.bookstore.product.adapter.mapper.ProductEntityMapper;
import com.bookstore.product.adapter.out.jpa.repository.ProductRepository;
import com.bookstore.product.application.port.out.PageProductsPort;
import com.bookstore.product.domain.Product;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

@Component
@RequiredArgsConstructor
public class PageProductsPortImpl implements PageProductsPort {
    private final ProductRepository repository;
    private final ProductEntityMapper mapper;

    @Override
    public Page<Product> page(Pageable pageable) {
        Assert.notNull(pageable, () -> "Pageable must be provided.");

        return repository.findAll(pageable).map(mapper::toProduct);
    }
}

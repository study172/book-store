package com.bookstore.product.application.impl;

import com.bookstore.product.application.port.in.UpdateReservationUseCase;
import com.bookstore.product.application.port.out.FindReservationByIdPort;
import com.bookstore.product.application.port.out.ReservationEventPort;
import com.bookstore.product.application.support.ReservationHelper;
import com.bookstore.product.common.exception.Exceptions;
import com.bookstore.product.domain.Reservation;
import com.bookstore.product.domain.command.UpdateReservationCommand;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

@Component
@RequiredArgsConstructor
public class UpdateReservationUseCaseImpl implements UpdateReservationUseCase {
    private final FindReservationByIdPort findReservationByIdPort;
    private final ReservationEventPort reservationEventPort;
    private final ReservationHelper reservationHelper;

    @Override
    @Transactional(isolation = Isolation.REPEATABLE_READ)
    public Reservation update(String id, UpdateReservationCommand command) {
        Reservation reservation = findReservationByIdPort.find(id).orElseThrow(() ->
                Exceptions.RESERVATION_NOT_FOUND_EXCEPTION.create()
                        .message("Reservation with id ''{}'' not found.", id)
                        .get()
        );

        Reservation updated = reservationHelper.update(reservation, command.getReservations());

        reservationEventPort.updated(updated);

        return updated;
    }
}

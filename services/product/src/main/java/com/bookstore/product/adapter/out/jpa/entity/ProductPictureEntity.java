package com.bookstore.product.adapter.out.jpa.entity;

import lombok.Data;

import java.util.List;

@Data
public class ProductPictureEntity {
    private List<PictureEntity> pictures;
}

package com.bookstore.product.adapter.out.jpa;

import com.bookstore.product.adapter.mapper.ReservationEntityMapper;
import com.bookstore.product.adapter.out.jpa.entity.ReservationEntity;
import com.bookstore.product.adapter.out.jpa.repository.ReservationRepository;
import com.bookstore.product.application.port.out.CreateReservationPort;
import com.bookstore.product.domain.Reservation;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

@Component
@RequiredArgsConstructor
public class CreateReservationPortImpl implements CreateReservationPort {
    private final ReservationRepository repository;
    private final ReservationEntityMapper mapper;

    @Override
    public Reservation create(Reservation reservation) {
        Assert.notNull(reservation, () -> "Reservation must be provided.");

        ReservationEntity entity = mapper.toReservationEntity(reservation);

        return mapper.toReservation(repository.save(entity));
    }
}

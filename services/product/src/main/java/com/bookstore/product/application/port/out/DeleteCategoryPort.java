package com.bookstore.product.application.port.out;

import com.bookstore.product.domain.Category;

public interface DeleteCategoryPort {
    void delete(Category category);
}

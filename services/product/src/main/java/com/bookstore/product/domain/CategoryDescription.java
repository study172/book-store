package com.bookstore.product.domain;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.util.List;

@Data
public class CategoryDescription {
    @NotEmpty(message = "{category-description.values.not-empty}")
    private List<Description> values;
}

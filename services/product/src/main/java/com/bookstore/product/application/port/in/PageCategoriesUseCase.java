package com.bookstore.product.application.port.in;

import com.bookstore.product.domain.Category;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface PageCategoriesUseCase {
    Page<Category> page(Pageable pageable);
}

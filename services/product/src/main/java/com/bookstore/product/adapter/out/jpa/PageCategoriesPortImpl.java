package com.bookstore.product.adapter.out.jpa;

import com.bookstore.product.adapter.mapper.CategoryEntityMapper;
import com.bookstore.product.adapter.out.jpa.repository.CategoryRepository;
import com.bookstore.product.application.port.out.PageCategoriesPort;
import com.bookstore.product.domain.Category;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

@Component
@RequiredArgsConstructor
public class PageCategoriesPortImpl implements PageCategoriesPort {
    private final CategoryRepository repository;
    private final CategoryEntityMapper mapper;

    @Override
    public Page<Category> page(Pageable pageable) {
        Assert.notNull(pageable, () -> "Pageable must be provided.");

        return repository.findAll(pageable).map(mapper::toCategory);
    }
}

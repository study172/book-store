package com.bookstore.product.application.impl;

import com.bookstore.product.application.port.in.GetReservationByIdUseCase;
import com.bookstore.product.application.port.out.FindReservationByIdPort;
import com.bookstore.product.common.exception.Exceptions;
import com.bookstore.product.domain.Reservation;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@RequiredArgsConstructor
public class GetReservationByIdUseCaseImpl implements GetReservationByIdUseCase {
    private final FindReservationByIdPort findReservationByIdPort;

    @Override
    @Transactional(readOnly = true)
    public Reservation get(String id) {
        return findReservationByIdPort.find(id).orElseThrow(() ->
                Exceptions.RESERVATION_NOT_FOUND_EXCEPTION.create()
                        .message("Reservation with id ''{}'' not found.", id)
                        .get()
        );
    }
}

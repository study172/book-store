package com.bookstore.product.application.impl;

import com.bookstore.product.application.port.in.CreateReservationUseCase;
import com.bookstore.product.application.port.out.ReservationEventPort;
import com.bookstore.product.application.support.ReservationHelper;
import com.bookstore.product.domain.Reservation;
import com.bookstore.product.domain.command.CreateReservationCommand;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

@Component
@RequiredArgsConstructor
public class CreateReservationUseCaseImpl implements CreateReservationUseCase {
    private final ReservationEventPort reservationEventPort;
    private final ReservationHelper reservationHelper;

    @Override
    @Transactional(isolation = Isolation.REPEATABLE_READ)
    public Reservation create(CreateReservationCommand command) {
        Reservation reservation = reservationHelper.create(command.getReservations());

        reservationEventPort.created(reservation);

        return reservation;
    }
}

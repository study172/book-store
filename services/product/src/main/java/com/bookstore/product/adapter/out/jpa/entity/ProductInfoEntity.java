package com.bookstore.product.adapter.out.jpa.entity;

import lombok.Data;

import java.math.BigDecimal;
import java.math.BigInteger;

@Data
public class ProductInfoEntity {
    private BigDecimal price;
    private BigInteger quantity;
}

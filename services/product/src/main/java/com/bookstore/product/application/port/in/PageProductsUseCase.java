package com.bookstore.product.application.port.in;

import com.bookstore.product.domain.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface PageProductsUseCase {
    Page<Product> page(Pageable pageable);
}

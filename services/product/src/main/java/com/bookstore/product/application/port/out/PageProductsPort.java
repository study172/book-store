package com.bookstore.product.application.port.out;

import com.bookstore.product.domain.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface PageProductsPort {
    Page<Product> page(Pageable pageable);
}

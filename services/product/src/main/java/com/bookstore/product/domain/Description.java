package com.bookstore.product.domain;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class Description {
    @NotBlank(message = "{description.name.not-blank}")
    private String name;

    @NotBlank(message = "{description.value.not-blank}")
    private String value;
}

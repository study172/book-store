package com.bookstore.product.application.port.in;

import com.bookstore.product.domain.Reservation;

public interface GetReservationByIdUseCase {
    Reservation get(String id);
}

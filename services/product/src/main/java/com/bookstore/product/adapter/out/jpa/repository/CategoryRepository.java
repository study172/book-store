package com.bookstore.product.adapter.out.jpa.repository;

import com.bookstore.product.adapter.out.jpa.entity.CategoryEntity;
import com.bookstore.product.adapter.out.jpa.entity.ProductEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryRepository extends JpaRepository<CategoryEntity, String> {
}

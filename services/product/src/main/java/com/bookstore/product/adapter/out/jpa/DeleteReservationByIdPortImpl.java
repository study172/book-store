package com.bookstore.product.adapter.out.jpa;

import com.bookstore.product.adapter.out.jpa.repository.ReservationRepository;
import com.bookstore.product.application.port.out.DeleteReservationByIdPort;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

@Component
@RequiredArgsConstructor
public class DeleteReservationByIdPortImpl implements DeleteReservationByIdPort {
    private final ReservationRepository repository;

    @Override
    public void delete(String id) {
        Assert.hasText(id, () -> "Id must be provided and cannot be blank.");

        repository.deleteById(id);
    }
}

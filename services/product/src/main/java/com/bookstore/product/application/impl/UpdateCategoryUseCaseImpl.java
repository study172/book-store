package com.bookstore.product.application.impl;

import com.bookstore.product.application.port.in.UpdateCategoryUseCase;
import com.bookstore.product.application.port.out.CategoryEventPort;
import com.bookstore.product.application.port.out.FindCategoryByIdPort;
import com.bookstore.product.application.port.out.UpdateCategoryPort;
import com.bookstore.product.common.exception.Exceptions;
import com.bookstore.product.domain.Category;
import com.bookstore.product.domain.command.UpdateCategoryCommand;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

@Component
@RequiredArgsConstructor
public class UpdateCategoryUseCaseImpl implements UpdateCategoryUseCase {
    private final FindCategoryByIdPort findCategoryByIdPort;
    private final UpdateCategoryPort updateCategoryPort;
    private final CategoryEventPort eventPort;

    @Override
    @Transactional(isolation = Isolation.REPEATABLE_READ)
    public Category update(String id, UpdateCategoryCommand command) {
        Category category = findCategoryByIdPort.find(id).orElseThrow(() ->
                Exceptions.CATEGORY_NOT_FOUND_EXCEPTION.create()
                        .message("Category with id ''{}'' not found.", id)
                        .get()
        );

        Category updated = updateCategoryPort.update(category.update(command));
        eventPort.updated(updated);

        return updated;
    }
}

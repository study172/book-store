package com.bookstore.product.adapter.mapper;

import com.bookstore.common.api.productapi.ProductApi;
import com.bookstore.product.adapter.out.jpa.entity.ProductEntity;
import com.bookstore.product.common.mapper.CommonMapperConfig;
import com.bookstore.product.domain.Product;
import org.mapstruct.Mapper;

@Mapper(config = CommonMapperConfig.class, uses = CategoryMapper.class)
public abstract class ProductMapper {

    public abstract ProductApi.Product toProductApi(Product product);
}

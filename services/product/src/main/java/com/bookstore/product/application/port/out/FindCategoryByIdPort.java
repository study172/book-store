package com.bookstore.product.application.port.out;

import com.bookstore.product.domain.Category;

import java.util.Optional;

public interface FindCategoryByIdPort {
    Optional<Category> find(String id);
}

package com.bookstore.product.common.validation;

import com.bookstore.product.common.validation.annotation.RequiredCategoryDescriptionFields;
import com.bookstore.product.domain.CategoryDescription;
import com.github.sgreben.regex_builder.Pattern;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.validation.ConstraintValidatorContext;
import java.util.Set;

import static com.github.sgreben.regex_builder.FluentRe.match;
import static com.github.sgreben.regex_builder.Re.string;

@Component
public class RequiredCategoryDescriptionFieldsValidator
        extends AbstractRequiredDescriptionFieldsValidator<RequiredCategoryDescriptionFields, CategoryDescription> {

    private static final Set<Pattern> REQUIRED_FIELD_PATTERNS = Set.of(
            match(string("description")).compile()
    );

    public boolean isValid(CategoryDescription value, ConstraintValidatorContext context) {
        if (value == null || CollectionUtils.isEmpty(value.getValues())) {
            return true;
        }

        return containsRequiredFields(value.getValues());
    }

    @Override
    protected Set<Pattern> getRequiredPatterns() {
        return REQUIRED_FIELD_PATTERNS;
    }
}

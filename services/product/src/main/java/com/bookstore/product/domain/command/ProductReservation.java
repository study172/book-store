package com.bookstore.product.domain.command;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import java.math.BigInteger;

@Data
public class ProductReservation {
    @NotBlank
    private String productId;

    @Positive
    private BigInteger quantity;
}

package com.bookstore.product.application.impl;

import com.bookstore.product.application.port.in.GetCategoryByIdUseCase;
import com.bookstore.product.application.port.out.FindCategoryByIdPort;
import com.bookstore.product.common.exception.Exceptions;
import com.bookstore.product.domain.Category;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@RequiredArgsConstructor
public class GetCategoryByIdUseCaseImpl implements GetCategoryByIdUseCase {
    private final FindCategoryByIdPort findCategoryByIdPort;

    @Override
    @Transactional(readOnly = true)
    public Category get(String id) {
        return findCategoryByIdPort.find(id).orElseThrow(() ->
                Exceptions.CATEGORY_NOT_FOUND_EXCEPTION.create()
                        .message("Category with id ''{}'' not found.", id)
                        .get()
        );
    }
}

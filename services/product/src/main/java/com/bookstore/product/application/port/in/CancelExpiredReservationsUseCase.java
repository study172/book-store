package com.bookstore.product.application.port.in;

public interface CancelExpiredReservationsUseCase {
    void cancel();
}

package com.bookstore.product.domain.command;

import com.bookstore.product.common.validation.annotation.RequiredCategoryDescriptionFields;
import com.bookstore.product.domain.CategoryDescription;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class CreateCategoryCommand {

    @NotBlank(message = "{create-category-command.category.name.not-blank}")
    private String name;

    @NotNull(message = "{create-category-command.category.description.not-null}")
    @RequiredCategoryDescriptionFields(message = "{create-category-command.category.description.required-fields}")
    private CategoryDescription description;
}

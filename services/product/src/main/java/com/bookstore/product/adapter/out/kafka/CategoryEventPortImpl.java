package com.bookstore.product.adapter.out.kafka;

import com.bookstore.common.api.event.Events;
import com.bookstore.common.api.productapi.event.CategoryEvent;
import com.bookstore.product.adapter.mapper.CategoryMapper;
import com.bookstore.product.adapter.out.kafka.binding.CategoryBinding;
import com.bookstore.product.application.port.out.CategoryEventPort;
import com.bookstore.product.domain.Category;
import lombok.RequiredArgsConstructor;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class CategoryEventPortImpl implements CategoryEventPort {
    private final CategoryBinding binding;
    private final CategoryMapper mapper;

    @Override
    public void created(Category category) {
        CategoryEvent event = CategoryEvent.created(Events.Metadata.createDefault(), mapper.toCategoryApi(category));
        binding.events().send(new GenericMessage<>(event, Events.headersWithKey(category.getName())));
    }

    @Override
    public void updated(Category category) {
        CategoryEvent event = CategoryEvent.updated(Events.Metadata.createDefault(), mapper.toCategoryApi(category));
        binding.events().send(new GenericMessage<>(event, Events.headersWithKey(category.getName())));
    }

    @Override
    public void deleted(String id) {
        CategoryEvent event = CategoryEvent.deleted(Events.Metadata.createDefault(), id);
        binding.events().send(new GenericMessage<>(event, Events.headersWithKey(id)));
    }
}

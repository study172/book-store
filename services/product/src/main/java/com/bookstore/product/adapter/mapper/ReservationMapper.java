package com.bookstore.product.adapter.mapper;

import com.bookstore.common.api.productapi.ProductApi;
import com.bookstore.product.common.mapper.CommonMapperConfig;
import com.bookstore.product.domain.Reservation;
import org.mapstruct.Mapper;

@Mapper(config = CommonMapperConfig.class, uses = {CategoryMapper.class, ProductMapper.class})
public abstract class ReservationMapper {
    public abstract ProductApi.Reservation toReservationApi(Reservation reservation);
}

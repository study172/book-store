package com.bookstore.product.application.impl;

import com.bookstore.product.application.port.in.PageCategoriesUseCase;
import com.bookstore.product.application.port.out.PageCategoriesPort;
import com.bookstore.product.domain.Category;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@RequiredArgsConstructor
public class PageCategoriesUseCaseImpl implements PageCategoriesUseCase {
    private final PageCategoriesPort pageCategoriesPort;

    @Override
    @Transactional(readOnly = true)
    public Page<Category> page(Pageable pageable) {
        return pageCategoriesPort.page(pageable);
    }
}

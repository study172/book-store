package com.bookstore.product.common.exception;

import com.bookstore.common.helper.exception.BaseException;

public class CannotReserveProductException extends BaseException {
    private static final long serialVersionUID = 2064065685243949068L;

    public CannotReserveProductException(String message, Throwable cause) {
        super(message, cause);
    }
}

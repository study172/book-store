package com.bookstore.product.application.port.out;

import com.bookstore.product.domain.Category;

public interface CreateCategoryPort {
    Category create(Category category);
}

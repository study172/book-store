package com.bookstore.product.adapter.in.kafka.binding;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

public interface OrderBinding {
    String ORDER_EVENTS = "order-events";

    @Input(ORDER_EVENTS)
    SubscribableChannel events();
}

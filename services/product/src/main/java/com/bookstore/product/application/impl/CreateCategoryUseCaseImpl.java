package com.bookstore.product.application.impl;

import com.bookstore.product.application.port.in.CreateCategoryUseCase;
import com.bookstore.product.application.port.out.CategoryEventPort;
import com.bookstore.product.application.port.out.CreateCategoryPort;
import com.bookstore.product.domain.Category;
import com.bookstore.product.domain.command.CreateCategoryCommand;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

@Component
@RequiredArgsConstructor
public class CreateCategoryUseCaseImpl implements CreateCategoryUseCase {
    private final CreateCategoryPort createCategoryPort;
    private final CategoryEventPort eventPort;

    @Override
    @Transactional(isolation = Isolation.REPEATABLE_READ)
    public Category create(CreateCategoryCommand command) {
        Category created = createCategoryPort.create(new Category(command));
        eventPort.created(created);

        return created;
    }
}

package com.bookstore.product.application.port.out;

import com.bookstore.product.domain.Reservation;

import java.util.Optional;

public interface FindReservationByIdPort {
    Optional<Reservation> find(String id);
}

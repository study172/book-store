package com.bookstore.product.domain;

import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.util.List;

@Data
public class ProductPicture {
    @NotEmpty(message = "{product-picture.pictures.not-empty}")
    private List<@Valid Picture> pictures;
}

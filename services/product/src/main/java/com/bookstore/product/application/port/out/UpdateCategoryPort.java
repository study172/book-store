package com.bookstore.product.application.port.out;

import com.bookstore.product.domain.Category;

public interface UpdateCategoryPort {
    Category update(Category category);
}

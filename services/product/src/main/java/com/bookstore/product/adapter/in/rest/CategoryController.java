package com.bookstore.product.adapter.in.rest;

import com.bookstore.common.api.Page;
import com.bookstore.common.api.productapi.ProductApi;
import com.bookstore.product.adapter.mapper.CategoryMapper;
import com.bookstore.product.application.port.in.CreateCategoryUseCase;
import com.bookstore.product.application.port.in.DeleteCategoryUseCase;
import com.bookstore.product.application.port.in.GetCategoryByIdUseCase;
import com.bookstore.product.application.port.in.PageCategoriesUseCase;
import com.bookstore.product.application.port.in.UpdateCategoryUseCase;
import com.bookstore.product.common.mapper.PageMapper;
import com.bookstore.product.domain.command.CreateCategoryCommand;
import com.bookstore.product.domain.command.UpdateCategoryCommand;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import static com.bookstore.product.application.support.SecurityUtils.HAS_COMMON_ACCESS;
import static com.bookstore.product.application.support.SecurityUtils.HAS_CONTENT_ACCESS;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/categories")
public class CategoryController {

    private final GetCategoryByIdUseCase getCategoryByIdUseCase;
    private final PageCategoriesUseCase pageCategoriesUseCase;
    private final CreateCategoryUseCase createCategoryUseCase;
    private final UpdateCategoryUseCase updateCategoryUseCase;
    private final DeleteCategoryUseCase deleteCategoryUseCase;
    private final CategoryMapper mapper;
    private final PageMapper pageMapper;

    @GetMapping("/{id}")
    @PreAuthorize(HAS_COMMON_ACCESS)
    public ProductApi.Category get(@PathVariable("id") String id) {
        return mapper.toCategoryApi(getCategoryByIdUseCase.get(id));
    }

    @GetMapping
    @PreAuthorize(HAS_COMMON_ACCESS)
    public Page<ProductApi.Category> get(@PageableDefault(sort = "name") Pageable pageable) {
        return pageMapper.toPage(pageCategoriesUseCase.page(pageable).map(mapper::toCategoryApi));
    }

    @PostMapping
    @PreAuthorize(HAS_CONTENT_ACCESS)
    public ProductApi.Category create(@RequestBody @Validated CreateCategoryCommand command) {
        return mapper.toCategoryApi(createCategoryUseCase.create(command));
    }

    @PutMapping("/{id}")
    @PreAuthorize(HAS_CONTENT_ACCESS)
    public ProductApi.Category update(@PathVariable("id") String id,
                                      @RequestBody @Validated UpdateCategoryCommand command) {
        return mapper.toCategoryApi(updateCategoryUseCase.update(id, command));
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize(HAS_CONTENT_ACCESS)
    public void delete(@PathVariable("id") String id) {
        deleteCategoryUseCase.delete(id);
    }
}

package com.bookstore.product.application.support;

import com.bookstore.product.domain.Reservation;
import com.bookstore.product.domain.command.ProductReservation;

import java.util.Collection;

public interface ReservationHelper {
    Reservation create(Collection<ProductReservation> reservations);

    Reservation update(Reservation reservation, Collection<ProductReservation> reservations);
}

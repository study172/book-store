package com.bookstore.product.application.impl;

import com.bookstore.product.application.port.in.PageProductsUseCase;
import com.bookstore.product.application.port.out.PageProductsPort;
import com.bookstore.product.domain.Product;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@RequiredArgsConstructor
public class PageProductsUseCaseImpl implements PageProductsUseCase {
    private final PageProductsPort pageProductsPort;

    @Override
    @Transactional(readOnly = true)
    public Page<Product> page(Pageable pageable) {
        return pageProductsPort.page(pageable);
    }
}

package com.bookstore.product.application.port.out;

import com.bookstore.product.domain.Product;

import java.util.Collection;

public interface UpdateProductPort {
    Product update(Product product);

    void updateAll(Collection<Product> products);
}

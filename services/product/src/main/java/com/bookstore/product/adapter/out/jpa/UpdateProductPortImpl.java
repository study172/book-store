package com.bookstore.product.adapter.out.jpa;

import com.bookstore.product.adapter.mapper.ProductEntityMapper;
import com.bookstore.product.adapter.out.jpa.entity.ProductEntity;
import com.bookstore.product.adapter.out.jpa.repository.ProductRepository;
import com.bookstore.product.application.port.out.UpdateProductPort;
import com.bookstore.product.common.util.AssertUtils;
import com.bookstore.product.domain.Product;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import java.util.Collection;

import static com.bookstore.product.common.util.AssertUtils.elementsMatchPredicate;

@Component
@RequiredArgsConstructor
public class UpdateProductPortImpl implements UpdateProductPort {
    private final ProductRepository repository;
    private final ProductEntityMapper mapper;

    @Override
    public Product update(Product product) {
        Assert.hasText(product.getId(), () -> "Product id must be provided.");

        ProductEntity saved = repository.save(mapper.toProductEntity(product));
        return mapper.toProduct(saved);
    }

    @Override
    public void updateAll(Collection<Product> products) {
        Assert.notEmpty(products, () -> "Products cannot be empty.");
        Assert.noNullElements(products, () -> "Products cannot contain null elements.");
        elementsMatchPredicate(products, p -> StringUtils.hasText(p.getId()), () -> "Product ids must be provided.");

        repository.saveAll(mapper.toProductEntities(products));
    }
}

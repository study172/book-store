package com.bookstore.product.common.exception;

import com.bookstore.common.helper.exception.BaseException;

public class ReservationNotFoundException extends BaseException {
    private static final long serialVersionUID = -3546841348803151177L;

    public ReservationNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}

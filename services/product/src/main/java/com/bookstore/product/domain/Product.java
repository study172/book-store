package com.bookstore.product.domain;

import com.bookstore.product.common.exception.Exceptions;
import com.bookstore.product.common.util.BigIntegerUtils;
import com.bookstore.product.domain.command.CreateProductCommand;
import com.bookstore.product.domain.command.UpdateProductCommand;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigInteger;
import java.time.Instant;
import java.util.List;

@Data
@NoArgsConstructor
public class Product {
    private String id;

    private ProductDescription description;

    private List<Category> categories;

    private ProductInfo productInfo;

    private ProductPicture picture;

    private Instant createdAt;

    private Instant modifiedAt;

    private String createdBy;

    private String modifiedBy;

    public Product(CreateProductCommand command) {
        this.description = command.getDescription();
        this.categories = command.getCategories();
        this.productInfo = command.getProductInfo();
        this.picture = command.getPicture();
    }

    public Product update(UpdateProductCommand command) {
        this.description = command.getDescription();
        this.categories = command.getCategories();
        this.productInfo = command.getProductInfo();
        this.picture = command.getPicture();

        return this;
    }

    public ReservationDetails reserve(BigInteger quantity) {
        reserveQuantity(quantity);

        return new ReservationDetails(this, quantity);
    }

    public void reserveQuantity(BigInteger quantity) {
        if (BigIntegerUtils.isLessThan(productInfo.getQuantity(), quantity)) {
            Exceptions.CANNOT_RESERVE_PRODUCT_EXCEPTION.create()
                    .message("Not enough quantity [{}] of product [''{}'']", productInfo.getQuantity(), id)
                    .doThrow();
        }

        productInfo.subtract(quantity);
    }

    public void addProducts(BigInteger quantity) {
        productInfo.add(quantity);
    }
}

package com.bookstore.product.adapter.out.jpa;

import com.bookstore.product.adapter.mapper.ReservationEntityMapper;
import com.bookstore.product.adapter.out.jpa.repository.ReservationRepository;
import com.bookstore.product.application.port.out.FindReservationByIdPort;
import com.bookstore.product.domain.Reservation;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class FindReservationByIdPortImpl implements FindReservationByIdPort {
    private final ReservationRepository repository;
    private final ReservationEntityMapper mapper;

    @Override
    public Optional<Reservation> find(String id) {
        Assert.hasText(id, () -> "Id must be provided.");

        return repository.findById(id).map(mapper::toReservation);
    }
}

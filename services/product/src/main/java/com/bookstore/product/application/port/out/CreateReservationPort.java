package com.bookstore.product.application.port.out;

import com.bookstore.product.domain.Reservation;

public interface CreateReservationPort {
    Reservation create(Reservation reservation);
}

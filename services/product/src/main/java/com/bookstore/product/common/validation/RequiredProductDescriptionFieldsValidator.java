package com.bookstore.product.common.validation;

import com.bookstore.product.common.validation.annotation.RequiredProductDescriptionFields;
import com.bookstore.product.domain.ProductDescription;
import com.github.sgreben.regex_builder.Expression;
import com.github.sgreben.regex_builder.Pattern;
import com.github.sgreben.regex_builder.expression.Optional;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.validation.ConstraintValidatorContext;
import java.util.Set;

import static com.github.sgreben.regex_builder.CharClass.digit;
import static com.github.sgreben.regex_builder.FluentRe.match;
import static com.github.sgreben.regex_builder.Re.optional;
import static com.github.sgreben.regex_builder.Re.repeatAtLeastPossessive;
import static com.github.sgreben.regex_builder.Re.sequence;
import static com.github.sgreben.regex_builder.Re.string;

@Component
public class RequiredProductDescriptionFieldsValidator
        extends AbstractRequiredDescriptionFieldsValidator<RequiredProductDescriptionFields, ProductDescription> {

    private static final Set<Pattern> REQUIRED_FIELD_PATTERNS = Set.of(
            match(sequence(createAuthor(), createNumber(), string(".first-name"))).compile(),
            match(sequence(createAuthor(), createNumber(), string(".last-name"))).compile(),
            match(sequence(createAuthor(), createNumber(), string(".patronymic"))).compile(),
            match(string("publication-date")).compile(),
            match(string("publisher")).compile(),
            match(string("name")).compile()
    );

    public boolean isValid(ProductDescription value, ConstraintValidatorContext context) {
        if (value == null || CollectionUtils.isEmpty(value.getValues())) {
            return true;
        }

        return containsRequiredFields(value.getValues());
    }

    @Override
    protected Set<Pattern> getRequiredPatterns() {
        return REQUIRED_FIELD_PATTERNS;
    }

    private static Expression createAuthor() {
        return string("author");
    }

    private static Optional createNumber() {
        return optional(sequence(string("-"), repeatAtLeastPossessive(digit(), 1)));
    }
}

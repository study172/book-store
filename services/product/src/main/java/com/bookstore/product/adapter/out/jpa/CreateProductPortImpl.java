package com.bookstore.product.adapter.out.jpa;

import com.bookstore.product.adapter.mapper.ProductEntityMapper;
import com.bookstore.product.adapter.out.jpa.entity.CategoryEntity;
import com.bookstore.product.adapter.out.jpa.entity.ProductEntity;
import com.bookstore.product.adapter.out.jpa.repository.CategoryRepository;
import com.bookstore.product.adapter.out.jpa.repository.ProductRepository;
import com.bookstore.product.application.port.out.CreateProductPort;
import com.bookstore.product.domain.Product;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class CreateProductPortImpl implements CreateProductPort {
    private final ProductRepository productRepository;
    private final CategoryRepository categoryRepository;
    private final ProductEntityMapper productEntityMapper;

    @Override
    public Product create(Product product) {
        Assert.notNull(product, () -> "Product must be provided.");

        ProductEntity entity = setExistingCategories(productEntityMapper.toProductEntity(product));
        ProductEntity saved = productRepository.save(entity);
        return productEntityMapper.toProduct(saved);
    }

    private ProductEntity setExistingCategories(ProductEntity product) {
        if (!CollectionUtils.isEmpty(product.getCategories())) {
            List<String> categoryNames = product.getCategories().stream()
                    .map(CategoryEntity::getName)
                    .collect(Collectors.toList());

            Map<String, CategoryEntity> categories = Maps.uniqueIndex(
                    categoryRepository.findAllById(categoryNames),
                    CategoryEntity::getName
            );

            ListIterator<CategoryEntity> iterator = product.getCategories().listIterator();
            while (iterator.hasNext()) {
                CategoryEntity category = iterator.next();
                CategoryEntity existingCategory = categories.get(category.getName());

                if (existingCategory != null) {
                    iterator.set(existingCategory);
                }
            }
        }

        return product;
    }
}

package com.bookstore.product.application.port.out;

import com.bookstore.product.domain.Category;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface PageCategoriesPort {
    Page<Category> page(Pageable pageable);
}

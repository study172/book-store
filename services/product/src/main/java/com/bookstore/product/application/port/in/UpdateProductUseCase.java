package com.bookstore.product.application.port.in;

import com.bookstore.product.domain.Product;
import com.bookstore.product.domain.command.UpdateProductCommand;

public interface UpdateProductUseCase {
    Product update(String id, UpdateProductCommand command);
}

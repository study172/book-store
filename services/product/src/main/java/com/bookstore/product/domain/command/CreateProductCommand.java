package com.bookstore.product.domain.command;

import com.bookstore.product.common.validation.annotation.RequiredProductDescriptionFields;
import com.bookstore.product.domain.Category;
import com.bookstore.product.domain.ProductDescription;
import com.bookstore.product.domain.ProductInfo;
import com.bookstore.product.domain.ProductPicture;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class CreateProductCommand {

    @Valid
    @NotNull(message = "{create-product-command.description.not-null}")
    @RequiredProductDescriptionFields(message = "{create-product-command.description.required-fields}")
    private ProductDescription description;

    @NotEmpty(message = "{create-product-command.categories.not-empty}")
    private List<@Valid Category> categories;

    @Valid
    @NotNull(message = "{create-product-command.product-info.not-null}")
    private ProductInfo productInfo;

    @Valid
    @NotNull(message = "{create-product-command.pictures.not-null}")
    private ProductPicture picture;
}

package com.bookstore.product.adapter.out.kafka.binding;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

public interface ReservationBinding {
    String RESERVATION_EVENTS = "reservation-events";

    @Output(RESERVATION_EVENTS)
    MessageChannel events();
}

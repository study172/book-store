package com.bookstore.product.adapter.out.kafka;

import com.bookstore.common.api.event.Events;
import com.bookstore.common.api.productapi.event.ReservationEvent;
import com.bookstore.product.adapter.mapper.ReservationMapper;
import com.bookstore.product.adapter.out.kafka.binding.ReservationBinding;
import com.bookstore.product.application.port.out.ReservationEventPort;
import com.bookstore.product.domain.Reservation;
import lombok.RequiredArgsConstructor;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class ReservationEventPortImpl implements ReservationEventPort {
    private final ReservationBinding binding;
    private final ReservationMapper mapper;

    @Override
    public void created(Reservation reservation) {
        ReservationEvent event = ReservationEvent.created(
                Events.Metadata.createDefault(),
                mapper.toReservationApi(reservation)
        );
        binding.events().send(new GenericMessage<>(event, Events.headersWithKey(reservation.getId())));
    }

    @Override
    public void updated(Reservation reservation) {
        ReservationEvent event = ReservationEvent.updated(
                Events.Metadata.createDefault(),
                mapper.toReservationApi(reservation)
        );
        binding.events().send(new GenericMessage<>(event, Events.headersWithKey(reservation.getId())));
    }

    @Override
    public void expired(String id) {
        ReservationEvent event = ReservationEvent.expired(Events.Metadata.createDefault(), id);
        binding.events().send(new GenericMessage<>(event, Events.headersWithKey(id)));
    }

    @Override
    public void cancelled(String id) {
        ReservationEvent event = ReservationEvent.cancelled(Events.Metadata.createDefault(), id);
        binding.events().send(new GenericMessage<>(event, Events.headersWithKey(id)));
    }

    @Override
    public void completed(String id) {
        ReservationEvent event = ReservationEvent.completed(Events.Metadata.createDefault(), id);
        binding.events().send(new GenericMessage<>(event, Events.headersWithKey(id)));
    }
}

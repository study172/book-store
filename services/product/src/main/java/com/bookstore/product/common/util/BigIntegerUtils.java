package com.bookstore.product.common.util;

import java.math.BigInteger;
import java.util.Objects;

public final class BigIntegerUtils {
    private BigIntegerUtils() {
        throw new UnsupportedOperationException();
    }

    public static boolean isLessThan(BigInteger source, BigInteger target) {
        Objects.requireNonNull(source);
        Objects.requireNonNull(target);

        return source.compareTo(target) < 0;
    }
}

package com.bookstore.product.application.port.in;

public interface DeleteProductUseCase {
    void delete(String id);
}

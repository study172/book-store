package com.bookstore.product.application.impl;

import com.bookstore.product.application.port.in.CompleteReservationUseCase;
import com.bookstore.product.application.port.out.DeleteReservationByIdPort;
import com.bookstore.product.application.port.out.FindReservationByIdPort;
import com.bookstore.product.application.port.out.ReservationEventPort;
import com.bookstore.product.common.exception.Exceptions;
import com.bookstore.product.domain.Reservation;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

@Component
@RequiredArgsConstructor
public class CompleteReservationUseCaseImpl implements CompleteReservationUseCase {
    private final FindReservationByIdPort findReservationByIdPort;
    private final DeleteReservationByIdPort deleteReservationByIdPort;
    private final ReservationEventPort reservationEventPort;

    @Override
    @Transactional(isolation = Isolation.REPEATABLE_READ)
    public void complete(String id) {
        Reservation reservation = findReservationByIdPort.find(id)
                .orElseThrow(() -> Exceptions.RESERVATION_NOT_FOUND_EXCEPTION.create()
                        .message("Reservation with id ''{}'' not found.", id)
                        .get());

        deleteReservationByIdPort.delete(reservation.getId());
        reservationEventPort.completed(reservation.getId());
    }
}

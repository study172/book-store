package com.bookstore.product.application.port.out;

public interface DeleteReservationByIdPort {
    void delete(String id);
}

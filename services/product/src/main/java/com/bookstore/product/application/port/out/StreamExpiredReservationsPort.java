package com.bookstore.product.application.port.out;

import com.bookstore.product.domain.Reservation;

import java.util.stream.Stream;

public interface StreamExpiredReservationsPort {
    Stream<Reservation> stream();
}

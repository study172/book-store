package com.bookstore.product.adapter.out.jpa;

import com.bookstore.product.adapter.mapper.CategoryEntityMapper;
import com.bookstore.product.adapter.out.jpa.entity.CategoryEntity;
import com.bookstore.product.adapter.out.jpa.repository.CategoryRepository;
import com.bookstore.product.application.port.out.UpdateCategoryPort;
import com.bookstore.product.domain.Category;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

@Component
@RequiredArgsConstructor
public class UpdateCategoryPortImpl implements UpdateCategoryPort {
    private final CategoryRepository repository;
    private final CategoryEntityMapper mapper;

    @Override
    public Category update(Category category) {
        Assert.notNull(category, () -> "Category must be provided.");
        Assert.hasText(category.getName(), () -> "Category name must be provided.");

        CategoryEntity saved = repository.save(mapper.toCategoryEntity(category));
        return mapper.toCategory(saved);
    }
}

package com.bookstore.product.application.impl;

import com.bookstore.product.application.port.in.UpdateProductUseCase;
import com.bookstore.product.application.port.out.FindProductByIdPort;
import com.bookstore.product.application.port.out.ProductEventPort;
import com.bookstore.product.application.port.out.UpdateProductPort;
import com.bookstore.product.common.exception.Exceptions;
import com.bookstore.product.domain.Product;
import com.bookstore.product.domain.command.UpdateProductCommand;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

@Component
@RequiredArgsConstructor
public class UpdateProductUseCaseImpl implements UpdateProductUseCase {
    private final FindProductByIdPort findProductByIdPort;
    private final UpdateProductPort updateProductPort;
    private final ProductEventPort productEventPort;

    @Override
    @Transactional(isolation = Isolation.REPEATABLE_READ)
    public Product update(String id, UpdateProductCommand command) {
        Product product = findProductByIdPort.find(id).orElseThrow(() ->
                Exceptions.PRODUCT_NOT_FOUND_EXCEPTION.create()
                        .message("Product with id ''{}'' not found.", id)
                        .get()
        );

        Product updated = updateProductPort.update(product.update(command));
        productEventPort.updated(updated);

        return updated;
    }
}

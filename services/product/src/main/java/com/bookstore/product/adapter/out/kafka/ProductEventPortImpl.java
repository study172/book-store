package com.bookstore.product.adapter.out.kafka;

import com.bookstore.common.api.event.Events;
import com.bookstore.common.api.productapi.event.ProductEvent;
import com.bookstore.product.adapter.mapper.ProductMapper;
import com.bookstore.product.adapter.out.kafka.binding.ProductBinging;
import com.bookstore.product.application.port.out.ProductEventPort;
import com.bookstore.product.domain.Product;
import lombok.RequiredArgsConstructor;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class ProductEventPortImpl implements ProductEventPort {
    private final ProductBinging productBinging;
    private final ProductMapper mapper;

    @Override
    public void created(Product product) {
        ProductEvent event = ProductEvent.created(Events.Metadata.createDefault(), mapper.toProductApi(product));
        productBinging.events().send(new GenericMessage<>(event, Events.headersWithKey(product.getId())));
    }

    @Override
    public void updated(Product product) {
        ProductEvent event = ProductEvent.updated(Events.Metadata.createDefault(), mapper.toProductApi(product));
        productBinging.events().send(new GenericMessage<>(event, Events.headersWithKey(product.getId())));
    }

    @Override
    public void deleted(String id) {
        ProductEvent event = ProductEvent.deleted(Events.Metadata.createDefault(), id);
        productBinging.events().send(new GenericMessage<>(event, Events.headersWithKey(id)));
    }
}

package com.bookstore.product.adapter.mapper;

import com.bookstore.product.adapter.out.jpa.entity.ReservationDetailsEntity;
import com.bookstore.product.adapter.out.jpa.entity.ReservationEntity;
import com.bookstore.product.common.mapper.CommonMapperConfig;
import com.bookstore.product.domain.Reservation;
import com.bookstore.product.domain.ReservationDetails;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.Collection;
import java.util.List;

@Mapper(config = CommonMapperConfig.class, uses = {ProductEntityMapper.class, CategoryEntityMapper.class})
public abstract class ReservationEntityMapper {

    @Mapping(target = "allProducts", ignore = true)
    public abstract Reservation toReservation(ReservationEntity entity);

    public abstract ReservationEntity toReservationEntity(Reservation reservation);

    public abstract List<ReservationDetails> toDetails(Collection<ReservationDetailsEntity> entities);

    public abstract List<ReservationDetailsEntity> toDetailsEntity(Collection<ReservationDetails> details);
}

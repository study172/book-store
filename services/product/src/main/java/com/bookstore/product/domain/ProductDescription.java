package com.bookstore.product.domain;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.util.List;

@Data
public class ProductDescription {
    @NotEmpty(message = "{product-description.values.not-empty}")
    private List<Description> values;
}

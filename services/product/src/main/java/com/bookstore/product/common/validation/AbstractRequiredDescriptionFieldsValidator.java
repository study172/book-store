package com.bookstore.product.common.validation;

import com.bookstore.product.domain.Description;
import com.github.sgreben.regex_builder.Pattern;

import javax.validation.ConstraintValidator;
import java.lang.annotation.Annotation;
import java.util.Collection;
import java.util.Set;

public abstract class AbstractRequiredDescriptionFieldsValidator<A extends Annotation, T>
        implements ConstraintValidator<A, T> {

    protected boolean containsRequiredFields(Collection<? extends Description> descriptions) {
        return getRequiredPatterns().stream()
                .allMatch(pattern -> descriptions.stream()
                        .map(Description::getName)
                        .filter(name -> pattern.matcher(name).matches())
                        .count() == 1);
    }

    protected abstract Set<Pattern> getRequiredPatterns();
}

package com.bookstore.product.domain;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;
import java.math.BigDecimal;
import java.math.BigInteger;

@Data
public class ProductInfo {
    @NotNull(message = "{product-info.price.not-null}")
    @Positive(message = "{product-info.price.positive}")
    private BigDecimal price;

    @NotNull(message = "{product-info.quantity.not-null}")
    @PositiveOrZero(message = "{product-info.quantity.positive-or-zero}")
    private BigInteger quantity;

    public void subtract(BigInteger quantity) {
        this.quantity = this.quantity.subtract(quantity);
    }

    public void add(BigInteger quantity) {
        this.quantity = this.quantity.add(quantity);
    }
}

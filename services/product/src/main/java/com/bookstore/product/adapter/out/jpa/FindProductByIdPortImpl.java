package com.bookstore.product.adapter.out.jpa;

import com.bookstore.product.adapter.mapper.ProductEntityMapper;
import com.bookstore.product.adapter.out.jpa.repository.ProductRepository;
import com.bookstore.product.application.port.out.FindProductByIdPort;
import com.bookstore.product.domain.Product;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class FindProductByIdPortImpl implements FindProductByIdPort {
    private final ProductRepository repository;
    private final ProductEntityMapper mapper;

    @Override
    public Optional<Product> find(String id) {
        Assert.hasText(id, () -> "Id must be provided.");

        return repository.findById(id).map(mapper::toProduct);
    }
}

package com.bookstore.product.adapter.mapper;

import com.bookstore.common.api.productapi.ProductApi;
import com.bookstore.product.common.mapper.CommonMapperConfig;
import com.bookstore.product.domain.Category;
import org.mapstruct.Mapper;

@Mapper(config = CommonMapperConfig.class)
public abstract class CategoryMapper {

    public abstract ProductApi.Category toCategoryApi(Category category);
}

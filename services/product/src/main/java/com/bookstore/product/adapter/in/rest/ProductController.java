package com.bookstore.product.adapter.in.rest;

import com.bookstore.common.api.Page;
import com.bookstore.common.api.productapi.ProductApi;
import com.bookstore.product.adapter.mapper.ProductMapper;
import com.bookstore.product.application.port.in.CreateProductUseCase;
import com.bookstore.product.application.port.in.DeleteProductUseCase;
import com.bookstore.product.application.port.in.GetProductByIdUseCase;
import com.bookstore.product.application.port.in.PageProductsUseCase;
import com.bookstore.product.application.port.in.UpdateProductUseCase;
import com.bookstore.product.common.mapper.PageMapper;
import com.bookstore.product.domain.command.CreateProductCommand;
import com.bookstore.product.domain.command.UpdateProductCommand;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import static com.bookstore.product.application.support.SecurityUtils.HAS_COMMON_ACCESS;
import static com.bookstore.product.application.support.SecurityUtils.HAS_CONTENT_ACCESS;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/products")
public class ProductController {
    private final GetProductByIdUseCase getProductByIdUseCase;
    private final PageProductsUseCase pageProductsUseCase;
    private final CreateProductUseCase createProductUseCase;
    private final UpdateProductUseCase updateProductUseCase;
    private final DeleteProductUseCase deleteProductUseCase;
    private final ProductMapper mapper;
    private final PageMapper pageMapper;

    @GetMapping("/{id}")
    @PreAuthorize(HAS_COMMON_ACCESS)
    public ProductApi.Product get(@PathVariable("id") String id) {
        return mapper.toProductApi(getProductByIdUseCase.get(id));
    }

    @GetMapping
    @PreAuthorize(HAS_COMMON_ACCESS)
    public Page<ProductApi.Product> get(@PageableDefault(sort = "id") Pageable pageable) {
        return pageMapper.toPage(pageProductsUseCase.page(pageable).map(mapper::toProductApi));
    }

    @PostMapping
    @PreAuthorize(HAS_CONTENT_ACCESS)
    public ProductApi.Product create(@RequestBody @Validated CreateProductCommand command) {
        return mapper.toProductApi(createProductUseCase.create(command));
    }

    @PutMapping("/{id}")
    @PreAuthorize(HAS_CONTENT_ACCESS)
    public ProductApi.Product update(@PathVariable("id") String id,
                                     @RequestBody @Validated UpdateProductCommand command) {
        return mapper.toProductApi(updateProductUseCase.update(id, command));
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize(HAS_CONTENT_ACCESS)
    public void delete(@PathVariable("id") String id) {
        deleteProductUseCase.delete(id);
    }
}

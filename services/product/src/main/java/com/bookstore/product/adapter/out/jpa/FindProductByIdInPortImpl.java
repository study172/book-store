package com.bookstore.product.adapter.out.jpa;

import com.bookstore.product.adapter.mapper.ProductEntityMapper;
import com.bookstore.product.adapter.out.jpa.repository.ProductRepository;
import com.bookstore.product.application.port.out.FindProductByIdInPort;
import com.bookstore.product.domain.Product;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.util.Collection;
import java.util.List;

@Component
@RequiredArgsConstructor
public class FindProductByIdInPortImpl implements FindProductByIdInPort {
    private final ProductRepository repository;
    private final ProductEntityMapper mapper;

    @Override
    public List<Product> find(Collection<String> ids) {
        Assert.notNull(ids, () -> "Ids must be provided.");

        return mapper.toProductList(repository.findAllById(ids));
    }
}

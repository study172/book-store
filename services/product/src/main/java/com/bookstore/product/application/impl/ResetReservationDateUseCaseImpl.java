package com.bookstore.product.application.impl;

import com.bookstore.product.application.port.in.ResetReservationDateUseCase;
import com.bookstore.product.application.port.out.FindReservationByIdPort;
import com.bookstore.product.application.port.out.UpdateReservationPort;
import com.bookstore.product.common.exception.Exceptions;
import com.bookstore.product.domain.Reservation;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

@Component
@RequiredArgsConstructor
public class ResetReservationDateUseCaseImpl implements ResetReservationDateUseCase {
    private final FindReservationByIdPort findReservationByIdPort;
    private final UpdateReservationPort updateReservationPort;

    @Override
    @Transactional(isolation = Isolation.REPEATABLE_READ)
    public void reset(String id) {
        Reservation reservation = findReservationByIdPort.find(id)
                .orElseThrow(() -> Exceptions.RESERVATION_NOT_FOUND_EXCEPTION.create()
                        .message("Reservation with id ''{}'' not found.", id)
                        .get());

        reservation.setReservedTo(null);
        updateReservationPort.update(reservation);
    }
}

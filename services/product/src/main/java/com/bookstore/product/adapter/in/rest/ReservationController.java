package com.bookstore.product.adapter.in.rest;

import com.bookstore.common.api.productapi.ProductApi;
import com.bookstore.product.adapter.mapper.ReservationMapper;
import com.bookstore.product.application.port.in.CreateReservationUseCase;
import com.bookstore.product.application.port.in.GetReservationByIdUseCase;
import com.bookstore.product.application.port.in.UpdateReservationUseCase;
import com.bookstore.product.domain.command.CreateReservationCommand;
import com.bookstore.product.domain.command.UpdateReservationCommand;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.bookstore.product.application.support.SecurityUtils.HAS_COMMON_ACCESS;
import static com.bookstore.product.application.support.SecurityUtils.HAS_CONTENT_ACCESS;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/reservations")
public class ReservationController {
    private final GetReservationByIdUseCase getReservationByIdUseCase;
    private final CreateReservationUseCase createReservationUseCase;
    private final UpdateReservationUseCase updateReservationUseCase;
    private final ReservationMapper mapper;

    @GetMapping("/{id}")
    @PreAuthorize(HAS_COMMON_ACCESS)
    public ProductApi.Reservation get(@PathVariable("id") String id) {
        return mapper.toReservationApi(getReservationByIdUseCase.get(id));
    }

    @PostMapping
    @PreAuthorize(HAS_CONTENT_ACCESS)
    public ProductApi.Reservation create(@RequestBody @Validated CreateReservationCommand command) {
        return mapper.toReservationApi(createReservationUseCase.create(command));
    }

    @PutMapping("/{id}")
    @PreAuthorize(HAS_CONTENT_ACCESS)
    public ProductApi.Reservation update(@PathVariable("id") String id,
                                         @RequestBody @Validated UpdateReservationCommand command) {
        return mapper.toReservationApi(updateReservationUseCase.update(id, command));
    }
}

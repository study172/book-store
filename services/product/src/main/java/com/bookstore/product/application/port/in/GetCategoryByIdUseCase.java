package com.bookstore.product.application.port.in;

import com.bookstore.product.domain.Category;

public interface GetCategoryByIdUseCase {
    Category get(String id);
}

package com.bookstore.product.adapter.in.kafka;

import com.bookstore.common.api.orderapi.event.OrderEvent;
import com.bookstore.product.adapter.in.kafka.binding.OrderBinding;
import com.bookstore.product.application.port.in.CancelReservationUseCase;
import com.bookstore.product.application.port.in.CompleteReservationUseCase;
import com.bookstore.product.application.port.in.ResetReservationDateUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class OrderEventListener {
    private final CancelReservationUseCase cancelReservationUseCase;
    private final CompleteReservationUseCase completeReservationUseCase;
    private final ResetReservationDateUseCase resetReservationDateUseCase;

    @StreamListener(OrderBinding.ORDER_EVENTS)
    public void handle(OrderEvent event) {
        OrderEvent.Type type = event.getType();

        if (type == OrderEvent.Type.PAID) {
            String reservationId = event.getPaid().getOrder().getOrderDetails().getReservation().getId();

            resetReservationDateUseCase.reset(reservationId);
        } else if (type == OrderEvent.Type.COMPLETED) {
            String reservationId = event.getCompleted().getOrder().getOrderDetails().getReservation().getId();

            completeReservationUseCase.complete(reservationId);
        } else if (type == OrderEvent.Type.CANCELLED) {
            String reservationId = event.getCancelled().getOrder().getOrderDetails().getReservation().getId();

            cancelReservationUseCase.cancel(reservationId);
        }

    }
}

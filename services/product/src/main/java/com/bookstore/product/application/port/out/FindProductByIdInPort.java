package com.bookstore.product.application.port.out;

import com.bookstore.product.domain.Product;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface FindProductByIdInPort {
    List<Product> find(Collection<String> ids);
}

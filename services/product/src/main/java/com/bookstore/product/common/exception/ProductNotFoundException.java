package com.bookstore.product.common.exception;

import com.bookstore.common.helper.exception.BaseException;

public class ProductNotFoundException extends BaseException {
    private static final long serialVersionUID = -520602162805563954L;

    public ProductNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}

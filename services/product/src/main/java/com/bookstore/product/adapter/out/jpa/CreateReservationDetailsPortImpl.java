package com.bookstore.product.adapter.out.jpa;

import com.bookstore.product.adapter.mapper.ReservationEntityMapper;
import com.bookstore.product.adapter.out.jpa.entity.ReservationDetailsEntity;
import com.bookstore.product.adapter.out.jpa.repository.ReservationDetailsRepository;
import com.bookstore.product.application.port.out.CreateReservationDetailsPort;
import com.bookstore.product.domain.ReservationDetails;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.util.List;

@Component
@RequiredArgsConstructor
public class CreateReservationDetailsPortImpl implements CreateReservationDetailsPort {
    private final ReservationDetailsRepository repository;
    private final ReservationEntityMapper mapper;

    @Override
    public List<ReservationDetails> create(List<ReservationDetails> details) {
        Assert.notEmpty(details, () -> "Reservation details must be provided.");
        Assert.noNullElements(details, () -> "Reservation details can not contain null elements.");

        List<ReservationDetailsEntity> entities = mapper.toDetailsEntity(details);
        return mapper.toDetails(repository.saveAll(entities));
    }
}

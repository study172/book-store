package com.bookstore.product.adapter.out.jpa;

import com.bookstore.product.adapter.out.jpa.repository.ProductRepository;
import com.bookstore.product.application.port.out.DeleteProductPort;
import com.bookstore.product.domain.Product;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

@Component
@RequiredArgsConstructor
public class DeleteProductPortImpl implements DeleteProductPort {
    private final ProductRepository repository;

    @Override
    public void delete(Product product) {
        Assert.notNull(product, () -> "Product must be provided.");
        Assert.hasText(product.getId(), () -> "Product id must be provided.");

        repository.deleteById(product.getId());
    }
}

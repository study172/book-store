package com.bookstore.product.application.port.out;

import com.bookstore.product.domain.ReservationDetails;

import java.util.List;

public interface UpdateReservationDetailsPort {
    List<ReservationDetails> update(List<ReservationDetails> details);
}

package com.bookstore.product.application.impl;

import com.bookstore.product.application.port.in.CreateProductUseCase;
import com.bookstore.product.application.port.out.CreateProductPort;
import com.bookstore.product.application.port.out.ProductEventPort;
import com.bookstore.product.domain.Product;
import com.bookstore.product.domain.command.CreateProductCommand;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

@Component
@RequiredArgsConstructor
public class CreateProductUseCaseImpl implements CreateProductUseCase {
    private final CreateProductPort createProductPort;
    private final ProductEventPort productEventPort;

    @Override
    @Transactional(isolation = Isolation.REPEATABLE_READ)
    public Product create(CreateProductCommand command) {
        Product product = createProductPort.create(new Product(command));
        productEventPort.created(product);

        return product;
    }
}

package com.bookstore.product.application.port.in;

import com.bookstore.product.domain.Product;

public interface GetProductByIdUseCase {
    Product get(String id);
}

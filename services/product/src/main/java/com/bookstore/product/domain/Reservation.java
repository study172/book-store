package com.bookstore.product.domain;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.time.Instant;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
public class Reservation {
    private String id;
    private Collection<ReservationDetails> details;
    private Instant createdAt;
    private Instant reservedTo;
    private String createdBy;
    private String modifiedBy;
    private Instant modifiedAt;

    public Reservation(Collection<ReservationDetails> details, Instant reservedTo) {
        this.details = details;
        this.reservedTo = reservedTo;
    }

    public void cancel() {
        details.forEach(ReservationDetails::cancel);
    }

    public Collection<Product> getAllProducts() {
        return details.stream()
                .map(ReservationDetails::getProduct)
                .collect(Collectors.toList());
    }

    public void update(List<ReservationDetails> details) {
        this.details = details;
    }
}

package com.bookstore.product.adapter.out.kafka.binding;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

public interface CategoryBinding {
    String CATEGORY_EVENTS = "category-events";

    @Output(CATEGORY_EVENTS)
    MessageChannel events();
}

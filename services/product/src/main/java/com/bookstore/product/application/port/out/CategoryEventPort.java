package com.bookstore.product.application.port.out;

import com.bookstore.product.domain.Category;

public interface CategoryEventPort {
    void created(Category category);

    void updated(Category category);

    void deleted(String id);
}

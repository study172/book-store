package com.bookstore.product.adapter.out.jpa;

import com.bookstore.product.adapter.out.jpa.repository.CategoryRepository;
import com.bookstore.product.application.port.out.DeleteCategoryPort;
import com.bookstore.product.domain.Category;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

@Component
@RequiredArgsConstructor
public class DeleteCategoryPortImpl implements DeleteCategoryPort {
    private final CategoryRepository repository;

    @Override
    public void delete(Category category) {
        Assert.notNull(category, () -> "Category must be provided.");
        Assert.hasText(category.getName(), () -> "Category name must be provided.");

        repository.deleteById(category.getName());
    }
}

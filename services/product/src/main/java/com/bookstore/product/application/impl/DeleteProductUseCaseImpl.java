package com.bookstore.product.application.impl;

import com.bookstore.product.application.port.in.DeleteProductUseCase;
import com.bookstore.product.application.port.out.DeleteProductPort;
import com.bookstore.product.application.port.out.FindProductByIdPort;
import com.bookstore.product.application.port.out.ProductEventPort;
import com.bookstore.product.common.exception.Exceptions;
import com.bookstore.product.domain.Product;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

@Component
@RequiredArgsConstructor
public class DeleteProductUseCaseImpl implements DeleteProductUseCase {
    private final FindProductByIdPort findProductByIdPort;
    private final DeleteProductPort deleteProductPort;
    private final ProductEventPort productEventPort;

    @Override
    @Transactional(isolation = Isolation.REPEATABLE_READ)
    public void delete(String id) {
        Product product = findProductByIdPort.find(id).orElseThrow(() ->
                Exceptions.PRODUCT_NOT_FOUND_EXCEPTION.create()
                        .message("Product with id ''{}'' not found.", id)
                        .get()
        );

        deleteProductPort.delete(product);
        productEventPort.deleted(id);
    }
}

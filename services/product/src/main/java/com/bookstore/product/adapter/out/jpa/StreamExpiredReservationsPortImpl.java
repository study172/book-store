package com.bookstore.product.adapter.out.jpa;

import com.bookstore.product.adapter.mapper.ReservationEntityMapper;
import com.bookstore.product.adapter.out.jpa.repository.ReservationRepository;
import com.bookstore.product.application.port.out.StreamExpiredReservationsPort;
import com.bookstore.product.domain.Reservation;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.time.Clock;
import java.time.Instant;
import java.util.stream.Stream;

@Component
@RequiredArgsConstructor
public class StreamExpiredReservationsPortImpl implements StreamExpiredReservationsPort {
    private final ReservationRepository reservationRepository;
    private final ReservationEntityMapper mapper;
    private final Clock clock;

    @Override
    public Stream<Reservation> stream() {
        return reservationRepository.streamTop50ByReservedToLessThanEqualOrderByReservedTo(Instant.now(clock))
                .map(mapper::toReservation);
    }
}

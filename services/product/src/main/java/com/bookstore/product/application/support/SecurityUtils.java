package com.bookstore.product.application.support;

public class SecurityUtils {
    public static final String HAS_COMMON_ACCESS =
            "(hasRole('ADMIN') or hasRole('CONTENT_MANAGER') or hasRole('USER'))";

    public static final String HAS_CONTENT_ACCESS = "(hasRole('ADMIN') or hasRole('CONTENT_MANAGER'))";
}

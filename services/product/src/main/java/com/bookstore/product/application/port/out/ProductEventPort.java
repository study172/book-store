package com.bookstore.product.application.port.out;

import com.bookstore.product.domain.Product;

public interface ProductEventPort {
    void created(Product product);

    void updated(Product product);

    void deleted(String id);
}

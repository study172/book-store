package com.bookstore.product.adapter.mapper;

import com.bookstore.product.adapter.out.jpa.entity.CategoryEntity;
import com.bookstore.product.common.mapper.CommonMapperConfig;
import com.bookstore.product.domain.Category;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(config = CommonMapperConfig.class)
public abstract class CategoryEntityMapper {

    @Mapping(target = "update", ignore = true)
    public abstract Category toCategory(CategoryEntity entity);

    public abstract CategoryEntity toCategoryEntity(Category category);
}

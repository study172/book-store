package com.bookstore.product.adapter.mapper;

import com.bookstore.product.adapter.out.jpa.entity.ProductEntity;
import com.bookstore.product.common.mapper.CommonMapperConfig;
import com.bookstore.product.domain.Product;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.Collection;
import java.util.List;

@Mapper(config = CommonMapperConfig.class, uses = CategoryEntityMapper.class)
public abstract class ProductEntityMapper {

    @Mapping(target = "update", ignore = true)
    public abstract Product toProduct(ProductEntity entity);

    public abstract List<Product> toProductList(Collection<ProductEntity> entities);

    public abstract ProductEntity toProductEntity(Product product);

    public abstract Collection<ProductEntity> toProductEntities(Collection<Product> products);
}

package com.bookstore.product.adapter.out.jpa.entity;

import lombok.Data;

@Data
public class PictureEntity {
    private Long number;
    private String url;
}

package com.bookstore.product.application.port.in;

public interface CancelReservationUseCase {
    void cancel(String id);
}

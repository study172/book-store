package com.bookstore.product.application.port.in;

import com.bookstore.product.domain.Reservation;
import com.bookstore.product.domain.command.UpdateReservationCommand;

public interface UpdateReservationUseCase {
    Reservation update(String id, UpdateReservationCommand command);
}

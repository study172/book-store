package com.bookstore.product.domain;

import com.bookstore.product.common.validation.annotation.RequiredCategoryDescriptionFields;
import com.bookstore.product.domain.command.CreateCategoryCommand;
import com.bookstore.product.domain.command.UpdateCategoryCommand;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.Instant;

@Data
@NoArgsConstructor
public class Category {
    @NotBlank(message = "{category.name.not-blank}")
    private String name;

    @Valid
    @NotNull(message = "{category.description.not-null}")
    @RequiredCategoryDescriptionFields(message = "{category.description.required-fields}")
    private CategoryDescription description;

    private Instant createdAt;

    private Instant modifiedAt;

    private String createdBy;

    private String modifiedBy;

    public Category(CreateCategoryCommand command) {
        this.name = command.getName();
        this.description = command.getDescription();
    }

    public Category update(UpdateCategoryCommand command) {
        this.description = command.getDescription();

        return this;
    }
}

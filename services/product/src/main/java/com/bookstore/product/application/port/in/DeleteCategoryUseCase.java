package com.bookstore.product.application.port.in;

public interface DeleteCategoryUseCase {
    void delete(String id);
}

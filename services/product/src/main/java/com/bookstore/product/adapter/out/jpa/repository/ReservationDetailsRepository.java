package com.bookstore.product.adapter.out.jpa.repository;

import com.bookstore.product.adapter.out.jpa.entity.ReservationDetailsEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ReservationDetailsRepository extends JpaRepository<ReservationDetailsEntity, String> {
}

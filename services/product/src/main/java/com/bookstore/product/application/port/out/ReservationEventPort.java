package com.bookstore.product.application.port.out;

import com.bookstore.product.domain.Reservation;

public interface ReservationEventPort {
    void created(Reservation reservation);

    void updated(Reservation reservation);

    void expired(String id);

    void cancelled(String id);

    void completed(String id);
}

package com.bookstore.product.application.port.in;

import com.bookstore.product.domain.Category;
import com.bookstore.product.domain.command.CreateCategoryCommand;

public interface CreateCategoryUseCase {
    Category create(CreateCategoryCommand command);
}

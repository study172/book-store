package com.bookstore.product.application.port.out;

import com.bookstore.product.domain.Product;

import java.util.Optional;

public interface FindProductByIdPort {
    Optional<Product> find(String id);
}

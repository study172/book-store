package com.bookstore.product.application.support;

import com.bookstore.product.application.port.out.CreateReservationPort;
import com.bookstore.product.application.port.out.FindProductByIdInPort;
import com.bookstore.product.application.port.out.UpdateProductPort;
import com.bookstore.product.application.port.out.UpdateReservationDetailsPort;
import com.bookstore.product.application.port.out.UpdateReservationPort;
import com.bookstore.product.common.exception.Exceptions;
import com.bookstore.product.domain.Product;
import com.bookstore.product.domain.Reservation;
import com.bookstore.product.domain.ReservationDetails;
import com.bookstore.product.domain.command.ProductReservation;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.math.BigInteger;
import java.time.Clock;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class ReservationHelperImpl implements ReservationHelper {
    private static final Duration DEFAULT_RESERVATION_DURATION = Duration.ofDays(2);

    private final FindProductByIdInPort findProductByIdInPort;
    private final CreateReservationPort createReservationPort;
    private final UpdateReservationPort updateReservationPort;
    private final UpdateProductPort updateProductPort;
    private final UpdateReservationDetailsPort updateReservationDetailsPort;
    private final Clock clock;

    @Override
    public Reservation create(Collection<ProductReservation> reservations) {
        Map<String, BigInteger> idToQuantity = asMap(reservations);
        List<Product> products = findProductByIdInPort.find(idToQuantity.keySet());

        checkProducts(idToQuantity.keySet(), products);

        List<ReservationDetails> details = createDetails(products, idToQuantity);
        updateProductPort.updateAll(products);

        return createReservationPort.create(new Reservation(details, createReservationDate()));
    }

    @Override
    public Reservation update(Reservation reservation,
                              Collection<ProductReservation> reservations) {
        List<ReservationDetails> result = new ArrayList<>(reservation.getDetails());

        Map<String, BigInteger> idToQuantity = asMap(reservations);
        List<Product> products = findProductByIdInPort.find(idToQuantity.keySet());

        checkProducts(idToQuantity.keySet(), products);

        List<Product> unreserved = unreserve(result, idToQuantity);
        if (!unreserved.isEmpty()) {
            updateProductPort.updateAll(unreserved);
        }

        List<ReservationDetails> updatedDetails = updateReservations(result, idToQuantity);
        if (!updatedDetails.isEmpty()) {
            updateProductPort.updateAll(updatedDetails.stream()
                    .map(ReservationDetails::getProduct)
                    .collect(Collectors.toList()));

            updateReservationDetailsPort.update(updatedDetails);
        }

        List<ReservationDetails> newReservations = createDetails(result, products, idToQuantity);

        result.addAll(newReservations);

        reservation.update(result);

        return updateReservationPort.update(reservation);
    }

    private Map<String, BigInteger> asMap(Collection<ProductReservation> reservations) {
        return reservations.stream()
                .collect(Collectors.toMap(ProductReservation::getProductId, ProductReservation::getQuantity));
    }

    private void checkProducts(Collection<String> ids, List<Product> products) {
        var list = new ArrayList<>(ids);
        list.removeAll(products.stream().map(Product::getId).collect(Collectors.toList()));

        if (!list.isEmpty()) {
            Exceptions.PRODUCT_NOT_FOUND_EXCEPTION.create()
                    .message("Products with ids {} not found", list)
                    .doThrow();
        }
    }

    private List<ReservationDetails> createDetails(List<Product> products, Map<String, BigInteger> idToQuantity) {
        return products.stream()
                .map(product -> product.reserve(idToQuantity.get(product.getId())))
                .collect(Collectors.toList());
    }

    private List<ReservationDetails> updateReservations(Collection<ReservationDetails> details,
                                                        Map<String, BigInteger> idToQuantity) {
        List<ReservationDetails> forUpdate = new ArrayList<>();

        for (ReservationDetails detail : details) {
            if (detail.updateReservation(idToQuantity.get(detail.getProduct().getId()))) {
                forUpdate.add(detail);
            }
        }

        return forUpdate;
    }

    private List<Product> unreserve(Collection<ReservationDetails> details, Map<String, BigInteger> idToQuantity) {
        List<Product> unreserved = new ArrayList<>();

        Iterator<ReservationDetails> iterator = details.iterator();
        while (iterator.hasNext()) {
            ReservationDetails detail = iterator.next();

            if (!idToQuantity.containsKey(detail.getProduct().getId())) {
                detail.cancel();
                unreserved.add(detail.getProduct());

                iterator.remove();
            }
        }

        return unreserved;
    }

    private List<ReservationDetails> createDetails(Collection<ReservationDetails> existing,
                                                   List<Product> products,
                                                   Map<String, BigInteger> idToQuantity) {
        Map<String, ReservationDetails> productIdToDetails = existing.stream()
                .collect(Collectors.toMap(r -> r.getProduct().getId(), Function.identity()));

        return products.stream()
                .filter(it -> !productIdToDetails.containsKey(it.getId()))
                .map(product -> product.reserve(idToQuantity.get(product.getId())))
                .collect(Collectors.toList());
    }

    private Instant createReservationDate() {
        return Instant.now(clock).plus(DEFAULT_RESERVATION_DURATION);
    }
}

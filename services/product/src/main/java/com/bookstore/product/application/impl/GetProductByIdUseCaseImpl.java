package com.bookstore.product.application.impl;

import com.bookstore.product.application.port.in.GetProductByIdUseCase;
import com.bookstore.product.application.port.out.FindProductByIdPort;
import com.bookstore.product.common.exception.Exceptions;
import com.bookstore.product.domain.Product;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@RequiredArgsConstructor
public class GetProductByIdUseCaseImpl implements GetProductByIdUseCase {
    private final FindProductByIdPort findProductByIdPort;

    @Override
    @Transactional(readOnly = true)
    public Product get(String id) {
        return findProductByIdPort.find(id).orElseThrow(() ->
                Exceptions.PRODUCT_NOT_FOUND_EXCEPTION.create()
                        .message("Product with id ''{}'' not found.", id)
                        .get()
        );
    }
}

package com.bookstore.product.application.port.in;

import com.bookstore.product.domain.Category;
import com.bookstore.product.domain.command.UpdateCategoryCommand;

public interface UpdateCategoryUseCase {
    Category update(String id, UpdateCategoryCommand command);
}

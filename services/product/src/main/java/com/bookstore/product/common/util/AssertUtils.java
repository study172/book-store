package com.bookstore.product.common.util;

import java.util.Collection;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.function.Supplier;

public final class AssertUtils {
    private AssertUtils() {
        throw new UnsupportedOperationException();
    }

    public static <T> void elementsMatchPredicate(Collection<T> col, Predicate<T> p, Supplier<String> message) {
        Objects.requireNonNull(col);
        Objects.requireNonNull(p);
        Objects.requireNonNull(message);

        for (T t : col) {
            if (!p.test(t)) {
                throw new IllegalArgumentException(message.get());
            }
        }
    }
}

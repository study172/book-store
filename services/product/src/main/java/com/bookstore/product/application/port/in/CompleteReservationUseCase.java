package com.bookstore.product.application.port.in;

public interface CompleteReservationUseCase {
    void complete(String id);
}

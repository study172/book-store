package com.bookstore.product.application.port.in;

import com.bookstore.product.domain.Product;
import com.bookstore.product.domain.command.CreateProductCommand;

public interface CreateProductUseCase {
    Product create(CreateProductCommand command);
}

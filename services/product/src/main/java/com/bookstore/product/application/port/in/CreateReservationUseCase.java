package com.bookstore.product.application.port.in;

import com.bookstore.product.domain.Reservation;
import com.bookstore.product.domain.command.CreateReservationCommand;

public interface CreateReservationUseCase {
    Reservation create(CreateReservationCommand command);
}

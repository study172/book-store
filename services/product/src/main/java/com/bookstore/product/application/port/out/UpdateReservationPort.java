package com.bookstore.product.application.port.out;

import com.bookstore.product.domain.Reservation;

public interface UpdateReservationPort {
    Reservation update(Reservation reservation);
}

package com.bookstore.product.application.port.out;

import com.bookstore.product.domain.Product;

public interface DeleteProductPort {
    void delete(Product product);
}

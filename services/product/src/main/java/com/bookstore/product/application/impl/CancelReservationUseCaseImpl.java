package com.bookstore.product.application.impl;

import com.bookstore.product.application.port.in.CancelReservationUseCase;
import com.bookstore.product.application.port.out.DeleteReservationByIdPort;
import com.bookstore.product.application.port.out.FindReservationByIdPort;
import com.bookstore.product.application.port.out.ReservationEventPort;
import com.bookstore.product.application.port.out.UpdateProductPort;
import com.bookstore.product.common.exception.Exceptions;
import com.bookstore.product.domain.Reservation;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

@Component
@RequiredArgsConstructor
public class CancelReservationUseCaseImpl implements CancelReservationUseCase {
    private final FindReservationByIdPort findReservationByIdPort;
    private final UpdateProductPort updateProductPort;
    private final DeleteReservationByIdPort deleteReservationByIdPort;
    private final ReservationEventPort reservationEventPort;

    @Override
    @Transactional(isolation = Isolation.REPEATABLE_READ)
    public void cancel(String id) {
        findReservationByIdPort.find(id).ifPresent(reservation -> {
            reservation.cancel();

            updateProductPort.updateAll(reservation.getAllProducts());
            deleteReservationByIdPort.delete(reservation.getId());
            reservationEventPort.cancelled(reservation.getId());
        });
    }
}

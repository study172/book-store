package com.bookstore.product.domain;

import lombok.Data;
import org.hibernate.validator.constraints.URL;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;

@Data
public class Picture {
    @NotNull(message = "{picture.number.not-null}")
    @PositiveOrZero(message = "{picture.number.positive-or-zero}")
    private Long number;

    @NotNull(message = "{picture.uri.not-null}")
    @URL(message = "{picture.url.is-url}")
    private String url;
}

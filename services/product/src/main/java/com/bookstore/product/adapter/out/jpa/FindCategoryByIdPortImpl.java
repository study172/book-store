package com.bookstore.product.adapter.out.jpa;

import com.bookstore.product.adapter.mapper.CategoryEntityMapper;
import com.bookstore.product.adapter.out.jpa.repository.CategoryRepository;
import com.bookstore.product.application.port.out.FindCategoryByIdPort;
import com.bookstore.product.domain.Category;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class FindCategoryByIdPortImpl implements FindCategoryByIdPort {
    private final CategoryRepository repository;
    private final CategoryEntityMapper mapper;

    @Override
    public Optional<Category> find(String id) {
        Assert.hasText(id, () -> "Id must be provided.");

        return repository.findById(id).map(mapper::toCategory);
    }
}

package com.bookstore.product.application.impl;

import com.bookstore.product.application.port.in.DeleteCategoryUseCase;
import com.bookstore.product.application.port.out.CategoryEventPort;
import com.bookstore.product.application.port.out.DeleteCategoryPort;
import com.bookstore.product.application.port.out.FindCategoryByIdPort;
import com.bookstore.product.common.exception.Exceptions;
import com.bookstore.product.domain.Category;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

@Component
@RequiredArgsConstructor
public class DeleteCategoryUseCaseImpl implements DeleteCategoryUseCase {
    private final FindCategoryByIdPort findCategoryByIdPort;
    private final DeleteCategoryPort deleteCategoryPort;
    private final CategoryEventPort eventPort;

    @Override
    @Transactional(isolation = Isolation.REPEATABLE_READ)
    public void delete(String id) {
        Category category = findCategoryByIdPort.find(id).orElseThrow(() ->
                Exceptions.CATEGORY_NOT_FOUND_EXCEPTION.create()
                        .message("Category with id ''{}'' not found.", id)
                        .get()
        );

        deleteCategoryPort.delete(category);
        eventPort.deleted(id);
    }
}

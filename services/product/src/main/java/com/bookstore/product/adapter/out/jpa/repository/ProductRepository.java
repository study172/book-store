package com.bookstore.product.adapter.out.jpa.repository;

import com.bookstore.product.adapter.out.jpa.entity.ProductEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends JpaRepository<ProductEntity, String> {
}

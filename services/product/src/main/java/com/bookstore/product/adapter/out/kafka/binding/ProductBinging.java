package com.bookstore.product.adapter.out.kafka.binding;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

public interface ProductBinging {
    String PRODUCT_EVENTS = "product-events";

    @Output(PRODUCT_EVENTS)
    MessageChannel events();
}

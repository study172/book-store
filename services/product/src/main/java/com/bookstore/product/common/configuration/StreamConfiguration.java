package com.bookstore.product.common.configuration;

import com.bookstore.product.adapter.in.kafka.binding.OrderBinding;
import com.bookstore.product.adapter.out.kafka.binding.CategoryBinding;
import com.bookstore.product.adapter.out.kafka.binding.ProductBinging;
import com.bookstore.product.adapter.out.kafka.binding.ReservationBinding;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableBinding({
        CategoryBinding.class,
        ProductBinging.class,
        ReservationBinding.class,
        OrderBinding.class
})
public class StreamConfiguration {
}

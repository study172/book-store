package com.bookstore.product.application.port.out;

import com.bookstore.product.domain.ReservationDetails;

import java.util.List;

public interface CreateReservationDetailsPort {
    List<ReservationDetails> create(List<ReservationDetails> details);
}

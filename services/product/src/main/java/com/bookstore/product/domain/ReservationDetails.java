package com.bookstore.product.domain;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigInteger;

@Data
@NoArgsConstructor
public class ReservationDetails {
    private String id;
    private Product product;
    private BigInteger quantity;

    public ReservationDetails(Product product, BigInteger quantity) {
        this.product = product;
        this.quantity = quantity;
    }

    public void cancel() {
        product.addProducts(quantity);
    }

    public boolean updateReservation(BigInteger quantity) {
        int compare = this.quantity.compareTo(quantity);

        if (compare != 0) {
            BigInteger diff = this.quantity.subtract(quantity);

            setQuantity(quantity);

            product.addProducts(diff);

            return true;
        }

        return false;
    }
}

CREATE TABLE _order
(
    id              TEXT PRIMARY KEY,
    order_details   JSONB,
    payment_details JSONB,
    status          TEXT,

    created_at      TIMESTAMP WITHOUT TIME ZONE,
    modified_at     TIMESTAMP WITHOUT TIME ZONE,
    created_by      TEXT,
    modified_by     TEXT
);

package com.bookstore.order.domain.command;

import lombok.Data;

import javax.validation.Valid;
import java.util.List;

@Data
public class UpdateOrderCommand {
    private List<@Valid OrderProduct> orderProducts;
}

package com.bookstore.order.application.port.in;

import com.bookstore.order.domain.Order;

public interface ConfirmOrderUseCase {
    Order confirm(String id);
}

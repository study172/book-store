package com.bookstore.order.common.exception;

import com.bookstore.common.helper.exception.BaseException;

public class OrderNotFoundException extends BaseException {
    private static final long serialVersionUID = 5697435493018772393L;

    public OrderNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}

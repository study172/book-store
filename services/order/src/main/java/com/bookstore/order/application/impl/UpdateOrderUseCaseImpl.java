package com.bookstore.order.application.impl;

import com.bookstore.authstarter.service.SecurityService;
import com.bookstore.order.application.port.in.UpdateOrderUseCase;
import com.bookstore.order.application.port.out.CreateReservationPort;
import com.bookstore.order.application.port.out.FindOrderByIdPort;
import com.bookstore.order.application.port.out.OrderEventPort;
import com.bookstore.order.application.port.out.UpdateOrderPort;
import com.bookstore.order.application.port.out.UpdateReservationPort;
import com.bookstore.order.common.exception.Exceptions;
import com.bookstore.order.domain.Order;
import com.bookstore.order.domain.OrderDetails;
import com.bookstore.order.domain.Reservation;
import com.bookstore.order.domain.command.OrderProduct;
import com.bookstore.order.domain.command.UpdateOrderCommand;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Component
@RequiredArgsConstructor
public class UpdateOrderUseCaseImpl implements UpdateOrderUseCase {
    private final FindOrderByIdPort findOrderByIdPort;
    private final SecurityService securityService;
    private final CreateReservationPort createReservationPort;
    private final UpdateReservationPort updateReservationPort;
    private final UpdateOrderPort updateOrderPort;
    private final OrderEventPort eventPort;

    @Override
    @Transactional(isolation = Isolation.REPEATABLE_READ)
    public Order update(String id, UpdateOrderCommand command) {
        Order order = findOrderByIdPort.find(id).orElseThrow(() -> Exceptions.ORDER_NOT_FOUND.create()
                .message("Order with id ''{}'' not found.", id)
                .get());

        securityService.checkAccess(order);

        order.checkCanUpdate();

        Reservation reservation = createOrUpdateReservation(order, command.getOrderProducts());

        order.update(reservation);

        Order saved = updateOrderPort.update(order);
        eventPort.updated(saved);
        return saved;
    }

    private Reservation createOrUpdateReservation(Order order, List<OrderProduct> orderProducts) {
        OrderDetails orderDetails = order.getOrderDetails();

        if (orderDetails.getReservation() == null) {
            return createReservationPort.create(orderProducts);
        } else {
            return updateReservationPort.update(orderDetails.getReservation().getId(), orderProducts);
        }
    }

}

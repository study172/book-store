package com.bookstore.order.adapter.out.jpa.entity;

import lombok.Data;

@Data
public class DescriptionEntity {
    private String name;
    private String value;
}

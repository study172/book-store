package com.bookstore.order.domain;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.util.Assert;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Data
@NoArgsConstructor
@Accessors(chain = true)
public class OrderDetails {
    private Reservation reservation;
    private BigDecimal totalAmount = BigDecimal.ZERO;

    public OrderDetails(Reservation reservation) {
        this.reservation = reservation;
        this.totalAmount = computeTotalAmount(reservation);
    }

    public void update(Reservation reservation) {
        if (this.reservation != null) {
            Assert.isTrue(
                    Objects.equals(this.reservation.getId(), reservation.getId()),
                    () -> "Reservation ids not equal. Cannot perform update on different reservations."
            );
        }

        this.reservation = reservation;
        this.totalAmount = computeTotalAmount(reservation);
    }

    private BigDecimal computeTotalAmount(Reservation reservation) {
        return Optional.ofNullable(reservation).map(Reservation::getDetails).stream()
                .flatMap(List::stream)
                .map(details -> {
                    BigInteger quantity = details.getQuantity();
                    BigDecimal price = details.getProduct().getPrice();

                    return price.multiply(new BigDecimal(quantity));
                })
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }
}

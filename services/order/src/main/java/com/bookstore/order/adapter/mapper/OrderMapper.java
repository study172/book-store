package com.bookstore.order.adapter.mapper;

import com.bookstore.common.api.orderapi.OrderApi;
import com.bookstore.order.common.mapper.CommonMapperConfig;
import com.bookstore.order.domain.Order;
import org.mapstruct.Mapper;

@Mapper(config = CommonMapperConfig.class)
public abstract class OrderMapper {
    public abstract OrderApi.Order toOrderApi(Order order);
}

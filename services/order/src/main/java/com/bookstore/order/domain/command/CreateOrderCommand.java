package com.bookstore.order.domain.command;

import lombok.Data;

import javax.validation.Valid;
import java.util.List;

@Data
public class CreateOrderCommand {
    private List<@Valid OrderProduct> orderProducts;
}

package com.bookstore.order.application.port.out;

import com.bookstore.order.domain.Order;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface PageOrderPort {
    Page<Order> page(Pageable pageable);

    Page<Order> page(String userId, Pageable pageable);
}

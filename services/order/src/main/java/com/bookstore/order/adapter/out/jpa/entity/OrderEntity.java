package com.bookstore.order.adapter.out.jpa.entity;

import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.Instant;

@Data
@Entity
@Table(name = "_order")
@EntityListeners(AuditingEntityListener.class)
@TypeDef(name = Types.JSONB, typeClass = JsonBinaryType.class)
public class OrderEntity {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "org.hibernate.id.UUIDGenerator")
    private String id;

    @Type(type = Types.JSONB)
    @Column(columnDefinition = Types.JSONB)
    private OrderDetailsEntity orderDetails;

    @Type(type = Types.JSONB)
    @Column(columnDefinition = Types.JSONB)
    private PaymentDetailsEntity paymentDetails;

    @Enumerated(EnumType.STRING)
    private Status status;

    @CreatedDate
    private Instant createdAt;

    @LastModifiedDate
    private Instant modifiedAt;

    @CreatedBy
    private String createdBy;

    @LastModifiedBy
    private String modifiedBy;

    public enum Status {
        DRAFT, CONFIRMED, PAID, COMPLETED, CANCELLED;
    }
}

package com.bookstore.order.adapter.out.rest.client.model;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Collection;

@Data
@Accessors(chain = true)
public class UpdateReservationCommand {
    private Collection<ProductReservation> reservations;
}

package com.bookstore.order.adapter.out.jpa.repository;

import com.bookstore.order.adapter.out.jpa.entity.OrderEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface OrderRepository extends JpaRepository<OrderEntity, String> {

    @Query("SELECT o " +
            "FROM OrderEntity o " +
            "WHERE function('jsonb_extract_path_text', o.orderDetails, 'reservation', 'id') = ?1")
    Optional<OrderEntity> findByReservationId(String reservationId);

    Optional<OrderEntity> findByIdAndCreatedBy(String id, String userId);

    Page<OrderEntity> findAllByCreatedBy(String userId, Pageable pageable);
}

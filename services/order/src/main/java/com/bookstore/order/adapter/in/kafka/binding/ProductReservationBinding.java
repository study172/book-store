package com.bookstore.order.adapter.in.kafka.binding;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

public interface ProductReservationBinding {
    String RESERVATION_EVENTS = "reservation-events";

    @Input(RESERVATION_EVENTS)
    SubscribableChannel events();
}

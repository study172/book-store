package com.bookstore.order.domain;

import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.math.BigInteger;

@Data
@Accessors(chain = true)
public class ReservationDetails {
    private String id;
    private Product product;
    private BigInteger quantity;
}

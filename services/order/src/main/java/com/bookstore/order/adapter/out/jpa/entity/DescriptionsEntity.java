package com.bookstore.order.adapter.out.jpa.entity;

import lombok.Data;

import java.util.List;

@Data
public class DescriptionsEntity {
    private List<DescriptionEntity> values;
}

package com.bookstore.order.domain.command;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.math.BigInteger;

@Data
public class OrderProduct {
    @NotBlank
    private String id;

    @NotNull
    @Positive
    private BigInteger quantity;
}

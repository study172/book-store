package com.bookstore.order.adapter.out.rest.client;

import com.bookstore.common.api.productapi.ProductApi;
import com.bookstore.order.adapter.out.rest.client.model.CreateReservationCommand;
import com.bookstore.order.adapter.out.rest.client.model.UpdateReservationCommand;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient("product")
public interface ProductClient {
    @PostMapping("/api/reservations")
    ProductApi.Reservation createReservation(@RequestBody CreateReservationCommand command);

    @PutMapping("/api/reservations/{id}")
    ProductApi.Reservation updateReservation(@PathVariable("id") String id,
                                             @RequestBody UpdateReservationCommand command);
}

package com.bookstore.order.application.impl;

import com.bookstore.order.application.port.in.CreateOrderUseCase;
import com.bookstore.order.application.port.out.CreateOrderPort;
import com.bookstore.order.application.port.out.OrderEventPort;
import com.bookstore.order.application.port.out.CreateReservationPort;
import com.bookstore.order.domain.Order;
import com.bookstore.order.domain.OrderDetails;
import com.bookstore.order.domain.Reservation;
import com.bookstore.order.domain.command.CreateOrderCommand;
import com.bookstore.order.domain.command.OrderProduct;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.List;

@Component
@RequiredArgsConstructor
public class CreateOrderUseCaseImpl implements CreateOrderUseCase {
    private final CreateOrderPort createOrderPort;
    private final CreateReservationPort createReservationPort;
    private final OrderEventPort eventPort;

    @Override
    public Order create(CreateOrderCommand command) {
        Order order = Order.createDraft();

        if (CollectionUtils.isEmpty(command.getOrderProducts())) {
            return createAndSendEvent(order);
        }

        Reservation reservation = createReservationPort.create(command.getOrderProducts());
        order.setOrderDetails(new OrderDetails(reservation));

        return createAndSendEvent(order);
    }

    private Order createAndSendEvent(Order order) {
        Order saved = createOrderPort.create(order);
        eventPort.created(saved);
        return saved;
    }
}

package com.bookstore.order.adapter.out.jpa;

import com.bookstore.order.adapter.mapper.OrderEntityMapper;
import com.bookstore.order.adapter.out.jpa.repository.OrderRepository;
import com.bookstore.order.application.port.out.FindOrderByIdPort;
import com.bookstore.order.domain.Order;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class FindOrderByIdPortImpl implements FindOrderByIdPort {
    private final OrderRepository repository;
    private final OrderEntityMapper mapper;

    @Override
    public Optional<Order> find(String id) {
        return repository.findById(id).map(mapper::toOrder);
    }

    @Override
    public Optional<Order> find(String id, String userId) {
        return repository.findByIdAndCreatedBy(id, userId).map(mapper::toOrder);
    }
}

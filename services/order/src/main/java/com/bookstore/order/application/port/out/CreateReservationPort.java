package com.bookstore.order.application.port.out;

import com.bookstore.order.domain.Reservation;
import com.bookstore.order.domain.command.OrderProduct;

import java.util.Collection;

public interface CreateReservationPort {
    Reservation create(Collection<OrderProduct> products);
}

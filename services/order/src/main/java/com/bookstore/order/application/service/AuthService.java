package com.bookstore.order.application.service;

import com.bookstore.authstarter.Role;
import com.bookstore.order.domain.Order;

public interface AuthService {
    void checkAccess(Order order);

    default boolean hasRole(Role role) {
        return hasAnyRole(role);
    }

    boolean hasAnyRole(Role... roles);
}

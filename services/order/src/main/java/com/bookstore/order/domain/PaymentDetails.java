package com.bookstore.order.domain;

import lombok.Data;
import lombok.experimental.Accessors;

import java.time.Instant;

@Data
@Accessors(chain = true)
public class PaymentDetails {
    private Instant paidAt;

    public boolean isNotPaid() {
        return paidAt == null;
    }
}

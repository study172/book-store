package com.bookstore.order.adapter.out.jpa;

import com.bookstore.order.adapter.mapper.OrderEntityMapper;
import com.bookstore.order.adapter.out.jpa.entity.OrderEntity;
import com.bookstore.order.adapter.out.jpa.repository.OrderRepository;
import com.bookstore.order.application.port.out.UpdateOrderPort;
import com.bookstore.order.domain.Order;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

@Component
@RequiredArgsConstructor
public class UpdateOrderPortImpl implements UpdateOrderPort {
    private final OrderRepository repository;
    private final OrderEntityMapper mapper;

    @Override
    public Order update(Order order) {
        Assert.notNull(order, () -> "Order must be provided.");
        Assert.hasText(order.getId(), () -> "Order id must be provided.");

        OrderEntity entity = mapper.toOrderEntity(order);
        return mapper.toOrder(repository.save(entity));
    }
}

package com.bookstore.order.adapter.out.rest.client.model;

import lombok.Data;

import java.math.BigInteger;

@Data
public class ProductReservation {
    private String productId;
    private BigInteger quantity;
}

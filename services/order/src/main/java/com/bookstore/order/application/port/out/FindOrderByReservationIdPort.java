package com.bookstore.order.application.port.out;

import com.bookstore.order.domain.Order;

import java.util.Optional;

public interface FindOrderByReservationIdPort {
    Optional<Order> find(String reservationId);
}

package com.bookstore.order.application.impl;

import com.bookstore.order.application.port.in.CancelOrderWithExpiredReservationUseCase;
import com.bookstore.order.application.port.out.FindOrderByReservationIdPort;
import com.bookstore.order.application.port.out.OrderEventPort;
import com.bookstore.order.application.port.out.UpdateOrderPort;
import com.bookstore.order.common.exception.Exceptions;
import com.bookstore.order.domain.Order;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

@Component
@RequiredArgsConstructor
public class CancelOrderWithExpiredReservationUseCaseImpl implements CancelOrderWithExpiredReservationUseCase {
    private final FindOrderByReservationIdPort findOrderByReservationIdPort;
    private final UpdateOrderPort updateOrderPort;
    private final OrderEventPort eventPort;

    @Override
    @Transactional(isolation = Isolation.REPEATABLE_READ)
    public void cancel(String reservationId) {
        findOrderByReservationIdPort.find(reservationId).ifPresent(order -> {
            order.cancel();

            Order saved = updateOrderPort.update(order);
            eventPort.cancelled(saved);
        });
    }
}

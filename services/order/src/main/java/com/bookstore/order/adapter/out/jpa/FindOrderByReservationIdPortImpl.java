package com.bookstore.order.adapter.out.jpa;

import com.bookstore.order.adapter.mapper.OrderEntityMapper;
import com.bookstore.order.adapter.out.jpa.repository.OrderRepository;
import com.bookstore.order.application.port.out.FindOrderByReservationIdPort;
import com.bookstore.order.domain.Order;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class FindOrderByReservationIdPortImpl implements FindOrderByReservationIdPort {
    private final OrderRepository repository;
    private final OrderEntityMapper mapper;

    @Override
    public Optional<Order> find(String reservationId) {
        Assert.hasText(reservationId, () -> "Reservation id must be provided.");

        return repository.findByReservationId(reservationId).map(mapper::toOrder);
    }
}

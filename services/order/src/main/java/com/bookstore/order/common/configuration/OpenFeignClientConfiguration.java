package com.bookstore.order.common.configuration;

import com.bookstore.authstarter.util.SecurityUtils;
import com.bookstore.order.adapter.out.rest.client.ProductClient;
import feign.RequestInterceptor;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;

@Configuration
@EnableFeignClients(clients = {
        ProductClient.class
})
public class OpenFeignClientConfiguration {

    @Bean
    public RequestInterceptor authorizationRequestInterceptor() {
        return template -> {
            template.header(HttpHeaders.AUTHORIZATION, "Bearer " + SecurityUtils.getToken());
        };
    }
}

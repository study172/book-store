package com.bookstore.order.domain;

import lombok.Data;

import java.util.List;

@Data
public class Descriptions {
    private List<Description> values;
}

package com.bookstore.order.adapter.in.kafka;

import com.bookstore.common.api.productapi.event.ReservationEvent;
import com.bookstore.order.adapter.in.kafka.binding.ProductReservationBinding;
import com.bookstore.order.application.port.in.CancelOrderWithExpiredReservationUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class ProductReservationEventListener {
    private final CancelOrderWithExpiredReservationUseCase cancelOrderWithExpiredReservationUseCase;

    @StreamListener(ProductReservationBinding.RESERVATION_EVENTS)
    public void handle(ReservationEvent event) {
        if (event.getType() == ReservationEvent.Type.EXPIRED) {
            cancelOrderWithExpiredReservationUseCase.cancel(event.getExpired().getReservationId());
        }
    }
}

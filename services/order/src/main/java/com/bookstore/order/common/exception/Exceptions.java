package com.bookstore.order.common.exception;

import com.bookstore.common.helper.exception.BaseException;
import com.bookstore.common.helper.exception.ExceptionCreator;
import com.bookstore.common.helper.exception.ExceptionCreatorFactory;
import com.bookstore.common.helper.exception.SimpleExceptionCreator;
import lombok.RequiredArgsConstructor;

import java.util.function.BiFunction;

@RequiredArgsConstructor
public enum Exceptions implements ExceptionCreatorFactory<ExceptionCreator<? extends BaseException>> {
    ORDER_NOT_FOUND(OrderNotFoundException::new)
    ;

    private final BiFunction<String, Throwable, ? extends BaseException> function;

    @Override
    public SimpleExceptionCreator<? extends BaseException> create() {
        return SimpleExceptionCreator.create(function);
    }
}

package com.bookstore.order.adapter.in.rest;

import com.bookstore.common.api.Page;
import com.bookstore.common.api.orderapi.OrderApi;
import com.bookstore.order.adapter.mapper.OrderMapper;
import com.bookstore.order.application.port.in.CancelOrderUseCase;
import com.bookstore.order.application.port.in.CompleteOrderUseCase;
import com.bookstore.order.application.port.in.ConfirmOrderUseCase;
import com.bookstore.order.application.port.in.CreateOrderUseCase;
import com.bookstore.order.application.port.in.GetOrderByIdUseCase;
import com.bookstore.order.application.port.in.PageOrdersUseCase;
import com.bookstore.order.application.port.in.PayOrderUseCase;
import com.bookstore.order.application.port.in.UpdateOrderUseCase;
import com.bookstore.order.common.mapper.PageMapper;
import com.bookstore.order.common.util.SecurityUtils;
import com.bookstore.order.domain.command.CreateOrderCommand;
import com.bookstore.order.domain.command.UpdateOrderCommand;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/orders")
public class OrderController {
    private final GetOrderByIdUseCase getOrderByIdUseCase;
    private final PageOrdersUseCase pageOrdersUseCase;
    private final CreateOrderUseCase createOrderUseCase;
    private final UpdateOrderUseCase updateOrderUseCase;
    private final ConfirmOrderUseCase confirmOrderUseCase;
    private final PayOrderUseCase payOrderUseCase;
    private final CompleteOrderUseCase completeOrderUseCase;
    private final CancelOrderUseCase cancelOrderUseCase;
    private final OrderMapper mapper;
    private final PageMapper pageMapper;

    @GetMapping("/{id}")
    @PreAuthorize(SecurityUtils.HAS_COMMON_ACCESS)
    public OrderApi.Order get(@PathVariable("id") String id) {
        return mapper.toOrderApi(getOrderByIdUseCase.get(id));
    }

    @GetMapping
    @PreAuthorize(SecurityUtils.HAS_COMMON_ACCESS)
    public Page<OrderApi.Order> get(@PageableDefault(sort = "createdAt") Pageable pageable) {
        return pageMapper.toPage(pageOrdersUseCase.page(pageable).map(mapper::toOrderApi));
    }

    @PostMapping
    @PreAuthorize(SecurityUtils.HAS_COMMON_ACCESS)
    public OrderApi.Order create(@RequestBody @Validated CreateOrderCommand command) {
        return mapper.toOrderApi(createOrderUseCase.create(command));
    }

    @PutMapping("/{id}")
    @PreAuthorize(SecurityUtils.HAS_COMMON_ACCESS)
    public OrderApi.Order update(@PathVariable("id") String id, @RequestBody @Validated UpdateOrderCommand command) {
        return mapper.toOrderApi(updateOrderUseCase.update(id, command));
    }

    @PutMapping("/{id}/confirm")
    @PreAuthorize(SecurityUtils.HAS_COMMON_ACCESS)
    public OrderApi.Order confirm(@PathVariable("id") String id) {
        return mapper.toOrderApi(confirmOrderUseCase.confirm(id));
    }

    @PutMapping("/{id}/pay")
    @PreAuthorize(SecurityUtils.HAS_ACCOUNTANT_ACCESS)
    public OrderApi.Order pay(@PathVariable("id") String id) {
        return mapper.toOrderApi(payOrderUseCase.pay(id));
    }

    @PutMapping("/{id}/complete")
    @PreAuthorize(SecurityUtils.HAS_ACCOUNTANT_ACCESS)
    public OrderApi.Order complete(@PathVariable("id") String id) {
        return mapper.toOrderApi(completeOrderUseCase.complete(id));
    }

    @DeleteMapping("/{id}/cancel")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize(SecurityUtils.HAS_COMMON_ACCESS)
    public void cancel(@PathVariable("id") String id) {
        cancelOrderUseCase.cancel(id);
    }
}

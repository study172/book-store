package com.bookstore.order.application.port.out;

import com.bookstore.order.domain.Order;

public interface OrderEventPort {
    void created(Order order);

    void updated(Order order);

    void confirmed(Order order);

    void paid(Order order);

    void completed(Order order);

    void cancelled(Order order);
}

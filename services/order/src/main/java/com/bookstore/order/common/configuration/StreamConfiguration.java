package com.bookstore.order.common.configuration;

import com.bookstore.order.adapter.in.kafka.binding.ProductReservationBinding;
import com.bookstore.order.adapter.out.kafka.binding.OrderBinding;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableBinding({OrderBinding.class, ProductReservationBinding.class})
public class StreamConfiguration {
}

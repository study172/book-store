package com.bookstore.order.adapter.out.jpa.entity;

import lombok.Data;

import java.time.Instant;

@Data
public class PaymentDetailsEntity {
    private Instant paidAt;
}

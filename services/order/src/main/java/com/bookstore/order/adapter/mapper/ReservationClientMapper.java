package com.bookstore.order.adapter.mapper;

import com.bookstore.common.api.productapi.ProductApi;
import com.bookstore.order.adapter.out.rest.client.model.CreateReservationCommand;
import com.bookstore.order.adapter.out.rest.client.model.ProductReservation;
import com.bookstore.order.adapter.out.rest.client.model.UpdateReservationCommand;
import com.bookstore.order.common.mapper.CommonMapperConfig;
import com.bookstore.order.domain.Product;
import com.bookstore.order.domain.Reservation;
import com.bookstore.order.domain.command.OrderProduct;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.Collection;

@Mapper(config = CommonMapperConfig.class)
public abstract class ReservationClientMapper {

    public CreateReservationCommand toCreateCommand(Collection<OrderProduct> products) {
        return new CreateReservationCommand().setReservations(toProductReservations(products));
    }

    @Mapping(target = "reservations", source = "products")
    public UpdateReservationCommand toUpdateCommand(Collection<OrderProduct> products) {
        return new UpdateReservationCommand().setReservations(toProductReservations(products));
    }

    public abstract Reservation toReservation(ProductApi.Reservation reservation);

    protected abstract Collection<ProductReservation> toProductReservations(Collection<OrderProduct> products);

    @Mapping(target = "productId", source = "id")
    protected abstract ProductReservation toProductReservation(OrderProduct product);

    @Mapping(target = "price", source = "productInfo.price")
    protected abstract Product toProduct(ProductApi.Product product);
}

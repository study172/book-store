package com.bookstore.order.application.impl;

import com.bookstore.authstarter.Role;
import com.bookstore.authstarter.service.SecurityService;
import com.bookstore.authstarter.util.SecurityUtils;
import com.bookstore.order.application.port.in.GetOrderByIdUseCase;
import com.bookstore.order.application.port.out.FindOrderByIdPort;
import com.bookstore.order.common.exception.Exceptions;
import com.bookstore.order.domain.Order;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class GetOrderByIdUseCaseImpl implements GetOrderByIdUseCase {
    private final FindOrderByIdPort findOrderByIdPort;
    private final SecurityService securityService;

    @Override
    public Order get(String id) {
        if (securityService.hasAnyRole(Role.ADMIN, Role.ACCOUNTANT)) {
            return findOrderByIdPort.find(id).orElseThrow(() -> Exceptions.ORDER_NOT_FOUND.create()
                    .message("Order with id ''{}'' not found.", id)
                    .get());
        } else {
            return findOrderByIdPort.find(id, getUserId()).orElseThrow(() -> Exceptions.ORDER_NOT_FOUND.create()
                    .message("Order with id ''{}'' not found.", id)
                    .get());
        }
    }

    private String getUserId() {
        return SecurityUtils.findUserId().orElse(null);
    }
}

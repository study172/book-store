package com.bookstore.order.adapter.out.rest;

import com.bookstore.common.api.productapi.ProductApi;
import com.bookstore.order.adapter.mapper.ReservationClientMapper;
import com.bookstore.order.adapter.out.rest.client.ProductClient;
import com.bookstore.order.adapter.out.rest.client.model.CreateReservationCommand;
import com.bookstore.order.application.port.out.CreateReservationPort;
import com.bookstore.order.domain.Reservation;
import com.bookstore.order.domain.command.OrderProduct;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Collection;

@Component
@RequiredArgsConstructor
public class CreateReservationPortImpl implements CreateReservationPort {
    private final ProductClient productClient;
    private final ReservationClientMapper mapper;

    @Override
    public Reservation create(Collection<OrderProduct> products) {
        CreateReservationCommand command = mapper.toCreateCommand(products);

        ProductApi.Reservation reservation = productClient.createReservation(command);
        return mapper.toReservation(reservation);
    }
}

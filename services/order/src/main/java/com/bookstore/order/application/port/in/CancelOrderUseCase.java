package com.bookstore.order.application.port.in;

public interface CancelOrderUseCase {
    void cancel(String id);
}

package com.bookstore.order.application.impl;

import com.bookstore.authstarter.Role;
import com.bookstore.authstarter.service.SecurityService;
import com.bookstore.authstarter.util.SecurityUtils;
import com.bookstore.order.application.port.in.PageOrdersUseCase;
import com.bookstore.order.application.port.out.PageOrderPort;
import com.bookstore.order.domain.Order;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class PageOrdersUseCaseImpl implements PageOrdersUseCase {
    private final PageOrderPort pageOrderPort;
    private final SecurityService securityService;

    @Override
    public Page<Order> page(Pageable pageable) {
        if (securityService.hasAnyRole(Role.ADMIN, Role.ACCOUNTANT)) {
            return pageOrderPort.page(pageable);
        } else {
            return pageOrderPort.page(SecurityUtils.findUserId().orElse(null), pageable);
        }
    }
}

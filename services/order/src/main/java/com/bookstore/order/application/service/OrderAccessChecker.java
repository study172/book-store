package com.bookstore.order.application.service;

import com.bookstore.authstarter.service.AccessChecker;
import com.bookstore.authstarter.util.SecurityUtils;
import com.bookstore.order.domain.Order;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class OrderAccessChecker implements AccessChecker<Order> {
    @Override
    public void check(Order order) throws AccessDeniedException {
        if (!Objects.equals(order.getCreatedBy(), getUserId())) {
            throw new AccessDeniedException("User cannot access order.");
        }
    }

    @Override
    public Class<? extends Order> getTargetType() {
        return Order.class;
    }

    private String getUserId() {
        return SecurityUtils.findUserId().orElse("");
    }
}

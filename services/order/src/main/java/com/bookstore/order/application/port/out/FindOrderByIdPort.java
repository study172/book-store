package com.bookstore.order.application.port.out;

import com.bookstore.order.domain.Order;

import java.util.Optional;

public interface FindOrderByIdPort {
    Optional<Order> find(String id);

    Optional<Order> find(String id, String userId);
}

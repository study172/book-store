package com.bookstore.order.domain;

import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

@Data
@Accessors(chain = true)
public class Product {
    private String id;
    private Descriptions description;
    private BigDecimal price;
}

package com.bookstore.order.application.service;

import com.bookstore.authstarter.Role;
import com.bookstore.order.domain.Order;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class AuthServiceImpl implements AuthService {
    @Override
    public void checkAccess(Order order) {
        if (hasAnyRole(Role.ADMIN)) {
            return;
        }

        if (!Objects.equals(order.getCreatedBy(), getUserId())) {
            throw new AccessDeniedException("User cannot access to order.");
        }
    }

    @Override
    public boolean hasAnyRole(Role... roles) {
        Set<String> authorities = findAuthentication()
                .map(Authentication::getAuthorities)
                .stream()
                .flatMap(Collection::stream)
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toSet());

        if (authorities.isEmpty()) {
            return false;
        }

        for (Role role : roles) {
            if (authorities.contains(role.getFullName())) {
                return true;
            }
        }

        return false;
    }

    private Optional<Authentication> findAuthentication() {
        return Optional.of(SecurityContextHolder.getContext()).map(SecurityContext::getAuthentication);
    }

    private String getUserId() {
        return findAuthentication()
                .filter(JwtAuthenticationToken.class::isInstance)
                .map(JwtAuthenticationToken.class::cast)
                .map(JwtAuthenticationToken::getTokenAttributes)
                .map(attr -> attr.get("user_id"))
                .map(String::valueOf)
                .orElse(null);
    }
}

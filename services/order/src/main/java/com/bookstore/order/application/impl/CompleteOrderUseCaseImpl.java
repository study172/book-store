package com.bookstore.order.application.impl;

import com.bookstore.order.application.port.in.CompleteOrderUseCase;
import com.bookstore.order.application.port.out.FindOrderByIdPort;
import com.bookstore.order.application.port.out.OrderEventPort;
import com.bookstore.order.application.port.out.UpdateOrderPort;
import com.bookstore.order.common.exception.Exceptions;
import com.bookstore.order.domain.Order;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.time.Clock;

@Component
@RequiredArgsConstructor
public class CompleteOrderUseCaseImpl implements CompleteOrderUseCase {
    private final FindOrderByIdPort findOrderByIdPort;
    private final UpdateOrderPort updateOrderPort;
    private final OrderEventPort eventPort;

    @Override
    @Transactional(isolation = Isolation.REPEATABLE_READ)
    public Order complete(String id) {
        Order order = findOrderByIdPort.find(id).orElseThrow(() -> Exceptions.ORDER_NOT_FOUND.create()
                .message("Order with id ''{}'' not found.", id)
                .get());

        order.complete();

        Order saved = updateOrderPort.update(order);
        eventPort.completed(saved);
        return saved;
    }
}

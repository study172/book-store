package com.bookstore.order.adapter.mapper;

import com.bookstore.order.adapter.out.jpa.entity.OrderEntity;
import com.bookstore.order.common.mapper.CommonMapperConfig;
import com.bookstore.order.domain.Order;
import org.mapstruct.Mapper;

@Mapper(config = CommonMapperConfig.class)
public abstract class OrderEntityMapper {
    public abstract Order toOrder(OrderEntity entity);

    public abstract OrderEntity toOrderEntity(Order order);
}

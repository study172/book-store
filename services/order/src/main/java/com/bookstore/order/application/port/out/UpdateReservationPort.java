package com.bookstore.order.application.port.out;

import com.bookstore.order.domain.Reservation;
import com.bookstore.order.domain.command.OrderProduct;

import java.util.List;

public interface UpdateReservationPort {
    Reservation update(String id, List<OrderProduct> products);
}

package com.bookstore.order.domain;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.util.Assert;

import java.time.Clock;
import java.time.Instant;

@Data
@NoArgsConstructor
@Accessors(chain = true)
public class Order {
    private String id;
    private OrderDetails orderDetails = new OrderDetails();
    private PaymentDetails paymentDetails = new PaymentDetails();
    private Status status;
    private Instant createdAt;
    private Instant modifiedAt;
    private String createdBy;
    private String modifiedBy;

    public static Order createDraft() {
        return new Order().setStatus(Status.DRAFT);
    }

    public void update(Reservation reservation) {
        checkCanUpdate();
        orderDetails.update(reservation);
    }

    public void checkCanUpdate() {
        Assert.state(status == Status.DRAFT, () -> "Cannot update order is status " + status);
    }

    public void confirm() {
        Assert.state(status == Status.DRAFT, () -> "Cannot confirm order is status " + status);

        setStatus(Status.CONFIRMED);
    }

    public void pay(Clock clock) {
        Assert.state(status == Status.CONFIRMED, () -> "Cannot pay order is status " + status);
        Assert.state(paymentDetails.isNotPaid(), () -> "Order already paid.");

        paymentDetails.setPaidAt(Instant.now(clock));
        setStatus(Status.PAID);
    }

    public void complete() {
        Assert.state(status == Status.PAID, () -> "Cannot complete order is status " + status);

        setStatus(Status.COMPLETED);
    }

    public void cancel() {
        Assert.state(status != Status.CANCELLED, () -> "Cannot cancel already cancelled order.");

        setStatus(Status.CANCELLED);
    }

    public enum Status {
        DRAFT, CONFIRMED, PAID, COMPLETED, CANCELLED;
    }
}

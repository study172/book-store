package com.bookstore.order.application.port.out;

import com.bookstore.order.domain.Order;

public interface CreateOrderPort {
    Order create(Order order);
}

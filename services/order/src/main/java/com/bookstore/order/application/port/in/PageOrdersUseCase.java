package com.bookstore.order.application.port.in;

import com.bookstore.order.domain.Order;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface PageOrdersUseCase {
    Page<Order> page(Pageable pageable);
}

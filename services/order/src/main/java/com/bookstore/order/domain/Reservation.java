package com.bookstore.order.domain;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
public class Reservation {
    private String id;
    private List<ReservationDetails> details;
}

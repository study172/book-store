package com.bookstore.order.application.port.in;

import com.bookstore.order.domain.Order;
import com.bookstore.order.domain.command.UpdateOrderCommand;

public interface UpdateOrderUseCase {
    Order update(String id, UpdateOrderCommand command);
}

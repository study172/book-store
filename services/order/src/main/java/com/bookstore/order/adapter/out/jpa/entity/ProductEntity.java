package com.bookstore.order.adapter.out.jpa.entity;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class ProductEntity {
    private String id;
    private DescriptionsEntity description;
    private BigDecimal price;
}

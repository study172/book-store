package com.bookstore.order.adapter.out.jpa.entity;

import lombok.Data;

import java.math.BigDecimal;
import java.math.BigInteger;

@Data
public class ReservationDetailsEntity {
    private String id;
    private ProductEntity product;
    private BigInteger quantity;
}

package com.bookstore.order.common.util;

public final class SecurityUtils {
    private SecurityUtils() {
        throw new UnsupportedOperationException();
    }

    public static final String HAS_COMMON_ACCESS = "isAuthenticated()";
    public static final String HAS_ACCOUNTANT_ACCESS = "(hasRole('ADMIN') or hasRole('CONTENT_MANAGER'))";
}

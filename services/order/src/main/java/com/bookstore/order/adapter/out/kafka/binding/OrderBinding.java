package com.bookstore.order.adapter.out.kafka.binding;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

public interface OrderBinding {
    String ORDER_EVENTS = "order-events";

    @Output(ORDER_EVENTS)
    MessageChannel events();
}

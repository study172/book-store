package com.bookstore.order.application.port.in;

public interface CancelOrderWithExpiredReservationUseCase {
    void cancel(String reservationId);
}

package com.bookstore.order.adapter.out.kafka;

import com.bookstore.common.api.event.Events;
import com.bookstore.common.api.orderapi.event.OrderEvent;
import com.bookstore.order.adapter.mapper.OrderMapper;
import com.bookstore.order.adapter.out.kafka.binding.OrderBinding;
import com.bookstore.order.application.port.out.OrderEventPort;
import com.bookstore.order.domain.Order;
import lombok.RequiredArgsConstructor;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class OrderEventPortImpl implements OrderEventPort {
    private final OrderBinding binding;
    private final OrderMapper mapper;

    @Override
    public void created(Order order) {
        OrderEvent event = OrderEvent.created(Events.Metadata.createDefault(), mapper.toOrderApi(order));
        binding.events().send(new GenericMessage<>(event, Events.headersWithKey(order.getId())));
    }

    @Override
    public void updated(Order order) {
        OrderEvent event = OrderEvent.updated(Events.Metadata.createDefault(), mapper.toOrderApi(order));
        binding.events().send(new GenericMessage<>(event, Events.headersWithKey(order.getId())));
    }

    @Override
    public void confirmed(Order order) {
        OrderEvent event = OrderEvent.confirmed(Events.Metadata.createDefault(), mapper.toOrderApi(order));
        binding.events().send(new GenericMessage<>(event, Events.headersWithKey(order.getId())));
    }

    @Override
    public void paid(Order order) {
        OrderEvent event = OrderEvent.paid(Events.Metadata.createDefault(), mapper.toOrderApi(order));
        binding.events().send(new GenericMessage<>(event, Events.headersWithKey(order.getId())));
    }

    @Override
    public void completed(Order order) {
        OrderEvent event = OrderEvent.completed(Events.Metadata.createDefault(), mapper.toOrderApi(order));
        binding.events().send(new GenericMessage<>(event, Events.headersWithKey(order.getId())));
    }

    @Override
    public void cancelled(Order order) {
        OrderEvent event = OrderEvent.cancelled(Events.Metadata.createDefault(), mapper.toOrderApi(order));
        binding.events().send(new GenericMessage<>(event, Events.headersWithKey(order.getId())));
    }
}

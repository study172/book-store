package com.bookstore.order.application.port.out;

import com.bookstore.order.domain.Order;

public interface UpdateOrderPort {
    Order update(Order order);
}

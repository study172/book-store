package com.bookstore.order.adapter.out.jpa;

import com.bookstore.order.adapter.mapper.OrderEntityMapper;
import com.bookstore.order.adapter.out.jpa.repository.OrderRepository;
import com.bookstore.order.application.port.out.PageOrderPort;
import com.bookstore.order.domain.Order;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class PageOrderPortImpl implements PageOrderPort {
    private final OrderRepository repository;
    private final OrderEntityMapper mapper;

    @Override
    public Page<Order> page(Pageable pageable) {
        return repository.findAll(pageable).map(mapper::toOrder);
    }

    @Override
    public Page<Order> page(String userId, Pageable pageable) {
        return repository.findAllByCreatedBy(userId, pageable).map(mapper::toOrder);
    }
}

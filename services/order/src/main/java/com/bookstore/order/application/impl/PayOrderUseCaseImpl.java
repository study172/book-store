package com.bookstore.order.application.impl;

import com.bookstore.order.application.port.in.PayOrderUseCase;
import com.bookstore.order.application.port.out.FindOrderByIdPort;
import com.bookstore.order.application.port.out.OrderEventPort;
import com.bookstore.order.application.port.out.UpdateOrderPort;
import com.bookstore.order.common.exception.Exceptions;
import com.bookstore.order.domain.Order;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.time.Clock;

@Component
@RequiredArgsConstructor
public class PayOrderUseCaseImpl implements PayOrderUseCase {
    private final FindOrderByIdPort findOrderByIdPort;
    private final UpdateOrderPort updateOrderPort;
    private final OrderEventPort eventPort;
    private final Clock clock;

    @Override
    @Transactional(isolation = Isolation.REPEATABLE_READ)
    public Order pay(String id) {
        Order order = findOrderByIdPort.find(id).orElseThrow(() -> Exceptions.ORDER_NOT_FOUND.create()
                .message("Order with id ''{}'' not found.", id)
                .get());

        order.pay(clock);

        Order saved = updateOrderPort.update(order);
        eventPort.paid(saved);
        return saved;
    }
}

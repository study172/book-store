package com.bookstore.order.adapter.out.rest;

import com.bookstore.common.api.productapi.ProductApi;
import com.bookstore.order.adapter.mapper.ReservationClientMapper;
import com.bookstore.order.adapter.out.rest.client.ProductClient;
import com.bookstore.order.adapter.out.rest.client.model.UpdateReservationCommand;
import com.bookstore.order.application.port.out.UpdateReservationPort;
import com.bookstore.order.domain.Reservation;
import com.bookstore.order.domain.command.OrderProduct;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class UpdateReservationPortImpl implements UpdateReservationPort {
    private final ProductClient productClient;
    private final ReservationClientMapper mapper;

    @Override
    public Reservation update(String id, List<OrderProduct> products) {
        UpdateReservationCommand command = mapper.toUpdateCommand(products);

        ProductApi.Reservation reservation = productClient.updateReservation(id, command);
        return mapper.toReservation(reservation);
    }
}

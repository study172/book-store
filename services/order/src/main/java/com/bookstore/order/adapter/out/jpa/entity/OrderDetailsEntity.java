package com.bookstore.order.adapter.out.jpa.entity;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class OrderDetailsEntity {
    private ReservationEntity reservation;
    private BigDecimal totalAmount;
}

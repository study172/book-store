package com.bookstore.order.application.impl;

import com.bookstore.authstarter.service.SecurityService;
import com.bookstore.order.application.port.in.ConfirmOrderUseCase;
import com.bookstore.order.application.port.out.FindOrderByIdPort;
import com.bookstore.order.application.port.out.OrderEventPort;
import com.bookstore.order.application.port.out.UpdateOrderPort;
import com.bookstore.order.common.exception.Exceptions;
import com.bookstore.order.domain.Order;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

@Component
@RequiredArgsConstructor
public class ConfirmOrderUseCaseImpl implements ConfirmOrderUseCase {
    private final FindOrderByIdPort findOrderByIdPort;
    private final SecurityService securityService;
    private final UpdateOrderPort updateOrderPort;
    private final OrderEventPort eventPort;

    @Override
    @Transactional(isolation = Isolation.REPEATABLE_READ)
    public Order confirm(String id) {
        Order order = findOrderByIdPort.find(id).orElseThrow(() -> Exceptions.ORDER_NOT_FOUND.create()
                .message("Order with id ''{}'' not found.", id)
                .get());

        securityService.checkAccess(order);

        order.confirm();

        Order saved = updateOrderPort.update(order);
        eventPort.confirmed(saved);
        return saved;
    }
}

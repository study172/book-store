package com.bookstore.order.application.port.in;

import com.bookstore.order.domain.Order;
import com.bookstore.order.domain.command.CreateOrderCommand;

public interface CreateOrderUseCase {
    Order create(CreateOrderCommand command);
}

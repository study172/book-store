package com.bookstore.common.api;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data(staticConstructor = "of")
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public class Page<T> {
    private final int pageNumber;
    private final int pageSize;
    private final int totalPages;
    private final long totalElements;
    private final List<T> content;
}

package com.bookstore.common.api.authapi.event;

import com.bookstore.common.api.authapi.AuthApi;
import com.bookstore.common.api.event.Events;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@NoArgsConstructor
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class UserEvent extends Events.BaseEvent {
    private UserCreated created;
    private UserUpdated updated;
    private UserDeleted deleted;
    private Type type;

    public enum Type {
        CREATED, UPDATED, DELETED
    }

    public static UserEvent created(Events.Metadata metadata, AuthApi.User user) {
        return UserEvent.builder()
                .type(Type.CREATED)
                .metadata(metadata)
                .created(UserCreated.builder().user(user).build())
                .build();
    }

    public static UserEvent updated(Events.Metadata metadata, AuthApi.User user) {
        return UserEvent.builder()
                .type(Type.UPDATED)
                .metadata(metadata)
                .updated(UserUpdated.builder().user(user).build())
                .build();
    }

    public static UserEvent deleted(Events.Metadata metadata, String userId) {
        return UserEvent.builder()
                .type(Type.DELETED)
                .metadata(metadata)
                .deleted(UserDeleted.builder().userId(userId).build())
                .build();
    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @Accessors(chain = true)
    public static class UserCreated {
        private AuthApi.User user;
    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @Accessors(chain = true)
    public static class UserUpdated {
        private AuthApi.User user;
    }


    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @Accessors(chain = true)
    public static class UserDeleted {
        private String userId;
    }
}

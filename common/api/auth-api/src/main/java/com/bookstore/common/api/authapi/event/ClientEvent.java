package com.bookstore.common.api.authapi.event;

import com.bookstore.common.api.authapi.AuthApi;
import com.bookstore.common.api.event.Events;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@NoArgsConstructor
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class ClientEvent extends Events.BaseEvent {
    private ClientCreated created;
    private ClientUpdated updated;
    private ClientDeleted deleted;
    private Type type;

    public enum Type {
        CREATED, UPDATED, DELETED
    }

    public static ClientEvent created(Events.Metadata metadata, AuthApi.Client client) {
        return ClientEvent.builder()
                .type(Type.CREATED)
                .metadata(metadata)
                .created(ClientCreated.builder().client(client).build())
                .build();
    }

    public static ClientEvent updated(Events.Metadata metadata, AuthApi.Client client) {
        return ClientEvent.builder()
                .type(Type.UPDATED)
                .metadata(metadata)
                .updated(ClientUpdated.builder().client(client).build())
                .build();
    }

    public static ClientEvent deleted(Events.Metadata metadata, String clientId) {
        return ClientEvent.builder()
                .type(Type.DELETED)
                .metadata(metadata)
                .deleted(ClientDeleted.builder().clientId(clientId).build())
                .build();
    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @Accessors(chain = true)
    public static class ClientCreated {
        private AuthApi.Client client;
    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @Accessors(chain = true)
    public static class ClientUpdated {
        private AuthApi.Client client;
    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @Accessors(chain = true)
    public static class ClientDeleted {
        private String clientId;
    }
}

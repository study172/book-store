package com.bookstore.common.api.authapi.event;

import com.bookstore.common.api.authapi.AuthApi;
import com.bookstore.common.api.event.Events;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@NoArgsConstructor
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class AuthorityEvent extends Events.BaseEvent {
    private AuthorityCreated created;
    private AuthorityDeleted deleted;
    private Type type;

    public enum Type {
        CREATED, DELETED
    }

    public static AuthorityEvent created(Events.Metadata metadata, AuthApi.Authority authority) {
        return AuthorityEvent.builder()
                .type(Type.CREATED)
                .metadata(metadata)
                .created(AuthorityCreated.builder().authority(authority).build())
                .build();
    }

    public static AuthorityEvent deleted(Events.Metadata metadata, String authorityId) {
        return AuthorityEvent.builder()
                .type(Type.DELETED)
                .metadata(metadata)
                .deleted(AuthorityDeleted.builder().authorityId(authorityId).build())
                .build();
    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @Accessors(chain = true)
    public static class AuthorityCreated {
        private AuthApi.Authority authority;
    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @Accessors(chain = true)
    public static class AuthorityDeleted {
        private String authorityId;
    }
}

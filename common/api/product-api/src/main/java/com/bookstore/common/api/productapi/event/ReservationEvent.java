package com.bookstore.common.api.productapi.event;

import com.bookstore.common.api.event.Events;
import com.bookstore.common.api.productapi.ProductApi;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@NoArgsConstructor
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class ReservationEvent extends Events.BaseEvent {
    private ReservationCreated created;
    private ReservationUpdated updated;
    private ReservationExpired expired;
    private ReservationCancelled cancelled;
    private ReservationCompleted completed;
    private Type type;

    public enum Type {
        CREATED, UPDATED, EXPIRED, CANCELLED, COMPLETED
    }

    public static ReservationEvent created(Events.Metadata metadata, ProductApi.Reservation reservation) {
        return ReservationEvent.builder()
                .type(Type.CREATED)
                .metadata(metadata)
                .created(ReservationCreated.builder().reservation(reservation).build())
                .build();
    }

    public static ReservationEvent updated(Events.Metadata metadata, ProductApi.Reservation reservation) {
        return ReservationEvent.builder()
                .type(Type.UPDATED)
                .metadata(metadata)
                .updated(ReservationUpdated.builder().reservation(reservation).build())
                .build();
    }

    public static ReservationEvent expired(Events.Metadata metadata, String reservationId) {
        return ReservationEvent.builder()
                .type(Type.EXPIRED)
                .metadata(metadata)
                .expired(ReservationExpired.builder().reservationId(reservationId).build())
                .build();
    }

    public static ReservationEvent cancelled(Events.Metadata metadata, String reservationId) {
        return ReservationEvent.builder()
                .type(Type.CANCELLED)
                .metadata(metadata)
                .cancelled(ReservationCancelled.builder().reservationId(reservationId).build())
                .build();
    }

    public static ReservationEvent completed(Events.Metadata metadata, String reservationId) {
        return ReservationEvent.builder()
                .type(Type.COMPLETED)
                .metadata(metadata)
                .completed(ReservationCompleted.builder().reservationId(reservationId).build())
                .build();
    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @Accessors(chain = true)
    public static class ReservationCreated {
        private ProductApi.Reservation reservation;
    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @Accessors(chain = true)
    public static class ReservationUpdated {
        private ProductApi.Reservation reservation;
    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @Accessors(chain = true)
    public static class ReservationExpired {
        private String reservationId;
    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @Accessors(chain = true)
    public static class ReservationCancelled {
        private String reservationId;
    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @Accessors(chain = true)
    public static class ReservationCompleted {
        private String reservationId;
    }
}

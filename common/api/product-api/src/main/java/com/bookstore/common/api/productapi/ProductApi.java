package com.bookstore.common.api.productapi;

import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.Instant;
import java.util.Collection;

public final class ProductApi {
    private ProductApi() {
        throw new UnsupportedOperationException();
    }

    @Data
    @Accessors(chain = true)
    public static class Product {
        private String id;
        private ProductDescription description;
        private Collection<Category> categories;
        private ProductInfo productInfo;
        private ProductPicture picture;
    }

    @Data
    @Accessors(chain = true)
    public static class ProductDescription {
        private Collection<Description> values;
    }

    @Data
    @Accessors(chain = true)
    public static class Description {
        private String name;
        private String value;
    }

    @Data
    @Accessors(chain = true)
    public static class Category {
        private String name;
        private CategoryDescription description;
    }

    @Data
    @Accessors(chain = true)
    public static class CategoryDescription {
        private Collection<Description> values;
    }

    @Data
    @Accessors(chain = true)
    public static class ProductInfo {
        private BigDecimal price;
        private BigInteger quantity;
    }

    @Data
    @Accessors(chain = true)
    public static class ProductPicture {
        private Collection<Picture> pictures;
    }

    @Data
    @Accessors(chain = true)
    public static class Picture {
        private Long number;
        private String url;
    }

    @Data
    @Accessors(chain = true)
    public static class Reservation {
        private String id;
        private Collection<ReservationDetails> details;
        private Instant createdAt;
        private Instant reservedTo;
    }

    @Data
    @Accessors(chain = true)
    public static class ReservationDetails {
        private String id;
        private Product product;
        private BigInteger quantity;
    }
}

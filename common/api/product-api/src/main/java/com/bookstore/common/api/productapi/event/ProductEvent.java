package com.bookstore.common.api.productapi.event;

import com.bookstore.common.api.event.Events;
import com.bookstore.common.api.productapi.ProductApi;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@NoArgsConstructor
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class ProductEvent extends Events.BaseEvent {
    private ProductCreated created;
    private ProductUpdated updated;
    private ProductDeleted deleted;
    private Type type;

    public enum Type {
        CREATED, UPDATED, DELETED
    }

    public static ProductEvent created(Events.Metadata metadata, ProductApi.Product product) {
        return ProductEvent.builder()
                .type(Type.CREATED)
                .metadata(metadata)
                .created(ProductCreated.builder().product(product).build())
                .build();
    }

    public static ProductEvent updated(Events.Metadata metadata, ProductApi.Product product) {
        return ProductEvent.builder()
                .type(Type.UPDATED)
                .metadata(metadata)
                .updated(ProductUpdated.builder().product(product).build())
                .build();
    }

    public static ProductEvent deleted(Events.Metadata metadata, String productId) {
        return ProductEvent.builder()
                .type(Type.DELETED)
                .metadata(metadata)
                .deleted(ProductDeleted.builder().productId(productId).build())
                .build();
    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @Accessors(chain = true)
    public static class ProductCreated {
        private ProductApi.Product product;
    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @Accessors(chain = true)
    public static class ProductUpdated {
        private ProductApi.Product product;
    }


    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @Accessors(chain = true)
    public static class ProductDeleted {
        private String productId;
    }
}

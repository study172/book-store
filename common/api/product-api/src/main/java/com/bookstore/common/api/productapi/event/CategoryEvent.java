package com.bookstore.common.api.productapi.event;

import com.bookstore.common.api.event.Events;
import com.bookstore.common.api.productapi.ProductApi;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@NoArgsConstructor
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class CategoryEvent extends Events.BaseEvent {
    private CategoryCreated created;
    private CategoryUpdated updated;
    private CategoryDeleted deleted;
    private Type type;

    public enum Type {
        CREATED, UPDATED, DELETED
    }

    public static CategoryEvent created(Events.Metadata metadata, ProductApi.Category category) {
        return CategoryEvent.builder()
                .type(Type.CREATED)
                .metadata(metadata)
                .created(CategoryCreated.builder().category(category).build())
                .build();
    }

    public static CategoryEvent updated(Events.Metadata metadata, ProductApi.Category category) {
        return CategoryEvent.builder()
                .type(Type.UPDATED)
                .metadata(metadata)
                .updated(CategoryUpdated.builder().category(category).build())
                .build();
    }

    public static CategoryEvent deleted(Events.Metadata metadata, String categoryId) {
        return CategoryEvent.builder()
                .type(Type.DELETED)
                .metadata(metadata)
                .deleted(CategoryDeleted.builder().categoryId(categoryId).build())
                .build();
    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @Accessors(chain = true)
    public static class CategoryCreated {
        private ProductApi.Category category;
    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @Accessors(chain = true)
    public static class CategoryUpdated {
        private ProductApi.Category category;
    }


    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @Accessors(chain = true)
    public static class CategoryDeleted {
        private String categoryId;
    }
}

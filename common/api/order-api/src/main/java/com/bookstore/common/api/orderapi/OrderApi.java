package com.bookstore.common.api.orderapi;

import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.Instant;
import java.util.Collection;

public final class OrderApi {
    private OrderApi() {
        throw new UnsupportedOperationException();
    }

    @Data
    public static class Order {
        private String id;
        private OrderDetails orderDetails;
        private PaymentDetails paymentDetails;
        private OrderStatus status;
    }

    @Data
    public static class OrderDetails {
        private Reservation reservation;
        private BigDecimal totalAmount;
    }

    @Data
    @Accessors(chain = true)
    public static class Reservation {
        private String id;
        private Collection<ReservationDetails> details;
    }

    @Data
    @Accessors(chain = true)
    public static class ReservationDetails {
        private String id;
        private Product product;
        private BigInteger quantity;
    }

    @Data
    public static class Product {
        private String id;
        private Descriptions description;
        private BigDecimal price;
    }

    @Data
    public static class PaymentDetails {
        private Instant paidAt;
    }

    @Data
    @Accessors(chain = true)
    public static class Descriptions {
        private Collection<Description> values;
    }

    @Data
    @Accessors(chain = true)
    public static class Description {
        private String name;
        private String value;
    }

    public enum OrderStatus {
        DRAFT, CONFIRMED, PAID, COMPLETED, CANCELLED;
    }
}

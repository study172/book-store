package com.bookstore.common.api.orderapi.event;

import com.bookstore.common.api.event.Events;
import com.bookstore.common.api.orderapi.OrderApi;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@NoArgsConstructor
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class OrderEvent extends Events.BaseEvent {
    private OrderCreated created;
    private OrderUpdated updated;
    private OrderConfirmed confirmed;
    private OrderPaid paid;
    private OrderCompleted completed;
    private OrderCancelled cancelled;
    private Type type;

    public enum Type {
        CREATED, UPDATED, CONFIRMED, PAID, COMPLETED, CANCELLED
    }

    public static OrderEvent created(Events.Metadata metadata, OrderApi.Order order) {
        return OrderEvent.builder()
                .type(Type.CREATED)
                .metadata(metadata)
                .created(OrderCreated.builder().order(order).build())
                .build();
    }

    public static OrderEvent updated(Events.Metadata metadata, OrderApi.Order order) {
        return OrderEvent.builder()
                .type(Type.UPDATED)
                .metadata(metadata)
                .updated(OrderUpdated.builder().order(order).build())
                .build();
    }

    public static OrderEvent confirmed(Events.Metadata metadata, OrderApi.Order order) {
        return OrderEvent.builder()
                .type(Type.CONFIRMED)
                .metadata(metadata)
                .confirmed(OrderConfirmed.builder().order(order).build())
                .build();
    }

    public static OrderEvent paid(Events.Metadata metadata, OrderApi.Order order) {
        return OrderEvent.builder()
                .type(Type.PAID)
                .metadata(metadata)
                .paid(OrderPaid.builder().order(order).build())
                .build();
    }

    public static OrderEvent completed(Events.Metadata metadata, OrderApi.Order order) {
        return OrderEvent.builder()
                .type(Type.COMPLETED)
                .metadata(metadata)
                .completed(OrderCompleted.builder().order(order).build())
                .build();
    }

    public static OrderEvent cancelled(Events.Metadata metadata, OrderApi.Order order) {
        return OrderEvent.builder()
                .type(Type.CANCELLED)
                .metadata(metadata)
                .cancelled(OrderCancelled.builder().order(order).build())
                .build();
    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @Accessors(chain = true)
    public static class OrderCreated {
        private OrderApi.Order order;
    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @Accessors(chain = true)
    public static class OrderUpdated {
        private OrderApi.Order order;
    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @Accessors(chain = true)
    public static class OrderConfirmed {
        private OrderApi.Order order;
    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @Accessors(chain = true)
    public static class OrderPaid {
        private OrderApi.Order order;
    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @Accessors(chain = true)
    public static class OrderCompleted {
        private OrderApi.Order order;
    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @Accessors(chain = true)
    public static class OrderCancelled {
        private OrderApi.Order order;
    }
}

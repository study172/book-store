package com.bookstore.common.api.exceptionhandlingwebapi;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;

import java.util.ArrayList;
import java.util.Collection;

public final class ExceptionHandlingApi {
    private ExceptionHandlingApi() {
        throw new UnsupportedOperationException();
    }

    @Data
    @Accessors(chain = true)
    public static abstract class ValidationError {
        private String message;

        public static ValidationError from(ObjectError error) {
            if (error instanceof FieldError) {
                return new FieldValidationError()
                        .setField(((FieldError) error).getField())
                        .setMessage(error.getDefaultMessage());
            } else {
                return new ObjectValidationError()
                        .setObjectName(error.getObjectName())
                        .setMessage(error.getDefaultMessage());
            }
        }
    }

    @Data
    @Accessors(chain = true)
    @EqualsAndHashCode(callSuper = true)
    public static class ObjectValidationError extends ValidationError {
        private String objectName;
    }

    @Data
    @Accessors(chain = true)
    @EqualsAndHashCode(callSuper = true)
    public static class FieldValidationError extends ValidationError {
        private String field;
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ValidationErrors {
        private Collection<ValidationError> errors = new ArrayList<>();
    }
}

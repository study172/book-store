package com.bookstore.common.helper.exceptionhandlingweb.configuration;

import com.bookstore.common.helper.exceptionhandlingweb.RestExceptionHandler;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import(RestExceptionHandler.class)
public class RestExceptionHandlerConfiguration {
}

package com.bookstore.common.helper.exceptionhandlingweb;

import com.bookstore.common.api.exceptionhandlingwebapi.ExceptionHandlingApi;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.List;
import java.util.stream.Collectors;

@RestControllerAdvice
public class RestExceptionHandler {

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ExceptionHandlingApi.ValidationErrors> handle(MethodArgumentNotValidException ex) {
        List<ExceptionHandlingApi.ValidationError> errors = ex.getBindingResult().getAllErrors().stream()
                .map(ExceptionHandlingApi.ValidationError::from)
                .collect(Collectors.toList());
        return ResponseEntity.badRequest().body(new ExceptionHandlingApi.ValidationErrors(errors));
    }
}

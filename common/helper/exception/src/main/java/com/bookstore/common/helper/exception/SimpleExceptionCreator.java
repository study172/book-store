package com.bookstore.common.helper.exception;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;

import java.util.Arrays;
import java.util.function.BiFunction;

@RequiredArgsConstructor
public class SimpleExceptionCreator<E extends Exception> implements ExceptionCreator<E> {
    private final BiFunction<String, Throwable, E> function;

    private String message;
    private Object[] args;
    private Throwable cause;

    @Override
    public SimpleExceptionCreator<E> message(String message) {
        if (this.message == null) {
            this.message = message;
        } else {
            this.message = this.message + message;
        }

        return this;
    }

    @Override
    public SimpleExceptionCreator<E> message(String message, Object... args) {
        return message(message).args(args);
    }

    @Override
    public SimpleExceptionCreator<E> args(Object... args) {
        if (this.args == null) {
            this.args = args;
        } else {
            this.args = concat(this.args, args);
        }

        return this;
    }

    @Override
    public SimpleExceptionCreator<E> cause(Throwable cause) {
        this.cause = cause;

        return this;
    }

    @Override
    @SneakyThrows
    public E get() {
        String message = MessageFormatter.format(this.message, this.args);

        return function.apply(message, cause);
    }

    private Object[] concat(Object[] array1, Object[] array2) {
        Object[] result = Arrays.copyOf(array1, array1.length + array2.length);
        System.arraycopy(array2, 0, result, array1.length, array2.length);
        return result;
    }

    public static <E extends RuntimeException> SimpleExceptionCreator<E> create(BiFunction<String, Throwable, E> f) {
        return new SimpleExceptionCreator<>(f);
    }
}

package com.bookstore.common.helper.exception;

@SuppressWarnings("serial")
public abstract class BaseException extends RuntimeException {
    public BaseException(String message, Throwable cause) {
        super(message, cause);
    }
}

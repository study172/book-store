package com.bookstore.authstarter.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.util.StringUtils;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class RoleJwtGrantedAuthoritiesConverter implements Converter<Jwt, Collection<GrantedAuthority>> {
    private static final String SCOPE_PREFIX = "SCOPE_";

    private final Map<String, Function<Object, Collection<String>>> claimToConverter = Map.of(
            "scope", o -> toCollection(o, SCOPE_PREFIX),
            "authorities", this::toCollection
    );

    @Override
    public Collection<GrantedAuthority> convert(Jwt jwt) {
        return claimToConverter.entrySet().stream()
                .filter(entry -> jwt.containsClaim(entry.getKey()))
                .map(entry -> entry.getValue().apply(jwt.getClaim(entry.getKey())))
                .flatMap(Collection::stream)
                .map(SimpleGrantedAuthority::new)
                .collect(Collectors.toList());
    }

    @SuppressWarnings("unchecked")
    private Collection<String> toCollection(Object authorities) {
        if (authorities instanceof String) {
            if (StringUtils.hasText((String) authorities)) {
                return Arrays.asList(((String) authorities).split(" "));
            } else {
                return Collections.emptyList();
            }
        } else if (authorities instanceof Collection) {
            return (Collection<String>) authorities;
        }

        return Collections.emptyList();
    }

    private Collection<String> toCollection(Object authorities, String authorityPrefix) {
        return toCollection(authorities).stream()
                .map(a -> authorityPrefix + a)
                .collect(Collectors.toList());
    }
}

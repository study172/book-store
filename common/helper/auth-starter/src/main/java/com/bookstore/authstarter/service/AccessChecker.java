package com.bookstore.authstarter.service;

import org.springframework.security.access.AccessDeniedException;

public interface AccessChecker<T> {
    void check(T object) throws AccessDeniedException;

    Class<? extends T> getTargetType();
}

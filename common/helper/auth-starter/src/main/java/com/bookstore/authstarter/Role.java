package com.bookstore.authstarter;

import lombok.Getter;

@Getter
public enum Role {
    ADMIN("ADMIN"),
    USER("USER"),
    ACCOUNTANT("ACCOUNTANT"),
    CONTENT_MANAGER("CONTENT_MANAGER");

    public static final String DEFAULT_PREFIX = "ROLE_";

    private final String name;
    private final String fullName;

    Role(String name) {
        this.name = name;
        this.fullName = DEFAULT_PREFIX + name;
    }
}

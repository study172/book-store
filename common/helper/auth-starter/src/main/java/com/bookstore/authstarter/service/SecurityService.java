package com.bookstore.authstarter.service;

import com.bookstore.authstarter.Role;

public interface SecurityService {
    <T> void checkAccess(T object);

    default boolean hasRole(Role role) {
        return hasAnyRole(role);
    }

    boolean hasAnyRole(Role... roles);
}

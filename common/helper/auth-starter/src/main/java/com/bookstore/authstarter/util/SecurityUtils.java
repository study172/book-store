package com.bookstore.authstarter.util;

import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;

import java.util.Map;
import java.util.Optional;

public final class SecurityUtils {
    private SecurityUtils() {
        throw new UnsupportedOperationException();
    }

    public static Optional<String> findClientId() {
        return findTokenAttributes()
                .map(attr -> attr.get("client_id"))
                .map(String::valueOf);
    }

    public static Optional<String> findUserId() {
        return findTokenAttributes()
                .map(attr -> attr.get("user_id"))
                .map(String::valueOf);
    }

    public static String getToken() {
        return findAuthentication()
                .map(JwtAuthenticationToken::getToken)
                .map(Jwt::getTokenValue)
                .orElse("");
    }

    public static Optional<Map<String, Object>> findTokenAttributes() {
        return findAuthentication()
                .map(JwtAuthenticationToken::getTokenAttributes);
    }

    public static Optional<JwtAuthenticationToken> findAuthentication() {
        return Optional.of(SecurityContextHolder.getContext())
                .map(SecurityContext::getAuthentication)
                .filter(JwtAuthenticationToken.class::isInstance)
                .map(JwtAuthenticationToken.class::cast);
    }
}

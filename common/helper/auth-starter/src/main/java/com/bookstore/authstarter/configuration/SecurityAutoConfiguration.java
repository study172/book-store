package com.bookstore.authstarter.configuration;

import com.bookstore.authstarter.converter.RoleJwtGrantedAuthoritiesConverter;
import com.bookstore.authstarter.service.AccessChecker;
import com.bookstore.authstarter.service.SecurityService;
import com.bookstore.authstarter.service.DefaultSecurityService;
import com.bookstore.authstarter.util.SecurityUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.autoconfigure.security.servlet.EndpointRequest;
import org.springframework.boot.actuate.autoconfigure.security.servlet.ManagementWebSecurityAutoConfiguration;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.security.oauth2.resource.servlet.OAuth2ResourceServerAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.domain.AuditorAware;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.jwt.JwtDecoder;
import org.springframework.security.oauth2.jwt.NimbusJwtDecoderJwkSupport;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationConverter;
import org.springframework.web.client.RestOperations;
import org.springframework.web.client.RestTemplate;

@Configuration
@AutoConfigureBefore({
        OAuth2ResourceServerAutoConfiguration.class,
        ManagementWebSecurityAutoConfiguration.class
})
@ConditionalOnProperty(prefix = "security", name = "enabled", havingValue = "true", matchIfMissing = true)
public class SecurityAutoConfiguration {

    @Configuration
    @RequiredArgsConstructor
    public static class ResourceServerConfiguration {
        @Value("${auth.jwk-set-uri}")
        private final String jwkSetUri;

        @Bean
        @ConditionalOnMissingBean
        @SuppressWarnings("deprecation")
        public JwtDecoder jwtDecoder(RestOperations restOperations) {
            NimbusJwtDecoderJwkSupport decoder = new NimbusJwtDecoderJwkSupport(jwkSetUri);
            decoder.setRestOperations(restOperations);
            return decoder;
        }
    }

    @Configuration
    @RequiredArgsConstructor
    @EnableGlobalMethodSecurity(securedEnabled = true, prePostEnabled = true, proxyTargetClass = true)
    public static class ServletResourceServerConfiguration extends WebSecurityConfigurerAdapter {
        private static final String SCOPE_PREFIX = "SCOPE_";

        @Value("${spring.application.name}")
        private final String applicationName;
        private final JwtDecoder jwtDecoder;

        @Override
        protected void configure(HttpSecurity http) throws Exception {
            http
                    .httpBasic().disable()
                    .formLogin().disable()
                    .csrf().disable()
                    .authorizeRequests(request -> request
                            .requestMatchers(EndpointRequest.toAnyEndpoint()).permitAll()
                            .anyRequest().access("hasAuthority('" + SCOPE_PREFIX + applicationName + "')"))
                    .oauth2ResourceServer()
                    .jwt()
                    .jwtAuthenticationConverter(createConverter())
                    .decoder(jwtDecoder);
        }

        private Converter<Jwt, ? extends AbstractAuthenticationToken> createConverter() {
            JwtAuthenticationConverter converter = new JwtAuthenticationConverter();
            converter.setJwtGrantedAuthoritiesConverter(new RoleJwtGrantedAuthoritiesConverter());
            return converter;
        }
    }

    @Configuration
    @ConditionalOnClass(name = "org.springframework.data.domain.AuditorAware")
    public static class AuditorAwareConfiguration {
        @Bean
        @ConditionalOnMissingBean
        public AuditorAware<String> auditorAware() {
            return () -> SecurityUtils.findUserId().or(SecurityUtils::findClientId);
        }
    }

    @Configuration
    public static class SecurityServiceAutoConfiguration {
        @Bean
        @ConditionalOnMissingBean
        public SecurityService defaultSecurityService(ObjectProvider<AccessChecker<?>> accessCheckers) {
            return new DefaultSecurityService(accessCheckers);
        }
    }
}

package com.bookstore.authstarter.service;

import com.bookstore.authstarter.Role;
import com.bookstore.authstarter.util.SecurityUtils;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

public class DefaultSecurityService implements SecurityService {
    private final Map<Class<?>, AccessChecker<?>> accessCheckers;

    public DefaultSecurityService(ObjectProvider<AccessChecker<?>> accessCheckers) {
        this.accessCheckers = accessCheckers.stream()
                .collect(Collectors.toMap(AccessChecker::getTargetType, Function.identity()));
    }

    @Override
    public <T> void checkAccess(T object) {
        if (hasAnyRole(Role.ADMIN) || object == null) {
            return;
        }

        AccessChecker<T> accessChecker = getChecker(object);

        if (accessChecker == null) {
            throw new AccessDeniedException("Access is denied.");
        }

        accessChecker.check(object);
    }

    @Override
    public boolean hasAnyRole(Role... roles) {
        Set<String> authorities = SecurityUtils.findAuthentication()
                .map(Authentication::getAuthorities)
                .stream()
                .flatMap(Collection::stream)
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toSet());

        if (authorities.isEmpty()) {
            return false;
        }

        for (Role role : roles) {
            if (authorities.contains(role.getFullName())) {
                return true;
            }
        }

        return false;
    }

    @SuppressWarnings("unchecked")
    private <T> AccessChecker<T> getChecker(T object) {
        return (AccessChecker<T>) accessCheckers.get(object.getClass());
    }
}
